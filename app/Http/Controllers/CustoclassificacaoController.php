<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Redirect,Response,DB,Config;
use Datatables;
use View;
use Session;
use Carbon\Carbon;

class CustoclassificacaoController extends Controller
{
    public $itemMenu = 11;
    public $arrayTipo = array('MOEDA' => 'M','NUMERO' => 'N','PERCENTUAL' => 'P','FUNÇÃO'=>'F','BOOLEANO'=>'B');
    public $created_at = '';
    public $updated_at = '';

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('ativo');
        $this->created_at = Carbon::now()->format('Y-m-d H:i:s');
        $this->updated_at = Carbon::now()->format('Y-m-d H:i:s');
        View::share ( 'itemMenu', $this->itemMenu );
    }

    public function index()
    {
        //$pageIcon = 'admin-list';
        //$pageTitulo = 'Receita Rota Itens';
        //$itemMenu = 6;
        //View::share ( 'itemMenu', $itemMenu );

        return view('custoclassificacao.index');
    }
    public function custoclassificacaoList()
    {
        $sql = "SELECT C.*,P.DESCRI AS ANALITICA,R.DESCRI AS CUSTO,PS.DESCRI AS SINTETICA FROM CUSTO_CLASSIFICACAO C
        LEFT JOIN PAGCLA P ON C.CODCLAP = P.CODCLAP
        LEFT JOIN RODCUS R ON C.CODCUS = R.CODCUS
        LEFT JOIN PAGCLA PS ON P.PAICLAP = PS.CODCLAP
        ORDER BY P.DESCRI ";
        $lista = DB::select($sql);

       // dd($lista);
        return datatables()->of($lista)
        ->addIndexColumn()
        ->addColumn('action', function($lista){

               $btn =  '<a href="'.route('custoclassificacao.edit',['ID'=>$lista->ID]).'" class="btn btn-sm btn-info pull-left" style="margin-right: 3px;">Editar</a>';

                return $btn;
        })
        ->rawColumns(['action'])
            ->make(true);
    }

    public function create()
    {

        $listaClassificacao = DB::select("SELECT P.DESCRI AS ANALITICA,F.CODCLAP,F.DESCRI AS SINTETICA FROM PAGCLA F INNER JOIN PAGCLA P ON F.PAICLAP = P.CODCLAP WHERE F.TIPCLA = 2 AND F.SITUAC = 'A' ORDER BY P.DESCRI,F.DESCRI");
        $listaCustos = DB::select("SELECT CODCUS,DESCRI AS CUSTO FROM `RODCUS` WHERE SITUAC = 'A' ORDER BY DESCRI");

        return view('custoclassificacao.create',['listaClassificacao'=>$listaClassificacao,'listaCustos'=>$listaCustos]);
    }

    public function store(Request $request)
    {

       // $val[]= array('CODUNN'=>$request->CODUNN,'DESCRI'=>$request->DESCRI,'IDPAI'=>$request->IDPAI);

        $val = array('CODCLAP'=>$request->CODCLAP,'CODCUS'=>$request->CODCUS,'OBRIGARATEIO'=>$request->OBRIGARATEIO, 'created_at'=> $this->created_at ,'updated_at' => $this->updated_at);

        $insertVal = DB::table('CUSTO_CLASSIFICACAO')->insert($val);

        return redirect()->route('custoclassificacao.index')
            ->with('flash_message',
             'Registro Adicionado.');
    }

    public function show($id)
    {

    }

    public function edit(Request $request,$id)
    {

        $listaClassificacao = DB::select("SELECT P.DESCRI AS ANALITICA,F.CODCLAP,F.DESCRI AS SINTETICA FROM PAGCLA F INNER JOIN PAGCLA P ON F.PAICLAP = P.CODCLAP WHERE F.TIPCLA = 2 AND F.SITUAC = 'A' ORDER BY P.DESCRI,F.DESCRI");
        $listaCustos = DB::select("SELECT CODCUS,DESCRI AS CUSTO FROM `RODCUS` WHERE SITUAC = 'A' ORDER BY DESCRI");

        if($id){
            $custoClassificacao = DB::table('CUSTO_CLASSIFICACAO')->select('*')->where('ID','=',$id)->first();
        }else{
            return redirect()->route('custoclassificacao.index')
            ->with('flash_message',
             'Registro não encotrado.');
        }

        return view('custoclassificacao.edit',['listaClassificacao'=>$listaClassificacao,'listaCustos'=>$listaCustos,'custoClassificacao' =>$custoClassificacao,'id'=>$id]);
    }

    public function update(Request $request, $id)
    {

        $val = array('CODCLAP'=>$request->CODCLAP,'CODCUS'=>$request->CODCUS,'OBRIGARATEIO'=>$request->OBRIGARATEIO, 'created_at'=> $this->created_at ,'updated_at' => $this->updated_at);

        $updateVal  = DB::table('CUSTO_CLASSIFICACAO')
            ->where('ID','=', $id)
            ->update($val);
        if($request->OBRIGARATEIO == 'S'){
            $delitem = DB::table('ORCLAN_FOR')->where('ANALIT', '=', $request->CODCLAP)->delete();
        }

        return redirect()->route('custoclassificacao.index')
            ->with('flash_message',
             'Registro atualizado.');

    }

    public function destroy($id)
    {

    }


}
