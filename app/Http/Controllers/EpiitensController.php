<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Redirect,Response,DB,Config;
use Datatables;
use View;
use Session;

class EpiitensController extends Controller
{
    public $itemMenu = 12;

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('ativo');
        View::share ( 'itemMenu', $this->itemMenu );
    }

    public function index()
    {
        //$pageIcon = 'admin-list';
        //$pageTitulo = 'Receita Rota Itens';
        //$itemMenu = 6;
        //View::share ( 'itemMenu', $itemMenu );

        return view('epiitens.index');
    }
    public function epiitensList()
    {
        $sql = "SELECT EI.*, PC.DESCRI AS ANALITICA, EC.DESCRI AS CARGO, EP.DESCRI AS PROD FROM EPI_SMLD_ITEM EI
        INNER JOIN PAGCLA PC ON PC.CODCLAP = EI.ANALIT
        INNER JOIN EPI_SMLD_CARGO EC ON EC.ID = EI.EPI_SMLD_CARGO_ID
        INNER JOIN EPI_SMLD_PROD EP ON EP.ID = EI.EPI_SMLD_PROD_ID
        ORDER BY PC.DESCRI ASC";
        $lista = DB::select($sql);

       // dd($lista);
        return datatables()->of($lista)
        ->addIndexColumn()
        ->addColumn('action', function($lista){

               $btn =  '<a href="'.route('epiitens.edit',['ID'=>$lista->ID]).'" class="btn btn-sm btn-info pull-left" style="margin-right: 3px;">Editar</a>';

                return $btn;
        })
        ->rawColumns(['action'])
            ->make(true);
    }

    public function create()
    {
        $listaProd = DB::table('EPI_SMLD_PROD')->select('*')->orderBy('DESCRI','ASC')->get();
        $listaCargo = DB::table('EPI_SMLD_CARGO')->select('*')->orderBy('DESCRI','ASC')->get();
        $listaAnalit = DB::table('EPI_SMLD_ANALIT')->select('EPI_SMLD_ANALIT.ANALIT','PAGCLA.DESCRI AS ANALITICA')->join('PAGCLA', 'EPI_SMLD_ANALIT.ANALIT', '=', 'PAGCLA.CODCLAP')->orderBy('PAGCLA.DESCRI','ASC')->get();

        return view('epiitens.create',['listaProd'=>$listaProd,'listaCargo'=>$listaCargo,'listaAnalit' =>$listaAnalit]);
    }

    public function store(Request $request)
    {

       // $val[]= array('CODUNN'=>$request->CODUNN,'DESCRI'=>$request->DESCRI,'IDPAI'=>$request->IDPAI);

        $val = array('TIPO'=>$request->TIPO,'ANALIT'=>$request->ANALIT,'EPI_SMLD_CARGO_ID'=>$request->EPI_SMLD_CARGO_ID,'EPI_SMLD_PROD_ID'=>$request->EPI_SMLD_PROD_ID);

        $insertVal = DB::table('EPI_SMLD_ITEM')->insert($val);

        return redirect()->route('epiitens.index')
            ->with('flash_message',
             'Registro Adicionado.');
    }

    public function show($id)
    {

    }

    public function edit(Request $request,$id)
    {
        $listaProd = DB::table('EPI_SMLD_PROD')->select('*')->orderBy('DESCRI','ASC')->get();
        $listaCargo = DB::table('EPI_SMLD_CARGO')->select('*')->orderBy('DESCRI','ASC')->get();
        $listaAnalit = DB::table('EPI_SMLD_ANALIT')->select('EPI_SMLD_ANALIT.ANALIT','PAGCLA.DESCRI AS ANALITICA')->join('PAGCLA', 'EPI_SMLD_ANALIT.ANALIT', '=', 'PAGCLA.CODCLAP')->orderBy('PAGCLA.DESCRI','ASC')->get();

        if($id){
            $listaItem = DB::table('EPI_SMLD_ITEM')->select('*')->where('ID','=',$id)->first();
        }else{
            return redirect()->route('epiitens.index')
            ->with('flash_message',
             'Registro não encotrado.');
        }

        return view('epiitens.edit',['listaProd'=>$listaProd,'listaCargo'=>$listaCargo,'listaAnalit'=>$listaAnalit,'listaItem'=>$listaItem,'id'=>$id]);
    }

    public function update(Request $request, $id)
    {

        $val = array('TIPO'=>$request->TIPO,'ANALIT'=>$request->ANALIT,'EPI_SMLD_CARGO_ID'=>$request->EPI_SMLD_CARGO_ID,'EPI_SMLD_PROD_ID'=>$request->EPI_SMLD_PROD_ID);

        $updateVal  = DB::table('EPI_SMLD_ITEM')
            ->where('ID','=', $id)
            ->update($val);

        return redirect()->route('epiitens.index')
            ->with('flash_message',
             'Registro atualizado.');

    }

    public function destroy($id)
    {

    }

}
