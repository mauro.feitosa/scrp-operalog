<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use View;
use DB;
use App\Rodcga;
use App\Rodcus;
use App\Rodgru;
use App\Rodunn;
use App\Pagcla;
use Session;
use Carbon\Carbon;
use Auth;
use App\Imports\ItemImporta;
use App\Imports\MultImport;
use Maatwebsite\Excel\Facades\Excel;
//use MenuController;

class ForecastController extends Controller
{
    //
    public $sqlServerDb = 'sqlsrv';

    public $itemMenu = 2;
    public $created_at = '';
    public $updated_at = '';
    public $arrayMes = array('jan'=>'01','fev'=>'02','mar'=>'03','abr'=>'04','mai'=>'05','jun'=>'06','jul'=>'07','ago'=>'08','set'=>'09','out'=>'10','nov'=>'11','dez'=>'12');

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('ativo');
        View::share ( 'itemMenu', $this->itemMenu );
        $this->created_at = Carbon::now()->format('Y-m-d H:i:s');
        $this->updated_at = Carbon::now()->format('Y-m-d H:i:s');
    }

    public function iniciaorclanfor(Request $request){


        $arrayMesNotIn = array(0=>'01',1=>'02',2=>'03',3=>'04',4=>'05',5=>'06');

        $items = array();

        $orc = DB::table('ORCAMENTO')->select('ano')->where('ativo','=','S')->first();
        $ano = $orc->ano;

 /*       $sql1 = "SELECT CODCGA,CODUNN FROM MENUFILIAL ORDER BY CODCGA,CODUNN ASC";
        $codcgaCodunn = DB::select($sql1);

        $grupoSql = "SELECT CODIGO FROM MENUPACOTE ORDER BY CODIGO ASC";
        $grupo = DB::select($grupoSql);

        DB::table('ORCLAN_FOR')->where('ANO','=',$ano)->whereIn('MES',$arrayMesNotIn)->delete();

        foreach($codcgaCodunn as $c){

            $CODCGA = $c->CODCGA;
            $CODUNN = $c->CODUNN;

            foreach ($grupo as $g) {

                $codgru = $g->CODIGO;

                $sqlPagcla = "SELECT P.* FROM PAGCLA P WHERE TIPCLA = 2 AND P.CODGRU = ".$g->CODIGO." AND P.SITUAC = 'A'  ORDER BY P.PAICLAP ASC";
                $pagcla = DB::select($sqlPagcla);

                //dd($pagcla);
                foreach($pagcla as $p ){

                    $ANALIT = $p->CODCLAP;
                    $SINTET = $p->PAICLAP;
                    $valor  =  0.00;
                    $items = array();
                    foreach($arrayMesNotIn as $mes){
                        $MESANO = $mes.'/'.$ano;
                        $items[] = ['ANO' => $ano,'MES' => $mes,'MESANO' => $MESANO, 'CODCGA' => $CODCGA, 'CODGRU' => $codgru, 'CODUNN' => $CODUNN,'ANALIT'=>$ANALIT,'SINTET' => $SINTET,'VALORFOR' =>  $valor, 'user_id'=> Auth::user()->id , 'created_at'=> $this->created_at ,'updated_at' => $this->updated_at ];
                    }
                    DB::table('ORCLAN_FOR')->insert($items);
                    //$items[] = ['ANO' => $ano,'MES' => $mes,'MESANO' => $MESANO, 'CODCGA' => $CODCGA, 'CODGRU' => $codgru, 'CODUNN' => $CODUNN,'ANALIT'=>$ANALIT,'SINTET' => $SINTET,'VALORFOR' =>  $valor, 'user_id'=> Auth::user()->id , 'created_at'=> $this->created_at ,'updated_at' => $this->updated_at ];
                }
                //dd($items);
            }
        }
*/
        $sql1 = "SELECT DISTINCT CODCGA FROM MENUFILIAL ORDER BY CODCGA ASC";
        $codcga = DB::select($sql1);
        $item = array();
        foreach($codcga as $c){

            foreach($arrayMesNotIn as $mes){
                $MESANO = $mes.'/'.$ano;

                $sqlOrclan = "SELECT PAGCLA.CODGRU,ORCLAN.MESANO,ORCLAN.CODCGA,ORCLAN.CODUNN,ORCLAN.SINTET,ORCLAN.ANALIT,ORCLAN.CMVLRE FROM ORCLAN
                INNER JOIN PAGCLA ON ORCLAN.ANALIT = PAGCLA.CODCLAP WHERE CODCGA = ".$c->CODCGA." AND MESANO = '".$MESANO."' ORDER BY ORCLAN.SINTET ASC";


               // if($c->CODCGA == 12){
                $orclan = DB::connection('sqlsrv')->select($sqlOrclan);

                foreach($orclan as $o){
                //$CMVLRE = $o->CMVLRE;
                //$where = ['ANALIT'=>$o->ANALIT,'CODCGA'=>$o->CODCGA,'CODUNN'=>$o->CODUNN,'MESANO'=>$o->MESANO];
               // $item[] = $where;

                DB::table('ORCLAN_FOR')
                ->where('ANALIT','=',$o->ANALIT)
                ->where('CODCGA','=',$o->CODCGA)
                ->where('CODUNN','=',$o->CODUNN)
                ->where('MESANO','=',$o->MESANO)
                ->update(['VALORFOR' => $o->CMVLRE]);


               // dd($o->ANALIT,$o->CODCGA,$o->CODUNN,$o->MESANO);
    /*
    `ANO`, `MES`, `MESANO`, `SINTET`, `ANALIT`, `CODCGA`, `CODUNN`, `CODGRU`, `VALORFOR`, `JUSTIFICATIVA`, `user_id`, `created_at`, `updated_at`
    +"CODGRU": "8"
    +"MESANO": "01/2019"
    +"CODCGA": "9"
    +"CODUNN": "21"
    +"SINTET": "5"
    +"ANALIT": "10"
    +"CMVLRE": ".00"
    */
                }
               // dd($orclan);
            //}
                //dd($sqlOrclan,$orclan);

            }

        }
        //dd($item);


        dd('PROCESSO FINALIZADO');
    }

    public function index(Request $request){

        return view('forecast.index');
    }

    public function menu(Request $request){

        return view('forecast.menu');
    }

    public function importar(Request $request){

        $file_path = public_path("uploads/teste.xlsx");

        $array = Excel::toArray(new ItemImporta, $file_path);

        $collection = Excel::toCollection(new ItemImporta, $file_path);


        $import = new MultImport();
        $import->onlySheets('Worksheet1', 'Worksheet2');

        $teste  = Excel::toArray($import, $file_path);

        dd($array,$collection,$teste);

    }

    public function menupacote(Request $request){
        View::share ( 'subMenu', '<li><a href="'.route('forecast/menufilial').'">Centro de Gasto</a></li><li>Pacote</li>' );
        $CODCGA = '';
        $CODUNN = '';
        $id =  Auth::user()->id;
        $usuario_pacote  = DB::table('USU_PACOTE')->select('CODIGO')->where('ID_USUARIO','=',$id)->pluck('CODIGO')->toArray();

        $listaPacote = DB::table('MENUPACOTE')->select('MENUPACOTE.CODIGO','RODGRU.DESCRI')->join('RODGRU','MENUPACOTE.CODIGO','=','RODGRU.CODIGO')->get();


        if($request->CODCGA && $request->CODUNN){
            $unidadeFilial = (array) DB::table('MENUFILIAL')
            ->select('MENUFILIAL.*','RODCGA.DESCRI AS CENTRODEGASTO','RODUNN.DESCRI AS UNIDADE')
            ->join('RODCGA','MENUFILIAL.CODCGA','=','RODCGA.CODCGA')
            ->join('RODUNN','MENUFILIAL.CODUNN','=','RODUNN.CODUNN')
            ->where('MENUFILIAL.CODCGA', '=', $request->CODCGA)
            ->where('MENUFILIAL.CODUNN', '=', $request->CODUNN)
            ->first();
            $CODCGA = $request->CODCGA;
            $CODUNN = $request->CODUNN;
           //dd($unidadeFilial);
        }


        return view('forecast.menupacote',compact('unidadeFilial','CODCGA','CODUNN','listaPacote','usuario_pacote'));
    }

    public function menufilial(Request $request){

        View::share ( 'subMenu', '<li>Centro de Gasto</li>' );

        $id =  Auth::user()->id;
        $usuario_codcga  = DB::table('USU_CGA')->select('CODCGA')->where('ID_USUARIO','=',$id)->pluck('CODCGA')->toArray();
        $usuario_unidade = DB::table('USU_UNIDADE')->select('CODUNN')->where('ID_USUARIO','=',$id)->pluck('CODUNN')->toArray();
        $usuario_pacote  = DB::table('USU_PACOTE')->select('CODIGO')->where('ID_USUARIO','=',$id)->pluck('CODIGO')->toArray();

        $listaFilial = DB::table('MENUFILIAL')
        ->select('MENUFILIAL.CODCGA','RODCGA.DESCRI AS CENTRODEGASTO')
        ->join('RODCGA','MENUFILIAL.CODCGA','=','RODCGA.CODCGA')
        ->whereIn('MENUFILIAL.CODCGA', $usuario_codcga)
        ->distinct()->get();
        //dd($listaFilial);

        $listaUnidade = DB::table('MENUFILIAL')
        ->select('MENUFILIAL.*','RODCGA.DESCRI AS CENTRODEGASTO','RODUNN.DESCRI AS UNIDADE')
        ->join('RODCGA','MENUFILIAL.CODCGA','=','RODCGA.CODCGA')
        ->join('RODUNN','MENUFILIAL.CODUNN','=','RODUNN.CODUNN')
        ->whereIn('MENUFILIAL.CODUNN', $usuario_unidade)
        ->get();

        return view('forecast.menufilial',compact('listaFilial','listaUnidade'));
    }

    public function pacote(Request $request){


        $analitBoqueia = DB::table('ANALIT_AUTO')->select('ANALIT')->pluck('ANALIT')->toArray();
        //dd($analitBoqueia);

        $arrayMes = $this->arrayMes;

        $arrayMesNotIn = array(0=>'01',1=>'02',2=>'03',3=>'04',4=>'05',5=>'06');

        $codgru = $request->codgru;
        $CODCGA = $request->CODCGA;
        $CODUNN = $request->CODUNN;

        $orc = DB::table('ORCAMENTO')->select('ano')->where('ativo','=','S')->first();
        $ano = $orc->ano;

        if(isset($_REQUEST['salvar'])){ // PEGA DADOS POSTADOS


            $del = ['ANO' => $ano,'CODCGA' => $CODCGA, 'CODGRU' => $codgru, 'CODUNN' => $CODUNN];
            //DB::table('ORCLAN_FOR')->where($del)->whereNotIn('MES',$arrayMesNotIn)->whereNotIn('ANALIT',$analitBoqueia)->delete();
            DB::table('ORCLAN_FOR')->where($del)->whereNotIn('MES',$arrayMesNotIn)->delete();
            DB::table('JUSTIFICATIVA')->where($del)->delete();


            $v = $request->all();
            $items = array();
            $jus = array();
            $justificativa = '';

            foreach ($v as $key => $value){

                $r = explode('_',$key);

                if($r[0] == 'just'){
                    $justificativa = $value; // pega a justificativa
                    $analit_jus = $r[1];
                    $jus[] = ['ANO' => $ano,'CODCGA' => $CODCGA, 'CODGRU' => $codgru, 'CODUNN' => $CODUNN,'ANALIT'=>$analit_jus,'TEXTO'=>$justificativa, 'user_id'=> Auth::user()->id , 'created_at'=> $this->created_at ,'updated_at' => $this->updated_at];
                }

                if(array_key_exists($r[0],$arrayMes)){ // VERIFICA ITEM E MES

                    $mes = $arrayMes[$r[0]];
                    $ANALIT = $r[1];
                    $SINTET = $r[2];
                    $MESANO = $mes."/".$ano;

                    $valor = str_replace(',','.', str_replace('.','', $value));

                    $items[] = ['ANO' => $ano,'MES' => $mes,'MESANO' => $MESANO, 'CODCGA' => $CODCGA, 'CODGRU' => $codgru, 'CODUNN' => $CODUNN,'ANALIT'=>$ANALIT,'SINTET' => $SINTET,'VALORFOR' =>  $valor, 'user_id'=> Auth::user()->id , 'created_at'=> $this->created_at ,'updated_at' => $this->updated_at ];

                }
            }

       // dd($items);
        DB::table('ORCLAN_FOR')->insert($items);
        DB::table('JUSTIFICATIVA')->insert($jus);
        //dd($jus);

        }

        $unidadeFilial = DB::table('MENUFILIAL')
        ->select('MENUFILIAL.*','RODCGA.DESCRI AS CENTRODEGASTO','RODUNN.DESCRI AS UNIDADE')
        ->join('RODCGA','MENUFILIAL.CODCGA','=','RODCGA.CODCGA')
        ->join('RODUNN','MENUFILIAL.CODUNN','=','RODUNN.CODUNN')
        ->where('MENUFILIAL.CODCGA', '=', $request->CODCGA)
        ->where('MENUFILIAL.CODUNN', '=', $request->CODUNN)
        ->first();
        //echo "SELECT PC.PAICLAP FROM PAGCLA PC WHERE PC.CODGRU = ".$codgru." AND PC.SITUAC = 'A' GROUP BY PC.PAICLAP";
        //$listaSintet = DB::select("SELECT PC.PAICLAP FROM PAGCLA PC WHERE PC.CODGRU = ".$codgru." AND PC.SITUAC = 'A' GROUP BY PC.PAICLAP");
        //dd($listaSintet);


        $pacote = DB::table('RODGRU')->select('DESCRI')->where('CODIGO','=',$codgru)->first();
        $sqlSintet = "SELECT P.CODCLAP,P.DESCRI,
		IFNULL((SELECT SUM(C.VALORFOR) FROM ORCLAN_FOR C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.CODGRU = ".$codgru." AND C.MES = '01' AND P.CODCLAP = C.SINTET),0) AS VAL1,
        IFNULL((SELECT SUM(C.VALORFOR) FROM ORCLAN_FOR C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.CODGRU = ".$codgru." AND C.MES = '02' AND P.CODCLAP = C.SINTET),0) AS VAL2,
        IFNULL((SELECT SUM(C.VALORFOR) FROM ORCLAN_FOR C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.CODGRU = ".$codgru." AND C.MES = '03' AND P.CODCLAP = C.SINTET),0) AS VAL3,
        IFNULL((SELECT SUM(C.VALORFOR) FROM ORCLAN_FOR C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.CODGRU = ".$codgru." AND C.MES = '04' AND P.CODCLAP = C.SINTET),0) AS VAL4,
        IFNULL((SELECT SUM(C.VALORFOR) FROM ORCLAN_FOR C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.CODGRU = ".$codgru." AND C.MES = '05' AND P.CODCLAP = C.SINTET),0) AS VAL5,
        IFNULL((SELECT SUM(C.VALORFOR) FROM ORCLAN_FOR C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.CODGRU = ".$codgru." AND C.MES = '06' AND P.CODCLAP = C.SINTET),0) AS VAL6,
        IFNULL((SELECT SUM(C.VALORFOR) FROM ORCLAN_FOR C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.CODGRU = ".$codgru." AND C.MES = '07' AND P.CODCLAP = C.SINTET),0) AS VAL7,
        IFNULL((SELECT SUM(C.VALORFOR) FROM ORCLAN_FOR C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.CODGRU = ".$codgru." AND C.MES = '08' AND P.CODCLAP = C.SINTET),0) AS VAL8,
        IFNULL((SELECT SUM(C.VALORFOR) FROM ORCLAN_FOR C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.CODGRU = ".$codgru." AND C.MES = '09' AND P.CODCLAP = C.SINTET),0) AS VAL9,
        IFNULL((SELECT SUM(C.VALORFOR) FROM ORCLAN_FOR C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.CODGRU = ".$codgru." AND C.MES = '10' AND P.CODCLAP = C.SINTET),0) AS VAL10,
        IFNULL((SELECT SUM(C.VALORFOR) FROM ORCLAN_FOR C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.CODGRU = ".$codgru." AND C.MES = '11' AND P.CODCLAP = C.SINTET),0) AS VAL11,
        IFNULL((SELECT SUM(C.VALORFOR) FROM ORCLAN_FOR C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.CODGRU = ".$codgru." AND C.MES = '12' AND P.CODCLAP = C.SINTET),0) AS VAL12
        FROM PAGCLA P WHERE P.CODCLAP IN(SELECT PC.PAICLAP FROM PAGCLA PC WHERE PC.CODGRU = ".$codgru." AND PC.SITUAC = 'A' GROUP BY PC.PAICLAP)";

         //dd($sqlSintet);
        //$sqlAnalit = "SELECT P.*, IFNULL(F.VALORFOR,'00,0') AS VAL FROM PAGCLA P
        //              LEFT JOIN ORCLAN_FOR F ON P.CODGRU = F.CODGRU
        //              WHERE P.CODGRU = ".$codgru." AND F.CODGRU = ".$codgru." AND P.SITUAC = 'A' AND F.ANO = $ano AND F.CODCGA = $CODCGA AND F.CODUNN  = $CODUNN ORDER BY P.PAICLAP ASC;";

         $sqlAnalit = "SELECT P.*,J.TEXTO,
         IFNULL((SELECT C.VALORFOR FROM ORCLAN_FOR C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.CODGRU = ".$codgru." AND C.MES = '01' AND P.CODCLAP = C.ANALIT),'0.00') AS VAL1,
         IFNULL((SELECT C.VALORFOR FROM ORCLAN_FOR C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.CODGRU = ".$codgru." AND C.MES = '02' AND P.CODCLAP = C.ANALIT),'0.00') AS VAL2,
         IFNULL((SELECT C.VALORFOR FROM ORCLAN_FOR C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.CODGRU = ".$codgru." AND C.MES = '03' AND P.CODCLAP = C.ANALIT),'0.00') AS VAL3,
         IFNULL((SELECT C.VALORFOR FROM ORCLAN_FOR C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.CODGRU = ".$codgru." AND C.MES = '04' AND P.CODCLAP = C.ANALIT),'0.00') AS VAL4,
         IFNULL((SELECT C.VALORFOR FROM ORCLAN_FOR C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.CODGRU = ".$codgru." AND C.MES = '05' AND P.CODCLAP = C.ANALIT),'0.00') AS VAL5,
         IFNULL((SELECT C.VALORFOR FROM ORCLAN_FOR C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.CODGRU = ".$codgru." AND C.MES = '06' AND P.CODCLAP = C.ANALIT),'0.00') AS VAL6,
         IFNULL((SELECT C.VALORFOR FROM ORCLAN_FOR C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.CODGRU = ".$codgru." AND C.MES = '07' AND P.CODCLAP = C.ANALIT),'0.00') AS VAL7,
         IFNULL((SELECT C.VALORFOR FROM ORCLAN_FOR C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.CODGRU = ".$codgru." AND C.MES = '08' AND P.CODCLAP = C.ANALIT),'0.00') AS VAL8,
         IFNULL((SELECT C.VALORFOR FROM ORCLAN_FOR C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.CODGRU = ".$codgru." AND C.MES = '09' AND P.CODCLAP = C.ANALIT),'0.00') AS VAL9,
         IFNULL((SELECT C.VALORFOR FROM ORCLAN_FOR C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.CODGRU = ".$codgru." AND C.MES = '10' AND P.CODCLAP = C.ANALIT),'0.00') AS VAL10,
         IFNULL((SELECT C.VALORFOR FROM ORCLAN_FOR C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.CODGRU = ".$codgru." AND C.MES = '11' AND P.CODCLAP = C.ANALIT),'0.00') AS VAL11,
         IFNULL((SELECT C.VALORFOR FROM ORCLAN_FOR C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.CODGRU = ".$codgru." AND C.MES = '12' AND P.CODCLAP = C.ANALIT),'0.00') AS VAL12
         FROM PAGCLA P
         LEFT JOIN JUSTIFICATIVA J ON P.CODCLAP = J.ANALIT AND J.CODCGA = ".$CODCGA." AND J.CODUNN = ".$CODUNN." AND  J.ANO =  ".$ano."
         WHERE P.CODGRU = ".$codgru." AND P.SITUAC = 'A'  ORDER BY P.PAICLAP ASC";

        //$sqlAnalit = "SELECT PC.*,CUS.CODCUS,CUS.DESCRI AS CUSTO FROM PAGCLA PC INNER JOIN RODCUS CUS ON CUS.CODGRU = PC.CODGRU WHERE PC.CODGRU = ".$codgru." AND PC.SITUAC = 'A' AND CUS.SITUAC = 'A' ORDER BY PC.PAICLAP ASC";

        $pacoteAnaliticas = DB::select($sqlAnalit);
        $pacoteSinteticas = DB::select($sqlSintet);
        //$pacoteAnaliticas = DB::table('PAGCLA')->select('*')->where('CODGRU','=',$codgru)->where('SITUAC','=','A')->orderBy('PAICLAP','ASC')->get();
        //dd($sqlAnalit,$pacoteSinteticas,$pacoteAnaliticas);
        View::share ( 'subMenu', '<li><a href="'.route('forecast/menufilial').'">Centro de Gasto</a></li><li>Pacote</li>' );
        return view('forecast.pacote',['analitBoqueia'=>$analitBoqueia,'pacoteSinteticas'=>$pacoteSinteticas,'pacoteAnaliticas'=>$pacoteAnaliticas,'pacote'=>$pacote->DESCRI,'CODCGA'=>$CODCGA,'CODUNN'=>$CODUNN,'codgru'=>$codgru,'centrodegasto'=>$unidadeFilial->CENTRODEGASTO,'unidade'=>$unidadeFilial->UNIDADE]);
    }

    public function variaveis(Request $request){

        $arrayMes = $this->arrayMes;

        $codgru = $request->codgru;
        $CODCGA = $request->CODCGA;
        $CODUNN = $request->CODUNN;

        $funcoes = DB::table('VARIAVEIS_ITENS')->select('*')->where(['TIPO'=>'F','CODUNN'=>$CODUNN])->orderBy('ORDEMFUNC')->get();
        $orc = DB::table('ORCAMENTO')->select('ano')->where('ativo','=','S')->first();
        $ano = $orc->ano;

        if(isset($_REQUEST['salvar'])){
            $v = $request->all();

            //dd($v);
            DB::table('VARIAVEIS_CAD')->where(['ANO' => $ano, 'CODCGA' => $CODCGA, 'CODUNN' => $CODUNN])->delete();

            $items = array();
            //$t = 0;

            foreach ($v as $key => $value){
             /*
             echo "<pre>";
             var_dump($key,$value);
             echo "</pre>";
             */
                $r = explode('_',$key);
                if(array_key_exists($r[0],$arrayMes)){

                    $mes = $arrayMes[$r[0]];
                    $item = $r[1];
                    $valor = str_replace(',','.', str_replace('.','', $value));

                    $items[] = ['ANO' => $ano,'MES' => $mes, 'CODCGA' => $CODCGA, 'CODUNN' => $CODUNN,'ITEN'=>$item,'VALOR' =>  $valor, 'user_id'=> Auth::user()->id , 'created_at'=> $this->created_at ,'updated_at' => $this->updated_at ];

                   // DB::table('RECEITA_ROTA_CAD')->where(['ANO' => $ano,'MES' => $mes, 'CODCGA' => $CODCGA, 'CODUNN' => $CODUNN,'ITEN'=>$item])->delete();

                  //  DB::table('RECEITA_ROTA_CAD')->insert(
                  //      ['ANO' => $ano,'MES' => $mes, 'CODCGA' => $CODCGA, 'CODUNN' => $CODUNN,'ITEN'=>$item,'VALOR' =>  $valor, 'user_id'=> Auth::user()->id , 'created_at'=> $this->created_at ,'updated_at' => $this->updated_at ]
                  //  );
/*                    DB::table('RECEITA_ROTA_CAD')
                    ->updateOrInsert(
                        ['ANO' => $ano,'MES' => $mes, 'CODCGA' => $CODCGA, 'CODUNN' => $CODUNN,'ITEN'=>$item],
                        ['VALOR' =>  $valor, 'user_id'=> Auth::user()->id , 'created_at'=> $this->created_at ,'updated_at' => $this->updated_at ]
                    );
*/
                 //   if(count($funcoes)>0){
                 //       $this->aplicaFuncao($funcoes,$ano,$mes,$CODCGA,$CODUNN);
                //    }

                }




            }
            DB::table('VARIAVEIS_CAD')->insert($items);

            if(count($funcoes)>0){
                foreach ($arrayMes as $key => $value){
                    $mes = $value;
                    $this->aplicaFuncao($funcoes,$ano,$mes,$CODCGA,$CODUNN);
                }
            }






            /*
             echo "<pre>";
             var_dump($valor);
             echo "</pre>";
             */
        }
        //   dd($request);



        $unidadeFilial = DB::table('MENUFILIAL')
        ->select('MENUFILIAL.*','RODCGA.DESCRI AS CENTRODEGASTO','RODUNN.DESCRI AS UNIDADE')
        ->join('RODCGA','MENUFILIAL.CODCGA','=','RODCGA.CODCGA')
        ->join('RODUNN','MENUFILIAL.CODUNN','=','RODUNN.CODUNN')
        ->where('MENUFILIAL.CODCGA', '=', $request->CODCGA)
        ->where('MENUFILIAL.CODUNN', '=', $request->CODUNN)
        ->first();

        $pacote = DB::table('RODGRU')->select('DESCRI')->where('CODIGO','=',$codgru)->first();
/*
        $sqlSintet = "SELECT P.ID,P.DESCRI FROM VARIAVEIS_ITENS P WHERE P.ID IN(SELECT PC.IDPAI FROM VARIAVEIS_ITENS PC WHERE PC.CODUNN = ".$CODUNN." GROUP BY PC.IDPAI);";
        $sqlAnalit = "SELECT * FROM VARIAVEIS_ITENS I
                      LEFT JOIN  VARIAVEIS_CAD C ON  I.ID = C.ITEN
                      WHERE I.CODUNN = ".$CODUNN." AND C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO = ".$ano." ORDER BY I.ORDEM ASC;";
*/


/*
$sqlSintet = "SELECT P.ID,P.DESCRI FROM VARIAVEIS_ITENS P WHERE P.ID IN(SELECT PC.IDPAI FROM VARIAVEIS_ITENS PC GROUP BY PC.IDPAI);";
$sqlAnalit = "SELECT * FROM VARIAVEIS_ITENS I
              LEFT JOIN  VARIAVEIS_CAD C ON  I.ID = C.ITEN
              WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO = ".$ano." ORDER BY I.ORDEM ASC;";

dd($sqlSintet,$sqlAnalit);
*/
        //$sqlAnalit = "SELECT PC.*,CUS.CODCUS,CUS.DESCRI AS CUSTO FROM PAGCLA PC INNER JOIN RODCUS CUS ON CUS.CODGRU = PC.CODGRU WHERE PC.CODGRU = ".$codgru." AND PC.SITUAC = 'A' AND CUS.SITUAC = 'A' ORDER BY PC.PAICLAP ASC";
        $sqlAnalit = "SELECT I.* ,
        IFNULL((SELECT C.VALOR FROM VARIAVEIS_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '01' and I.ID = C.ITEN),'0.00') AS VAL1,
        IFNULL((SELECT C.VALOR FROM VARIAVEIS_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '02' and I.ID = C.ITEN),'0.00') AS VAL2,
        IFNULL((SELECT C.VALOR FROM VARIAVEIS_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '03' and I.ID = C.ITEN),'0.00') AS VAL3,
        IFNULL((SELECT C.VALOR FROM VARIAVEIS_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '04' and I.ID = C.ITEN),'0.00') AS VAL4,
        IFNULL((SELECT C.VALOR FROM VARIAVEIS_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '05' and I.ID = C.ITEN),'0.00') AS VAL5,
        IFNULL((SELECT C.VALOR FROM VARIAVEIS_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '06' and I.ID = C.ITEN),'0.00') AS VAL6,
        IFNULL((SELECT C.VALOR FROM VARIAVEIS_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '07' and I.ID = C.ITEN),'0.00') AS VAL7,
        IFNULL((SELECT C.VALOR FROM VARIAVEIS_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '08' and I.ID = C.ITEN),'0.00') AS VAL8,
        IFNULL((SELECT C.VALOR FROM VARIAVEIS_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '09' and I.ID = C.ITEN),'0.00') AS VAL9,
        IFNULL((SELECT C.VALOR FROM VARIAVEIS_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '10' and I.ID = C.ITEN),'0.00') AS VAL10,
        IFNULL((SELECT C.VALOR FROM VARIAVEIS_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '11' and I.ID = C.ITEN),'0.00') AS VAL11,
        IFNULL((SELECT C.VALOR FROM VARIAVEIS_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '12' and I.ID = C.ITEN),'0.00') AS VAL12
         FROM VARIAVEIS_ITENS I ORDER BY I.ORDEM ASC";
//echo $sqlAnalit;
//dd();
        $sqlv = "SELECT * FROM UND_VARIAVEIS WHERE SITUAC = 'A' ORDER BY ID ASC";

        $pacoteAnaliticas = DB::select($sqlAnalit);
        $pacoteSinteticas = DB::select($sqlv);

        //$pacoteAnaliticas = DB::table('PAGCLA')->select('*')->where('CODGRU','=',$codgru)->where('SITUAC','=','A')->orderBy('PAICLAP','ASC')->get();
       // dd($pacoteSinteticas,$pacoteAnaliticas);
       // View::share ( 'subMenu', '<li>Centro de Gasto</li><li>Pacote</li><li>Metas</li>' );
        View::share ( 'subMenu', '<li><a href="'.route('forecast/menufilial').'">Centro de Gasto</a></li><li>Pacote</li>' );
        return view('forecast.variaveis',['pacoteSinteticas'=>$pacoteSinteticas,'pacoteAnaliticas'=>$pacoteAnaliticas,'pacote'=>$pacote->DESCRI,'CODCGA'=>$CODCGA,'CODUNN'=>$CODUNN,'codgru'=>$codgru,'centrodegasto'=>$unidadeFilial->CENTRODEGASTO,'unidade'=>$unidadeFilial->UNIDADE]);
    }


    public function receitarota(Request $request){

        $arrayMes = $this->arrayMes;

        $codgru = $request->codgru;
        $CODCGA = $request->CODCGA;
        $CODUNN = $request->CODUNN;

        $funcoes = DB::table('RECEITA_ROTA_ITENS')->select('*')->where(['TIPO'=>'F','CODUNN'=>$CODUNN])->orderBy('ORDEMFUNC')->get();
        $orc = DB::table('ORCAMENTO')->select('ano')->where('ativo','=','S')->first();
        $ano = $orc->ano;

        if(isset($_REQUEST['salvar'])){
            $v = $request->all();

            //dd($v);
            DB::table('RECEITA_ROTA_CAD')->where(['ANO' => $ano, 'CODCGA' => $CODCGA, 'CODUNN' => $CODUNN])->delete();

            $items = array();
            //$t = 0;

            foreach ($v as $key => $value){
             /*
             echo "<pre>";
             var_dump($key,$value);
             echo "</pre>";
             */
                $r = explode('_',$key);
                if(array_key_exists($r[0],$arrayMes)){

                    $mes = $arrayMes[$r[0]];
                    $item = $r[1];
                    $valor = str_replace(',','.', str_replace('.','', $value));

                    $items[] = ['ANO' => $ano,'MES' => $mes, 'CODCGA' => $CODCGA, 'CODUNN' => $CODUNN,'ITEN'=>$item,'VALOR' =>  $valor, 'user_id'=> Auth::user()->id , 'created_at'=> $this->created_at ,'updated_at' => $this->updated_at ];

                   // DB::table('RECEITA_ROTA_CAD')->where(['ANO' => $ano,'MES' => $mes, 'CODCGA' => $CODCGA, 'CODUNN' => $CODUNN,'ITEN'=>$item])->delete();

                  //  DB::table('RECEITA_ROTA_CAD')->insert(
                  //      ['ANO' => $ano,'MES' => $mes, 'CODCGA' => $CODCGA, 'CODUNN' => $CODUNN,'ITEN'=>$item,'VALOR' =>  $valor, 'user_id'=> Auth::user()->id , 'created_at'=> $this->created_at ,'updated_at' => $this->updated_at ]
                  //  );
/*                    DB::table('RECEITA_ROTA_CAD')
                    ->updateOrInsert(
                        ['ANO' => $ano,'MES' => $mes, 'CODCGA' => $CODCGA, 'CODUNN' => $CODUNN,'ITEN'=>$item],
                        ['VALOR' =>  $valor, 'user_id'=> Auth::user()->id , 'created_at'=> $this->created_at ,'updated_at' => $this->updated_at ]
                    );
*/
                 //   if(count($funcoes)>0){
                 //       $this->aplicaFuncao($funcoes,$ano,$mes,$CODCGA,$CODUNN);
                //    }

                }




            }
            DB::table('RECEITA_ROTA_CAD')->insert($items);

            if(count($funcoes)>0){
                foreach ($arrayMes as $key => $value){
                    $mes = $value;
                    $this->aplicaFuncao($funcoes,$ano,$mes,$CODCGA,$CODUNN);
                }
            }






            /*
             echo "<pre>";
             var_dump($valor);
             echo "</pre>";
             */
        }
        //   dd($request);



        $unidadeFilial = DB::table('MENUFILIAL')
        ->select('MENUFILIAL.*','RODCGA.DESCRI AS CENTRODEGASTO','RODUNN.DESCRI AS UNIDADE')
        ->join('RODCGA','MENUFILIAL.CODCGA','=','RODCGA.CODCGA')
        ->join('RODUNN','MENUFILIAL.CODUNN','=','RODUNN.CODUNN')
        ->where('MENUFILIAL.CODCGA', '=', $request->CODCGA)
        ->where('MENUFILIAL.CODUNN', '=', $request->CODUNN)
        ->first();

        $pacote = DB::table('RODGRU')->select('DESCRI')->where('CODIGO','=',$codgru)->first();

        $sqlSintet = "SELECT P.ID,P.DESCRI FROM RECEITA_ROTA_ITENS P WHERE P.ID IN(SELECT PC.IDPAI FROM RECEITA_ROTA_ITENS PC WHERE PC.CODUNN = ".$CODUNN." GROUP BY PC.IDPAI);";
        $sqlAnalit = "SELECT * FROM RECEITA_ROTA_ITENS I
                      LEFT JOIN  RECEITA_ROTA_CAD C ON  I.ID = C.ITEN
                      WHERE I.CODUNN = ".$CODUNN." AND C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO = ".$ano." ORDER BY I.ORDEM ASC;";
        //$sqlAnalit = "SELECT PC.*,CUS.CODCUS,CUS.DESCRI AS CUSTO FROM PAGCLA PC INNER JOIN RODCUS CUS ON CUS.CODGRU = PC.CODGRU WHERE PC.CODGRU = ".$codgru." AND PC.SITUAC = 'A' AND CUS.SITUAC = 'A' ORDER BY PC.PAICLAP ASC";
        $sqlAnalit = "SELECT I.* ,
        IFNULL((SELECT C.VALOR FROM RECEITA_ROTA_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '01' and I.ID = C.ITEN),'0.00') AS VAL1,
        IFNULL((SELECT C.VALOR FROM RECEITA_ROTA_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '02' and I.ID = C.ITEN),'0.00') AS VAL2,
        IFNULL((SELECT C.VALOR FROM RECEITA_ROTA_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '03' and I.ID = C.ITEN),'0.00') AS VAL3,
        IFNULL((SELECT C.VALOR FROM RECEITA_ROTA_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '04' and I.ID = C.ITEN),'0.00') AS VAL4,
        IFNULL((SELECT C.VALOR FROM RECEITA_ROTA_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '05' and I.ID = C.ITEN),'0.00') AS VAL5,
        IFNULL((SELECT C.VALOR FROM RECEITA_ROTA_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '06' and I.ID = C.ITEN),'0.00') AS VAL6,
        IFNULL((SELECT C.VALOR FROM RECEITA_ROTA_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '07' and I.ID = C.ITEN),'0.00') AS VAL7,
        IFNULL((SELECT C.VALOR FROM RECEITA_ROTA_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '08' and I.ID = C.ITEN),'0.00') AS VAL8,
        IFNULL((SELECT C.VALOR FROM RECEITA_ROTA_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '09' and I.ID = C.ITEN),'0.00') AS VAL9,
        IFNULL((SELECT C.VALOR FROM RECEITA_ROTA_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '10' and I.ID = C.ITEN),'0.00') AS VAL10,
        IFNULL((SELECT C.VALOR FROM RECEITA_ROTA_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '11' and I.ID = C.ITEN),'0.00') AS VAL11,
        IFNULL((SELECT C.VALOR FROM RECEITA_ROTA_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '12' and I.ID = C.ITEN),'0.00') AS VAL12
        FROM RECEITA_ROTA_ITENS I WHERE I.CODUNN = ".$CODUNN." ORDER BY I.ORDEM ASC";

        $pacoteAnaliticas = DB::select($sqlAnalit);
        $pacoteSinteticas = DB::select($sqlSintet);
        //$pacoteAnaliticas = DB::table('PAGCLA')->select('*')->where('CODGRU','=',$codgru)->where('SITUAC','=','A')->orderBy('PAICLAP','ASC')->get();
        //dd($pacoteSinteticas,$pacoteAnaliticas);
       // View::share ( 'subMenu', '<li>Centro de Gasto</li><li>Pacote</li><li>Metas</li>' );
        View::share ( 'subMenu', '<li><a href="'.route('forecast/menufilial').'">Centro de Gasto</a></li><li>Pacote</li>' );
        return view('forecast.receitarota',['pacoteSinteticas'=>$pacoteSinteticas,'pacoteAnaliticas'=>$pacoteAnaliticas,'pacote'=>$pacote->DESCRI,'CODCGA'=>$CODCGA,'CODUNN'=>$CODUNN,'codgru'=>$codgru,'centrodegasto'=>$unidadeFilial->CENTRODEGASTO,'unidade'=>$unidadeFilial->UNIDADE]);
    }

    public function aplicaFuncao($funcoes,$ano,$mes,$CODCGA,$CODUNN){

        foreach($funcoes as $f){

            $func = explode(';',$f->OBJTIPO);

            if(count($func) > 1){

                $funcao = array_shift($func);
                $parametros = $func;

                    if($funcao == 'mult'){

                        $q =  ['ANO' => $ano,'MES' => $mes, 'CODCGA' => $CODCGA, 'CODUNN' => $CODUNN];
                        $q3 = ['ANO' => $ano,'MES' => $mes, 'CODCGA' => $CODCGA, 'CODUNN' => $CODUNN,'ITEN'=>$f->ID];

                        $v  = DB::table('RECEITA_ROTA_CAD')->select(DB::raw('round(EXP(SUM(LOG(VALOR))),2) AS total'))->where($q)->whereIn('ITEN',$parametros)->first();

                        DB::table('RECEITA_ROTA_CAD')
                        ->where($q3)
                        ->update(['VALOR' => $v->total]);

                    }
                    elseif($funcao = 'multp'){

                        $q =  ['ANO' => $ano,'MES' => $mes, 'CODCGA' => $CODCGA, 'CODUNN' => $CODUNN];
                        $q3 = ['ANO' => $ano,'MES' => $mes, 'CODCGA' => $CODCGA, 'CODUNN' => $CODUNN,'ITEN'=>$f->ID];

                        $p1 = ['ANO' => $ano,'MES' => $mes, 'CODCGA' => $CODCGA, 'CODUNN' => $CODUNN,'ITEN'=>$parametros[0]];
                        $p2 = ['ANO' => $ano,'MES' => $mes, 'CODCGA' => $CODCGA, 'CODUNN' => $CODUNN,'ITEN'=>$parametros[1]];
                        $p3 = ['ANO' => $ano,'MES' => $mes, 'CODCGA' => $CODCGA, 'CODUNN' => $CODUNN,'ITEN'=>$parametros[2]];

                        $v1  = DB::table('RECEITA_ROTA_CAD')->select('VALOR')->where($p1)->first();
                        $v2  = DB::table('RECEITA_ROTA_CAD')->select('VALOR')->where($p2)->first();
                        $v3  = DB::table('RECEITA_ROTA_CAD')->select('VALOR')->where($p3)->first();

                        //dd($v1,$p1,$v2,$p2,$v3,$p3);
                        $valor = ($v1->VALOR * $v2->VALOR) * ($v3->VALOR/100);
                        //dd($v1->VALOR,$v2->VALOR,$v3->VALOR,$valor,$f->ID);

                        DB::table('RECEITA_ROTA_CAD')
                        ->where($q3)
                        ->update(['VALOR' => $valor]);

                    }
                    elseif($funcao = 'multp2'){


                    }
            }
       }
    }

    public function operacional(Request $request){
        $CODCGA = $request->CODCGA;
        $CODUNN = $request->CODUNN;
        $codgru = $request->codgru;


        $CODCGA = '';
        $CODUNN = '';

        if($request->CODCGA && $request->CODUNN){
            $unidadeFilial = (array) DB::table('MENUFILIAL')
            ->select('MENUFILIAL.*','RODCGA.DESCRI AS CENTRODEGASTO','RODUNN.DESCRI AS UNIDADE')
            ->join('RODCGA','MENUFILIAL.CODCGA','=','RODCGA.CODCGA')
            ->join('RODUNN','MENUFILIAL.CODUNN','=','RODUNN.CODUNN')
            ->where('MENUFILIAL.CODCGA', '=', $request->CODCGA)
            ->where('MENUFILIAL.CODUNN', '=', $request->CODUNN)
            ->first();
            $CODCGA = $request->CODCGA;
            $CODUNN = $request->CODUNN;
        }
        //View::share ( 'subMenu', '<li>Centro de Gasto</li><li>Pacote</li>' );
        View::share ( 'subMenu', '<li><a href="'.route('forecast/menufilial').'">Centro de Gasto</a></li><li><a href="'.route('forecast/operacional',['CODCGA'=>$CODCGA,'CODUNN'=>$CODUNN,'codgru'=>$codgru]).'">Pacote</a></li>' );
        return view('forecast.menupacotefrota',compact('CODCGA','CODUNN','codgru','unidadeFilial'));
    }

    public function gente(Request $request){
        $CODCGA = $request->CODCGA;
        $CODUNN = $request->CODUNN;
        $codgru = $request->codgru;


        return view('forecast.gente',compact('CODCGA','CODUNN','codgru'));
    }


}
