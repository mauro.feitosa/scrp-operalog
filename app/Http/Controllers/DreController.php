<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use View;
use DB;
use App\Rodcga;
use App\Rodcus;
use App\Rodgru;
use App\Rodunn;
use App\Pagcla;
use Session;
use Carbon\Carbon;
use App\Http\Controllers\Orcamentodre;
use App\Exports\Dreexport;
use Maatwebsite\Excel\Facades\Excel;
use Auth;

class DreController extends Controller
{
    //
    public $arrayQlp = array();
    public $sqlServerDb = 'sqlsrv';
    public $cont = 0;
    public $itemMenu = 17;
    public $created_at = '';
    public $updated_at = '';
    public $arrayMes = array('jan'=>'01','fev'=>'02','mar'=>'03','abr'=>'04','mai'=>'05','jun'=>'06','jul'=>'07','ago'=>'08','set'=>'09','out'=>'10','nov'=>'11','dez'=>'12');
    public $arrayMesF =  array('01'=>'JANEIRO',
                               '02'=>'FEVEREIRO',
                               '03'=>'MARÇO',
                               '04'=>'ABRIL',
                               '05'=>'MAIO',
                               '06'=>'JUNHO',
                               '07'=>'JUNHO',
                               '08'=>'AGOSTO',
                               '09'=>'SETEMBRO',
                               '10'=>'OUTUBRO',
                               '11'=>'NOVEMBRO',
                               '12'=>'DEZEMBRO');

    public function __construct()
    {

        $this->middleware('auth');
        $this->middleware('ativo');
        View::share ( 'itemMenu', $this->itemMenu );
        $this->created_at = Carbon::now()->format('Y-m-d H:i:s');
        $this->updated_at = Carbon::now()->format('Y-m-d H:i:s');

    }

    public function download(Request $request){

       // $tb = DB::table('dreexport')->select('dados')->where(['user_id'=>Auth::user()->id])->first();
        //$sessaoTabela = '';
       // dd($tb->dados);

        //  return view('dre.table',['tabela' => $tb->dados]);

       return Excel::download(new Dreexport(),'dre.xlsx');
    }

    public function index(Request $request){

        //teste
        //dd($_SERVER,gethostbyaddr($_SERVER['REMOTE_ADDR']));
        //View::share ( 'itemMenu', $this->itemMenu );
        //return view('dre.index');

        $orcamento = new Orcamentodre;
        $orcamento->url = 'dre';

        if($request->busca == "busca"){


            if(isset($request->unidade)){
                $orcamentoUnidade = $request->unidade;
            }else{
                $orcamentoUnidade = array();
            }

            if(isset($request->gasto)){
                $orcamentoGasto = $request->gasto;
            }else{
                $orcamentoGasto = array();
            }

            if(isset($request->centrodecusto)){
                $orcamentoCentrodecusto = $request->centrodecusto;
            }else{
                $orcamentoCentrodecusto = array();
            }

            //dd($request);

            $ano = $request->ano;
            $mes = $request->mes;
            $orcamento->gasto = $orcamentoGasto;
            $orcamento->unidade = $orcamentoUnidade;
            $orcamento->centrodecusto = $orcamentoCentrodecusto;
            $orcamento->ano = $ano;
            $orcamento->mes = $mes;
            $orcamento->opcaoview = 'COMPETENCIA';
            $orcamento->opcao = 'I';
            $orcamento->exportar = true;

            //dd($request->ano,$request->mes,$request->gasto,$request->unidade);

            //dd($orcamento->gasto,count($orcamento->gasto),$orcamento->gasto[0]);
            if(is_array( $orcamento->gasto ) && count($orcamento->gasto) == 1){
                $orcamento->fonteDados = 'VW_ORCAMENTO_COMPETENCIA_V6_'.$orcamento->gasto[0];
                $orcamento->fontedadosImp  = 'VW_ORCAMENTO_COMPETENCIA_V6_'.$orcamento->gasto[0];
            }else{
                $orcamento->fonteDados = 'VW_ORCAMENTO_COMPETENCIA_V6';
                $orcamento->fontedadosImp  = 'VW_ORCAMENTO_COMPETENCIA_V6';
            }
            //$orcamento->fonteDados = 'VW_ORCAMENTO_COMPETENCIA_V6';
            //$orcamento->fontedadosImp  = 'VW_ORCAMENTO_COMPETENCIA_V6';
            //dd($orcamento->fonteDados);
            //$orcamento->fonteDados = 'VW_ORCAMENTO_COMPETENCIA_V6';
            //$orcamento->fontedadosImp  = 'VW_ORCAMENTO_COMPETENCIA_V6';

            if(isset($orcamento->gasto[0]) == NULL){
                $orcamentoGasto = 9;
            }else{
                $orcamentoGasto = $orcamento->gasto[0];
            }
            $sqlConf = "SELECT * FROM IMPOSTOS WHERE ano = ".$orcamento->ano." AND filial=".$orcamentoGasto;

            //dd($sqlConf);
            $configImposto = db::select($sqlConf);
            $configuracao = $configImposto[0];

            $orcamento->indiceConfis = $configuracao->confis;
            $orcamento->indicePis = $configuracao->pis;
            $orcamento->indiceIss = $configuracao->indiceiss;
            $orcamento->contribuicaoServ = $configuracao->contribuicao_serv;
            $orcamento->contribuicaoTrans = $configuracao->contribuicao_trans;
            $orcamento->irpjServ = $configuracao->irpj_serv;
            $orcamento->irpjTrans = $configuracao->irpj_trans;
            $orcamento->indiceCPRB = $configuracao->cprb;
            $orcamento->despcorp = $configuracao->despcorp;

        }else{

            $ano = '';
            $mes = [];
            $orcamento->gasto = array();
            $orcamento->unidade = array();
            $orcamento->centrodecusto = array();

        }
        $pagina = '';


        $filtroMes = $orcamento->boxMes('mes',$mes);
        $filtroGasto = $orcamento->boxGasto();
        $filtroUnidade = $orcamento->boxUnidade();
        $filtroCentroDeCusto = $orcamento->boxCentroDeCusto();
        $filtroAno = $orcamento->boxAno($ano);
        $boxFiltro = $orcamento->boxFiltro($pagina,$filtroAno, $filtroMes, $filtroGasto,$filtroUnidade,$filtroCentroDeCusto,$filtroCentroDeCusto,'Relatório Demonstrativo de Resultado - DRE ');

        if($request->busca == "busca"){

            $orcamento->processaItem('(0,154) RECEBIMENTOS',0,154);
            $orcamento->processaItem('(0,439) RECEITA FINANCEIRA',0,439);
            $orcamento->processaItem('(1,136) IMPOSTOS',1,136);
            $orcamento->processaItem('(1,154) RECEBIMENTOS',1,154);
            $orcamento->processaItem('(2,105) PNEUS',2,105);
            $orcamento->processaItem('(2,108) COMBUSTÍVEIS',2,108);
            $orcamento->processaItem('(2,135) OUTRAS DESPESAS COM FROTA',2,135);
            $orcamento->processaItem('(2,145) MÁQUINAS E EQUIPAMENTOS',2,145);
            $orcamento->processaItem('(2,150) CONTRATO DE MANUTENÇÃO',2,150);
            $orcamento->processaItem('(2,16) BENEFÍCIOS - OP',2,163);
            $orcamento->processaItem('(2,244) REMUNERAÇÃO - OP',2,244);
            $orcamento->processaItem('(2,248) MATERIAL DE CONSUMO VEICULAR',2,248);
            $orcamento->processaItem('(2,497) ENCARGOS - OP',2,497);
            $orcamento->processaItem('(2,512) MANUTENÇÃO DE FROTA SEMIPESADA',2,512);
            $orcamento->processaItem('(2,520) MANUTENÇÃO DE FROTA PESADA',2,520);
            $orcamento->processaItem('(2,527) MANUTENÇÃO DE IMPLEMENTOS',2,527);
            $orcamento->processaItem('(3,15) ALUGUEL',3,15);
            $orcamento->processaItem('(3,20) DESPESAS ADMINISTRATIVAS',3,20);
            $orcamento->processaItem('(3,49) LICENCIAMENTOS DE VEÍCULOS',3,49);
            $orcamento->processaItem('(3,106) GERENCIAMENTO DE RISCOS',3,106);
            $orcamento->processaItem('(3,114) SEGURANÇA DO TRABALHO',3,114);
            $orcamento->processaItem('(3,136) IMPOSTOS',3,136);
            $orcamento->processaItem('(3,138) IMOBILIZADOS',3,138);
            $orcamento->processaItem('(3,149) FINANCIAMENTOS DE VEÍCULOS',3,149);
            $orcamento->processaItem('(3,163) BENEFÍCIOS - OP',3,163);
            $orcamento->processaItem('(3,244) REMUNERAÇÃO - OP',3,244);
            $orcamento->processaItem('(3,497) ENCARGOS - OP',3,497);
            $orcamento->processaItem('(3,672) RESCISÕES - OP',3,672);
            $orcamento->processaItem('(4,138) IMOBILIZADOS',4,138);
            $orcamento->processaItem('(5,452) REMUNERAÇÃO SÓCIOS',5,452);
            $orcamento->processaItem('(6,5) CONSULTORIAS',6,5);
            $orcamento->processaItem('(6,7) DESPESAS DE VIAGEM',6,7);
            $orcamento->processaItem('(6,14) TECNOLOGIA DA INFORMAÇÃO',6,14);
            $orcamento->processaItem('(6,15) ALUGUEL',6,15);
            $orcamento->processaItem('(6,20) DESPESAS ADMINISTRATIVAS',6,20);
            $orcamento->processaItem('(6,106) GERENCIAMENTO DE RISCOS',6,106);
            $orcamento->processaItem('(6,107) DESPESAS FINANCEIRAS',6,107);
            $orcamento->processaItem('(6,108) COMBUSTÍVEIS',6,108);
            $orcamento->processaItem('(6,136) IMPOSTOS',6,136);
            $orcamento->processaItem('(6,138) IMOBILIZADOS',6,138);
            $orcamento->processaItem('(6,147) MANUTENÇÃO PREDIAL',6,147);
            $orcamento->processaItem('(6,154) RECEBIMENTOS',6,154);
            $orcamento->processaItem('(6,355) DESPESAS JURÍDICAS',6,355);
            $orcamento->processaItem('(6,435) BENEFÍCIOS - ADM',6,435);
            $orcamento->processaItem('(6,439) RECEITA FINANCEIRA',6,439);
            $orcamento->processaItem('(6,460) DESPESAS COM FROTA LEVE',6,460);
            $orcamento->processaItem('(6,629) REMUNERAÇÃO - ADM',6,629);
            $orcamento->processaItem('(6,677) RESCISÕES - ADM',6,677);
            $orcamento->processaItem('(6,683) ENCARGOS - ADM',6,683);
            $orcamento->processaItem('(7,107) DESPESAS FINANCEIRAS',7,107);
            $orcamento->processaItem('(8,63) EMPRÉSTIMOS',8,63);
            $orcamento->processaItem('(9,136) IMPOSTOS',9,136);
            $orcamento->processaItem('(9,15) TRANSFERÊNCIAS DE NUMERÁRIOS',9,157);
            $orcamento->processaItem('(14,149) (-) FINANCIAMENTOS DE VEÍCULOS',14,149);
            $orcamento->processaItem('(14,115) (-) AQUISIÇÃO DE VEÍCULOS',14,115);
//dd('ok1');

            /*$orcamento->processaItem('RECEBIMENTOS',0, 154);
            $orcamento->processaItem('IMPOSTOS S/ VENDAS',1,645);
            $orcamento->processaItem('COMBUSTÍVEIS',2,108);
            $orcamento->processaItem('FRETES / SERVIÇOS DE TERCEIROS',2,135);
            $orcamento->processaItem('MANUTENÇÃO DE CAMINHÃO E CARROCERIA - MCC',2,671);
            $orcamento->processaItem('MANUTENÇÃO DE CARRETA - MCAR',2,663);
            $orcamento->processaItem('MANUTENÇÃO DE CAVALO - MCAV',2,654);
            $orcamento->processaItem('MATERIAL DE CONSUMO',2,248);
            $orcamento->processaItem('PNEUS',2,105);
            $orcamento->processaItem('EMPILHADEIRAS',2,379);
            $orcamento->processaItem('DESPESAS DE VIAGEM - OP',2,562);
            $orcamento->processaItem('SEGUROS',2,168);
            $orcamento->processaItem('ACESSÓRIOS',3,119);
            $orcamento->processaItem('BENEFÍCIOS - OP',3,509);
            $orcamento->processaItem('CARRINHOS DE ENTREGA',3,145);
            $orcamento->processaItem('E.P.I.s - OP',3,114);
            $orcamento->processaItem('ENCARGOS - OP',3,632);
            $orcamento->processaItem('LICENCIAMENTOS DE VEÍCULOS',3,49);
            $orcamento->processaItem('REMUNERAÇÃO - OP',3, 688);
            $orcamento->processaItem('GERENCIAMENTO DE RISCOS',3,106);
            //$orcamento->processaItem('REMUNERAÇÃO - ADM',3,703);
            $orcamento->processaItem('MEDICINA OCUPACIONAL - OP',3,730);
            $orcamento->processaItem('DESPESAS JURÍDICAS  - OP',4,583);
            $orcamento->processaItem('SEGUROS',4,168);
            $orcamento->processaItem('ALUGUEL',5,15);
            $orcamento->processaItem('DESPESAS COM FROTA LEVE',5,725);
            $orcamento->processaItem('FORMAÇÃO/MOTIVACIONAL-OP',5,528);
            $orcamento->processaItem('MARKETING',5,153);
            $orcamento->processaItem('PREJUIZO',5,307);
            $orcamento->processaItem('MULTAS',5,772);
            $orcamento->processaItem('ALUGUEL',6,15);
            $orcamento->processaItem('BENEFICIOS - ADM',6,519);
            $orcamento->processaItem('CONSULTORIAS',6,5);
            $orcamento->processaItem('DESPESAS ADMINISTRATIVAS',6,20);
            $orcamento->processaItem('DESPESAS DE VIAGEM – ADM',6,573);
            $orcamento->processaItem('IMPOSTOS E TAXAS',6, 136);
            $orcamento->processaItem('DESPESAS JURÍDICAS  - ADM',6,590);
            $orcamento->processaItem('E.P.I.s –ADM',6, 600);
            $orcamento->processaItem('ENCARGOS-ADM',6, 638);
            $orcamento->processaItem('FORMAÇÃO/MOTIVACIONAL- ADM',6,535);
            $orcamento->processaItem('MANUTENÇÃO PREDIAL',6,147);
            $orcamento->processaItem('MEDICINA OCUPACIONAL - ADM',6,733);
            $orcamento->processaItem('REMUNERAÇÃO – ADM',6,703);
            $orcamento->processaItem('TECNOLOGIA DA INFORMAÇÃO',6,14);
            $orcamento->processaItem('DESPESAS COM MOBILIZADO',6,138);
            $orcamento->processaItem('DEPRECIAÇÃO',10, 746);
            $orcamento->processaItem('DESPESAS BANCÁRIAS',7,107);
            $orcamento->processaItem('IMPOSTOS E TAXAS',7,136);
            $orcamento->processaItem('JUROS/ENCARGOS',7,547);
            $orcamento->processaItem('TRANSFERÊNCIAS DE NUMERÁRIOS',7,157);
            //$orcamento->processaItem('RECEBIMENTOS',8,63);
            $orcamento->processaItem('RECEBIMENTOS',8,154);
            $orcamento->processaItem('RECEITA FINANCEIRA',8,720);
            $orcamento->processaItem('RECEITA NÃO OPERACIONAL',8,736);
            $orcamento->processaItem('IMPOSTOS S/ LUCROS',9,646);
            $orcamento->processaItem('(-) FINANCIAMENTOS DE VEÍCULOS',14,149);
            $orcamento->processaItem('(-) AQUISIÇÃO DE VEÍCULOS',14,115);*/
           // dd('fez');
           $orcamento->pegaDados();

           $orcamento->processaGrupo(0,154,'grupo0154');
           $orcamento->processaGrupo(0,439,'grupo0439');
           $orcamento->processaGrupo(1,136,'grupo1136');
           $orcamento->processaGrupo(1,154,'grupo1154');
           $orcamento->processaGrupo(2,105,'grupo2105');
           $orcamento->processaGrupo(2,108,'grupo2108');
           $orcamento->processaGrupo(2,135,'grupo2135');
           $orcamento->processaGrupo(2,145,'grupo2145');
           $orcamento->processaGrupo(2,150,'grupo2150');
           $orcamento->processaGrupo(2,163,'grupo2163');
           $orcamento->processaGrupo(2,244,'grupo2244');
           $orcamento->processaGrupo(2,248,'grupo2248');
           $orcamento->processaGrupo(2,497,'grupo2497');
           $orcamento->processaGrupo(2,512,'grupo2512');
           $orcamento->processaGrupo(2,520,'grupo2520');
           $orcamento->processaGrupo(2,527,'grupo2527');
           $orcamento->processaGrupo(3, 15, 'grupo315');
           $orcamento->processaGrupo(3, 20, 'grupo320');
           $orcamento->processaGrupo(3, 49, 'grupo349');
           $orcamento->processaGrupo(3,106,'grupo3106');
           $orcamento->processaGrupo(3,114,'grupo3114');
           $orcamento->processaGrupo(3,136,'grupo3136');
           $orcamento->processaGrupo(3,138,'grupo3138');
           $orcamento->processaGrupo(3,149,'grupo3149');
           $orcamento->processaGrupo(3,163,'grupo3163');
           $orcamento->processaGrupo(3,244,'grupo3244');
           $orcamento->processaGrupo(3,497,'grupo3497');
           $orcamento->processaGrupo(3,672,'grupo3672');
           $orcamento->processaGrupo(4,138,'grupo4138');
           $orcamento->processaGrupo(5,452,'grupo5452');
           $orcamento->processaGrupo(6,  5,  'grupo65');
           $orcamento->processaGrupo(6,  7,  'grupo67');
           $orcamento->processaGrupo(6, 14, 'grupo614');
           $orcamento->processaGrupo(6, 15, 'grupo615');
           $orcamento->processaGrupo(6, 20, 'grupo620');
           $orcamento->processaGrupo(6,106,'grupo6106');
           $orcamento->processaGrupo(6,107,'grupo6107');
           $orcamento->processaGrupo(6,108,'grupo6108');
           $orcamento->processaGrupo(6,136,'grupo6136');
           $orcamento->processaGrupo(6,138,'grupo6138');
           $orcamento->processaGrupo(6,147,'grupo6147');
           $orcamento->processaGrupo(6,154,'grupo6154');
           $orcamento->processaGrupo(6,355,'grupo6355');
           $orcamento->processaGrupo(6,435,'grupo6435');
           $orcamento->processaGrupo(6,439,'grupo6439');
           $orcamento->processaGrupo(6,460,'grupo6460');
           $orcamento->processaGrupo(6,629,'grupo6629');
           $orcamento->processaGrupo(6,677,'grupo6677');
           $orcamento->processaGrupo(6,683,'grupo6683');
           $orcamento->processaGrupo(7,107,'grupo7107');
           $orcamento->processaGrupo(8, 63, 'grupo863');
           $orcamento->processaGrupo(9,136,'grupo9136');
           $orcamento->processaGrupo(9,157,'grupo9157');
           $orcamento->processaGrupo(14,149,'grupo14149');
           $orcamento->processaGrupo(14,115,'grupo14115');

           /*$orcamento->processaGrupo(0,154,'grupox');
           $orcamento->processaGrupo(1,645,'grupo2');
           $orcamento->processaGrupo(2,108,'grupo3');
           $orcamento->processaGrupo(2,135,'grupo4');
           $orcamento->processaGrupo(2,671,'grupo5');
           $orcamento->processaGrupo(2,663,'grupo6');
           $orcamento->processaGrupo(2,654,'grupo7');
           $orcamento->processaGrupo(2,248,'grupo8');
           $orcamento->processaGrupo(2,105,'grupo9');
           $orcamento->processaGrupo(2,379,'grupo10');
           $orcamento->processaGrupo(2,562,'grupo11');
           $orcamento->processaGrupo(2,168,'grupo12');
           $orcamento->processaGrupo(3,119,'grupo13');
           $orcamento->processaGrupo(3,509,'grupo14');
           $orcamento->processaGrupo(3,145,'grupo15');
           $orcamento->processaGrupo(3,114,'grupo16');
           $orcamento->processaGrupo(3,632,'grupo17');
            $orcamento->processaGrupo(3,49,'grupo18');
           $orcamento->processaGrupo(3,688,'grupo19');
           $orcamento->processaGrupo(3,106,'grupo20');
         //$orcamento->processaGrupo(3,703,'grupo1');
           $orcamento->processaGrupo(3,730,'grupo21');
           $orcamento->processaGrupo(4,583,'grupo22');
           $orcamento->processaGrupo(4,168,'grupo23');
            $orcamento->processaGrupo(5,15,'grupo24');
           $orcamento->processaGrupo(5,725,'grupo25');
           $orcamento->processaGrupo(5,528,'grupo26');
           $orcamento->processaGrupo(5,153,'grupo27');
           $orcamento->processaGrupo(5,307,'grupo28');
           $orcamento->processaGrupo(5,772,'grupo29');
            $orcamento->processaGrupo(6,15,'grupo30');
           $orcamento->processaGrupo(6,519,'grupo31');
             $orcamento->processaGrupo(6,5,'grupo32');
            $orcamento->processaGrupo(6,20,'grupo33');
           $orcamento->processaGrupo(6,573,'grupo34');
           $orcamento->processaGrupo(6,136,'grupo35');
           $orcamento->processaGrupo(6,590,'grupo36');
           $orcamento->processaGrupo(6,600,'grupo37');
           $orcamento->processaGrupo(6,638,'grupo38');
           $orcamento->processaGrupo(6,535,'grupo39');
           $orcamento->processaGrupo(6,147,'grupo40');
           $orcamento->processaGrupo(6,733,'grupo41');
           $orcamento->processaGrupo(6,703,'grupo42');
            $orcamento->processaGrupo(6,14,'grupo43');
           $orcamento->processaGrupo(6,138,'grupo44');
          $orcamento->processaGrupo(10,746,'xxx');
           $orcamento->processaGrupo(7,107,'grupo47');
           $orcamento->processaGrupo(7,136,'grupo48');
           $orcamento->processaGrupo(7,547,'grupo49');
           $orcamento->processaGrupo(7,157,'grupo50');
          //$orcamento->processaGrupo(8,63,'grupo1');
           $orcamento->processaGrupo(8,154,'grupo51');
           $orcamento->processaGrupo(8,720,'grupo52');
           $orcamento->processaGrupo(8,736,'grupo53');
           $orcamento->processaGrupo(9,646,'grupo54');
          $orcamento->processaGrupo(14,149,'grupo45');
          $orcamento->processaGrupo(14,115,'grupo46');*/

           $orcamento->somaGrupoGerencial(0);
           $orcamento->somaGrupoGerencial(1);
           $orcamento->somaGrupoGerencial(2);
           $orcamento->somaGrupoGerencial(3);
           $orcamento->somaGrupoGerencial(4);
           $orcamento->somaGrupoGerencial(5);
           $orcamento->somaGrupoGerencial(6);
           $orcamento->somaGrupoGerencial(7);
           $orcamento->somaGrupoGerencial(8);
           $orcamento->somaGrupoGerencial(9);
           //$orcamento->somaGrupoGerencial(10);
           $orcamento->somaGrupoGerencial(14);

           $orcamento->calculaFormulas();

           //dd(array_sum($orcamento->ABITDAi),array_sum($orcamento->ABITDA),(array_sum($orcamento->ABITDA)-array_sum($orcamento->ABITDAi)));

           $data = '';
           //$openTable = '<table id="table2excel" border="0" class="tableid table table-bordered table-hover">';
           $openTable = '<table id="fixTable" class="table table-bordered">';


               $data.= $openTable;
               $cabecalho = $orcamento->cabeMes();
               $data.= $cabecalho;
               $data.='<tbody>';
               $data.= $orcamento->boxSomaGrupoReceita(0,'( = ) RECEITA OPERACIONAL BRUTA');
               //$data.= $orcamento->boxGrupoReceita(0,154,'grupox');
               $data.= $orcamento->boxGrupoReceita(0,154,'grupo0154');
               $data.= $orcamento->boxGrupoReceita(0,439,'grupo0439');
               //$data.= $orcamento->boxGrupo(1,645,'grupo2');
               //$data.= $orcamento->boxSomaGrupo(9,'( - ) IMPOSTOS S/ LUCROS');
               $data.= $orcamento->boxGrupo(9,136,'grupo9136');
               $data.= $orcamento->boxGrupo(9,157,'grupo9157');
               $data.= $orcamento->boxGrupo(1,136,'grupo1136');
               $data.= $orcamento->boxGrupo(1,154,'grupo1154');
               //$data.= $orcamento->boxGrupo(9,646,'grupo54');
               $data.= $orcamento->boxDeducoes('( - ) DEDUÇÕES SOBRE RECEITA');
               $data.= $orcamento->opLiquido('( = ) RECEITA OPERACIONAL LIQUIDA');
               $data.= $orcamento->boxSomaGrupo(2,'( - ) CUSTO DIRETO VARIÁVEL');
               $data.= $orcamento->boxGrupo(2,105,'grupo2105');
               $data.= $orcamento->boxGrupo(2,108,'grupo2108');
               $data.= $orcamento->boxGrupo(2,135,'grupo2135');
               $data.= $orcamento->boxGrupo(2,145,'grupo2145');
               $data.= $orcamento->boxGrupo(2,150,'grupo2150');
               $data.= $orcamento->boxGrupo(2,163,'grupo2163');
               $data.= $orcamento->boxGrupo(2,244,'grupo2244');
               $data.= $orcamento->boxGrupo(2,248,'grupo2248');
               $data.= $orcamento->boxGrupo(2,497,'grupo2497');
               $data.= $orcamento->boxGrupo(2,512,'grupo2512');
               $data.= $orcamento->boxGrupo(2,520,'grupo2520');
               $data.= $orcamento->boxGrupo(2,527,'grupo2527');
               /*$data.= $orcamento->boxGrupo(2,108,'grupo3');
               $data.= $orcamento->boxGrupo(2,135,'grupo4');
               $data.= $orcamento->boxGrupo(2,671,'grupo5');
               $data.= $orcamento->boxGrupo(2,663,'grupo6');
               $data.= $orcamento->boxGrupo(2,654,'grupo7');
               $data.= $orcamento->boxGrupo(2,248,'grupo8');
               $data.= $orcamento->boxGrupo(2,105,'grupo9');
               $data.= $orcamento->boxGrupo(2,379,'grupo10');
               $data.= $orcamento->boxGrupo(2,562,'grupo11');
               $data.= $orcamento->boxGrupo(2,168,'grupo12');*/
               $data.= $orcamento->boxMargemContribuicao('( % ) MARGEM DE CONTRIBUIÇÃO');
               $data.= $orcamento->boxSomaGrupo(3,'( - ) CUSTO DIRETO FIXO');
               $data.= $orcamento->boxGrupo(3, 15, 'grupo315');
               $data.= $orcamento->boxGrupo(3, 20, 'grupo320');
               $data.= $orcamento->boxGrupo(3, 49, 'grupo349');
               $data.= $orcamento->boxGrupo(3,106,'grupo3106');
               $data.= $orcamento->boxGrupo(3,114,'grupo3114');
               $data.= $orcamento->boxGrupo(3,136,'grupo3136');
               $data.= $orcamento->boxGrupo(3,138,'grupo3138');
               $data.= $orcamento->boxGrupo(3,149,'grupo3149');
               $data.= $orcamento->boxGrupo(3,163,'grupo3163');
               $data.= $orcamento->boxGrupo(3,244,'grupo3244');
               $data.= $orcamento->boxGrupo(3,497,'grupo3497');
               $data.= $orcamento->boxGrupo(3,672,'grupo3672');
               /*$data.= $orcamento->boxGrupo(3,119,'grupo13');
               $data.= $orcamento->boxGrupo(3,509,'grupo14');
               $data.= $orcamento->boxGrupo(3,145,'grupo15');
               $data.= $orcamento->boxGrupo(3,114,'grupo16');
               $data.= $orcamento->boxGrupo(3,632,'grupo17');
               $data.=  $orcamento->boxGrupo(3,49,'grupo18');
               $data.= $orcamento->boxGrupo(3,688,'grupo19');
               $data.= $orcamento->boxGrupo(3,106,'grupo20');
               //$data.= $orcamento->boxGrupo(3,703);
               $data.= $orcamento->boxGrupo(3,730,'grupo21');*/
               $data.= $orcamento->boxSomaGrupo(4,'( - ) CUSTO INDIRETO');
               $data.= $orcamento->boxGrupo(4,138,'grupo4138');
               //$data.= $orcamento->boxGrupo(4,583,'grupo22');
               $data.= $orcamento->boxPassivo(' PASSIVO OPERACIONAL');
               //$data.= $orcamento->boxGrupo(4,168,'grupo23');
               $data.= $orcamento->boxLucroBruto('( = ) LUCRO BRUTO');
               $data.= $orcamento->boxLucroBrutoPerc('( % ) LUCRO BRUTO');
               $data.= $orcamento->boxSomaGrupo(5,'( - ) DESPESAS OPERACIONAIS');
               $data.=  $orcamento->boxGrupo(5,452,'grupo5452');
               /*$data.=  $orcamento->boxGrupo(5,15,'grupo24');
               $data.= $orcamento->boxGrupo(5,725,'grupo25');
               $data.= $orcamento->boxGrupo(5,528,'grupo26');
               $data.= $orcamento->boxGrupo(5,153,'grupo27');
               $data.= $orcamento->boxGrupo(5,307,'grupo28');
               $data.= $orcamento->boxGrupo(5,772,'grupo29');*/
               $data.= $orcamento->boxSomaGrupo(6,'( - ) DESPESAS ADMINISTRATIVAS');
               $data.= $orcamento->boxGrupo(6,  5,  'grupo65');
               $data.= $orcamento->boxGrupo(6,  7,  'grupo67');
               $data.= $orcamento->boxGrupo(6, 14, 'grupo614');
               $data.= $orcamento->boxGrupo(6, 15, 'grupo615');
               $data.= $orcamento->boxGrupo(6, 20, 'grupo620');
               $data.= $orcamento->boxGrupo(6,106,'grupo6106');
               $data.= $orcamento->boxGrupo(6,107,'grupo6107');
               $data.= $orcamento->boxGrupo(6,108,'grupo6108');
               $data.= $orcamento->boxGrupo(6,136,'grupo6136');
               $data.= $orcamento->boxGrupo(6,138,'grupo6138');
               $data.= $orcamento->boxGrupo(6,147,'grupo6147');
               $data.= $orcamento->boxGrupo(6,154,'grupo6154');
               $data.= $orcamento->boxGrupo(6,355,'grupo6355');
               $data.= $orcamento->boxGrupo(6,435,'grupo6435');
               $data.= $orcamento->boxGrupo(6,439,'grupo6439');
               $data.= $orcamento->boxGrupo(6,460,'grupo6460');
               $data.= $orcamento->boxGrupo(6,629,'grupo6629');
               $data.= $orcamento->boxGrupo(6,677,'grupo6677');
               $data.= $orcamento->boxGrupo(6,683,'grupo6683');
               /*$data.=  $orcamento->boxGrupo(6,15,'grupo30');
               $data.= $orcamento->boxGrupo(6,519,'grupo31');
               $data.=   $orcamento->boxGrupo(6,5,'grupo32');
               $data.=  $orcamento->boxGrupo(6,20,'grupo33');
               $data.= $orcamento->boxGrupo(6,573,'grupo34');
               $data.= $orcamento->boxGrupo(6,136,'grupo35');
               $data.= $orcamento->boxGrupo(6,590,'grupo36');
               $data.= $orcamento->boxGrupo(6,600,'grupo37');
               $data.= $orcamento->boxGrupo(6,638,'grupo38');
               $data.= $orcamento->boxGrupo(6,535,'grupo39');
               $data.= $orcamento->boxGrupo(6,147,'grupo40');
               $data.= $orcamento->boxGrupo(6,733,'grupo41');
               $data.= $orcamento->boxGrupo(6,703,'grupo42');
               $data.=  $orcamento->boxGrupo(6,14,'grupo43');
               $data.= $orcamento->boxGrupo(6,138,'grupo44');*/
               $data.= $orcamento->boxABITDA('( = ) EBITDA (Lucro Antes do Juros, Impostos,<br/> Depreciação e AmortIzação)');
               $data.= $orcamento->boxABITDAPerc2('( % ) EBITDA (Lucro Antes do Juros, Impostos,<br/> Depreciação e AmortIzação)');
               $data.= $orcamento->boxGrupo(14,149,'grupo145');
               $data.= $orcamento->boxGrupo(14,115,'grupo146');
               $data.= $orcamento->boxABITDAVei('( = ) EBITDA - FINANCIAMENTOS');
               $data.= $orcamento->boxABITDAPerc2Vei('( % ) EBITDA - FINANCIAMENTOS');
               //$data.= $orcamento->boxSomaGrupo(10,'( - ) DEPRECIAÇÃO');
               //$data.= $orcamento->boxGrupo(10,746);
              // $data.= $orcamento->boxLucroOperacional('( = ) LUCRO OPERACIONAL');
              // $data.= $orcamento->boxLucroOperacionalPerc('( % ) LUCRO OPERACIONAL');
               $data.= $orcamento->boxSomaGrupo(7,'( - ) DESPESAS FINANCEIRAS');
               $data.= $orcamento->boxGrupo(7,107,'grupo7107');
               /*$data.= $orcamento->boxGrupo(7,107,'grupo47');
               $data.= $orcamento->boxGrupo(7,136,'grupo48');
               $data.= $orcamento->boxGrupo(7,547,'grupo49');
               $data.= $orcamento->boxGrupo(7,157,'grupo50');*/
           /*
               if($orcamento->ac == null){
                   $data.= $orcamento->boxDespesasCorporativas('( - ) DESPESAS CORPORATIVAS');
               }else{
                   $data.= $orcamento->boxDespesasCorporativasAC('( - ) DESPESAS CORPORATIVAS');
               }
               */

               //$data.= $orcamento->boxFundoDeReserva('( - ) FUNDO DE RESERVA');
               $data.= $orcamento->boxSomaGrupo(8,'( + ) RECEITA NÃO OPERACIONAL');
               $data.= $orcamento->boxGrupo(8, 63, 'grupo863');
               /*$data.= $orcamento->boxGrupo(8,154,'grupo51');
               $data.= $orcamento->boxGrupo(8,720,'grupo52');
               $data.= $orcamento->boxGrupo(8,736,'grupo53');*/
               //$data.= $orcamento->boxLair('( = ) LAIR ( Lucro Anterior ao Imposto de Renda<br/> e Contribuição Social )');
               //$data.= $orcamento->boxLairPerc('( % )LAIR ( Lucro Anterior ao Imposto de Renda<br/> e Contribuição Social )');

               $data.= $orcamento->boxLucroLiquido('( = ) FLUXO DE CAIXA OPERACIONAL');
               $data.= $orcamento->boxLucroLiquidoPerc('( % ) FLUXO DE CAIXA OPERACIONAL');


               /*
               if($gerente == true){
                   $data.= $orcamento->boxSomaGrupoPositivo(10,'( + ) DEPRECIAÇÃO');
                   $data.= $orcamento->boxInvestimento('( - ) INVESTIMENTOS / PROJETOS');
                   $data.= $orcamento->boxCaixaLivre('( = ) FLUXO DE CAIXA LIVRE');
                   $data.= $orcamento->boxGrupo(14,149);
                   $data.= $orcamento->boxGrupo(14,115);
                   $data.= $orcamento->boxCaixaAcionista('( = ) FLUXO CAIXA DO ACIONISTA');
                   $data.= $orcamento->boxCaixaAcionistaPerc('( % ) FLUXO CAIXA DO ACIONISTA');
               }*/
               $closeTable = $orcamento->rodape().'</tbody></table>';
               $data.= $closeTable;
               //echo $data;

        }else{
            $data = '';
        }

        //$dados2 = preg_replace("/<a[^>]+\>[a-z]+/i", "", $data);
        $dados2 = preg_replace('#<a.*?>.*?</a>#i', '', $data);
        $dados3 = preg_replace('/<img[^>]+\>/i', '', $dados2);

        //DB::table('dreexport')->insert(['user_id'=>Auth::user()->id,'dados'=>$data]);
        DB::table('dreexport')->updateOrInsert(['user_id'=>Auth::user()->id],['dados'=>$dados3]);
        //public $ABITDA = array();
        //public $ABITDAi = array();
        //dd(array_sum($orcamento->ABITDAi),array_sum($orcamento->ABITDA),(array_sum($orcamento->ABITDA)-array_sum($orcamento->ABITDAi)));
        ///dd($boxFiltro);

        View::share ( 'subMenu', '<li><a href="'.route('dre').'">DRE</a></li>' );
        return view('dre.dre',['boxFiltro'=>$boxFiltro,'data'=>$data]);

    }
    //EBITDA
    public function processaebitda(Request $request){


        $time_start = microtime(true);

      // //$ano = Carbon\Carbon::now()->format('Y');
       // $mes = Carbon\Carbon::now()->format('n');
        //$dia = Carbon\Carbon::now()->format('j');

        $ano = Carbon::now()->format('Y');
        $mes = Carbon::now()->format('n');
        $dia = Carbon::now()->format('j');
        $m = array();
        //dd($this->arrayMes);

        if($dia > 10){
            $mes = (int) $mes;
        }else{
            $mes = $mes - 1;
            if($mes == 0){
                $mes = 1;
            }

        }

        if($mes == 1){
            $m[0] = 1;
        }else{
            for($x=1;$x < $mes;$x++ ){
                $m[] = $x;
            }
        }


        DB::connection($this->sqlServerDb)->table('ORCLAN_EBITDA_WORKFLOW')->truncate();

        $filiais = DB::table('MENUFILIAL')->select('MENUFILIAL.CODCGA')->groupBy('MENUFILIAL.CODCGA')->orderBy('MENUFILIAL.CODCGA','ASC')->get();

        foreach($filiais as $f){
        $orcamento = new Orcamentodre;
        $orcamento->url = 'dre';

            //dd($request);
            $ano = 2020;
            $mes = $m;
            $orcamento->gasto = array(0=>$f->CODCGA);
            $orcamento->unidade = array();
            $orcamento->ano = $ano;
            $orcamento->mes = $mes;
            $orcamento->opcaoview = 'COMPETENCIA';
            $orcamento->opcao = 'I';

            //dd($orcamento->gasto,count($orcamento->gasto),$orcamento->gasto[0]);
            if(is_array($orcamento->gasto) && count($orcamento->gasto) == 1){
                $orcamento->fonteDados = 'VW_ORCAMENTO_COMPETENCIA_V6_'.$orcamento->gasto[0];
                $orcamento->fontedadosImp  = 'VW_ORCAMENTO_COMPETENCIA_V6_'.$orcamento->gasto[0];
            }else{
                $orcamento->fonteDados = 'VW_ORCAMENTO_COMPETENCIA_V6';
                $orcamento->fontedadosImp  = 'VW_ORCAMENTO_COMPETENCIA_V6';
            }

            if(isset($orcamento->gasto[0]) == NULL){
                $orcamentoGasto = 9;
            }else{
                $orcamentoGasto = $orcamento->gasto[0];
            }
            $sqlConf = "SELECT * FROM IMPOSTOS WHERE ano = ".$orcamento->ano." AND filial=".$orcamentoGasto;

            //dd($sqlConf);
            $configImposto = db::select($sqlConf);
            $configuracao = $configImposto[0];

            $orcamento->indiceConfis = $configuracao->confis;
            $orcamento->indicePis = $configuracao->pis;
            $orcamento->indiceIss = $configuracao->indiceiss;
            $orcamento->contribuicaoServ = $configuracao->contribuicao_serv;
            $orcamento->contribuicaoTrans = $configuracao->contribuicao_trans;
            $orcamento->irpjServ = $configuracao->irpj_serv;
            $orcamento->irpjTrans = $configuracao->irpj_trans;
            $orcamento->indiceCPRB = $configuracao->cprb;
            $orcamento->despcorp = $configuracao->despcorp;




            $orcamento->processaItem('RECEBIMENTOS',0, 154);
            //$orcamento->processaItem('IMPOSTOS S/ VENDAS',1,645);
            $orcamento->processaItem('IMPOSTOS S/ VENDAS',1,136);
            $orcamento->processaItem('COMBUSTÍVEIS',2,108);
            //$orcamento->processaItem('COMBUSTÍVEIS',2,108);
            $orcamento->processaItem('FRETES / SERVIÇOS DE TERCEIROS',2,135);
            $orcamento->processaItem('MANUTENÇÃO DE CAMINHÃO E CARROCERIA - MCC',2,671);
            $orcamento->processaItem('MANUTENÇÃO DE CARRETA - MCAR',2,663);
            $orcamento->processaItem('MANUTENÇÃO DE CAVALO - MCAV',2,654);
            $orcamento->processaItem('MATERIAL DE CONSUMO',2,248);
            $orcamento->processaItem('PNEUS',2,105);
            $orcamento->processaItem('EMPILHADEIRAS',2,379);
            $orcamento->processaItem('DESPESAS DE VIAGEM - OP',2,562);
            $orcamento->processaItem('SEGUROS',2,168);
            $orcamento->processaItem('ACESSÓRIOS',3,119);
            $orcamento->processaItem('BENEFÍCIOS - OP',3,509);
            $orcamento->processaItem('CARRINHOS DE ENTREGA',3,145);
            $orcamento->processaItem('E.P.I.s - OP',3,114);
            $orcamento->processaItem('ENCARGOS - OP',3,632);
            $orcamento->processaItem('LICENCIAMENTOS DE VEÍCULOS',3,49);
            $orcamento->processaItem('REMUNERAÇÃO - OP',3, 688);
            $orcamento->processaItem('GERENCIAMENTO DE RISCOS',3,106);
            //$orcamento->processaItem('REMUNERAÇÃO - ADM',3,703);
            $orcamento->processaItem('MEDICINA OCUPACIONAL - OP',3,730);
            $orcamento->processaItem('DESPESAS JURÍDICAS  - OP',4,583);
            $orcamento->processaItem('SEGUROS',4,168);
            $orcamento->processaItem('ALUGUEL',5,15);
            $orcamento->processaItem('DESPESAS COM FROTA LEVE',5,725);
            $orcamento->processaItem('FORMAÇÃO/MOTIVACIONAL-OP',5,528);
            $orcamento->processaItem('MARKETING',5,153);
            $orcamento->processaItem('PREJUIZO',5,307);
            $orcamento->processaItem('MULTAS',5,772);
            $orcamento->processaItem('ALUGUEL',6,15);
            $orcamento->processaItem('BENEFICIOS - ADM',6,519);
            $orcamento->processaItem('CONSULTORIAS',6,5);
            $orcamento->processaItem('DESPESAS ADMINISTRATIVAS',6,20);
            $orcamento->processaItem('DESPESAS DE VIAGEM – ADM',6,573);
            $orcamento->processaItem('IMPOSTOS E TAXAS',6, 136);
            $orcamento->processaItem('DESPESAS JURÍDICAS  - ADM',6,590);
            $orcamento->processaItem('E.P.I.s –ADM',6, 600);
            $orcamento->processaItem('ENCARGOS-ADM',6, 638);
            $orcamento->processaItem('FORMAÇÃO/MOTIVACIONAL- ADM',6,535);
            $orcamento->processaItem('MANUTENÇÃO PREDIAL',6,147);
            $orcamento->processaItem('MEDICINA OCUPACIONAL - ADM',6,733);
            $orcamento->processaItem('REMUNERAÇÃO – ADM',6,703);
            $orcamento->processaItem('TECNOLOGIA DA INFORMAÇÃO',6,14);
            $orcamento->processaItem('DESPESAS COM MOBILIZADO',6,138);
            $orcamento->processaItem('DEPRECIAÇÃO',10, 746);
            $orcamento->processaItem('DESPESAS BANCÁRIAS',7,107);
            $orcamento->processaItem('IMPOSTOS E TAXAS',7,136);
            $orcamento->processaItem('JUROS/ENCARGOS',7,547);
            $orcamento->processaItem('TRANSFERÊNCIAS DE NUMERÁRIOS',7,157);
            //$orcamento->processaItem('RECEBIMENTOS',8,63);
            $orcamento->processaItem('RECEBIMENTOS',8,154);
            $orcamento->processaItem('RECEITA FINANCEIRA',8,720);
            $orcamento->processaItem('RECEITA NÃO OPERACIONAL',8,736);
            $orcamento->processaItem('IMPOSTOS S/ LUCROS',9,646);
            $orcamento->processaItem('(-) FINANCIAMENTOS DE VEÍCULOS',14,149);
            $orcamento->processaItem('(-) AQUISIÇÃO DE VEÍCULOS',14,115);
           // dd('fez');
           $orcamento->pegaDados();


           $orcamento->processaGrupo(0,154,'grupox');
           $orcamento->processaGrupo(1,645,'grupo2');
           $orcamento->processaGrupo(2,108,'grupo3');
           $orcamento->processaGrupo(2,135,'grupo4');
           $orcamento->processaGrupo(2,671,'grupo5');
           $orcamento->processaGrupo(2,663,'grupo6');
           $orcamento->processaGrupo(2,654,'grupo7');
           $orcamento->processaGrupo(2,248,'grupo8');
           $orcamento->processaGrupo(2,105,'grupo9');
           $orcamento->processaGrupo(2,379,'grupo10');
           $orcamento->processaGrupo(2,562,'grupo11');
           $orcamento->processaGrupo(2,168,'grupo12');
           $orcamento->processaGrupo(3,119,'grupo13');
           $orcamento->processaGrupo(3,509,'grupo14');
           $orcamento->processaGrupo(3,145,'grupo15');
           $orcamento->processaGrupo(3,114,'grupo16');
           $orcamento->processaGrupo(3,632,'grupo17');
           $orcamento->processaGrupo(3,49,'grupo18');
           $orcamento->processaGrupo(3,688,'grupo19');
           $orcamento->processaGrupo(3,106,'grupo20');
         //$orcamento->processaGrupo(3,703,'grupo1');
           $orcamento->processaGrupo(3,730,'grupo21');
           $orcamento->processaGrupo(4,583,'grupo22');
           $orcamento->processaGrupo(4,168,'grupo23');
           $orcamento->processaGrupo(5,15,'grupo24');
           $orcamento->processaGrupo(5,725,'grupo25');
           $orcamento->processaGrupo(5,528,'grupo26');
           $orcamento->processaGrupo(5,153,'grupo27');
           $orcamento->processaGrupo(5,307,'grupo28');
           $orcamento->processaGrupo(5,772,'grupo29');
           $orcamento->processaGrupo(6,15,'grupo30');
           $orcamento->processaGrupo(6,519,'grupo31');
           $orcamento->processaGrupo(6,5,'grupo32');
           $orcamento->processaGrupo(6,20,'grupo33');
           $orcamento->processaGrupo(6,573,'grupo34');
           $orcamento->processaGrupo(6,136,'grupo35');
           $orcamento->processaGrupo(6,590,'grupo36');
           $orcamento->processaGrupo(6,600,'grupo37');
           $orcamento->processaGrupo(6,638,'grupo38');
           $orcamento->processaGrupo(6,535,'grupo39');
           $orcamento->processaGrupo(6,147,'grupo40');
           $orcamento->processaGrupo(6,733,'grupo41');
           $orcamento->processaGrupo(6,703,'grupo42');
           $orcamento->processaGrupo(6,14,'grupo43');
           $orcamento->processaGrupo(6,138,'grupo44');
           $orcamento->processaGrupo(10,746,'xxx');
           $orcamento->processaGrupo(7,107,'grupo47');
           $orcamento->processaGrupo(7,136,'grupo48');
           $orcamento->processaGrupo(7,547,'grupo49');
           $orcamento->processaGrupo(7,157,'grupo50');
          //$orcamento->processaGrupo(8,63,'grupo1');
           $orcamento->processaGrupo(8,154,'grupo51');
           $orcamento->processaGrupo(8,720,'grupo52');
           $orcamento->processaGrupo(8,736,'grupo53');
           $orcamento->processaGrupo(9,646,'grupo54');
           $orcamento->processaGrupo(14,149,'grupo45');
           $orcamento->processaGrupo(14,115,'grupo46');

           $orcamento->somaGrupoGerencial(0);
           $orcamento->somaGrupoGerencial(1);
           $orcamento->somaGrupoGerencial(2);
           $orcamento->somaGrupoGerencial(3);
           $orcamento->somaGrupoGerencial(4);
           $orcamento->somaGrupoGerencial(5);
           $orcamento->somaGrupoGerencial(6);
           $orcamento->somaGrupoGerencial(7);
           $orcamento->somaGrupoGerencial(8);
           $orcamento->somaGrupoGerencial(9);
           $orcamento->somaGrupoGerencial(10);
           $orcamento->somaGrupoGerencial(14);

           $orcamento->calculaFormulas();

           dump('FILIAL:',$orcamento->gasto,'MES:',$orcamento->mes,array_sum($orcamento->ABITDAi),array_sum($orcamento->ABITDA),(array_sum($orcamento->ABITDA)-array_sum($orcamento->ABITDAi)));

           $dataAtual = Carbon::now()->format('Y-m-d H:i');
           $orcado      = array_sum($orcamento->ABITDAi);
           $realizado   = array_sum($orcamento->ABITDA);
           $disp        = (array_sum($orcamento->ABITDA)-array_sum($orcamento->ABITDAi));

           DB::connection($this->sqlServerDb)->table('ORCLAN_EBITDA_WORKFLOW')->insert(
               ['CODCGA'=>$f->CODCGA,'RECEITA'=>0,'DESPESAS'=>0,'ESTORNO'=>0,'ORCADO'=>$orcado,'DEDUCOES'=>$realizado,'EBITDADISP'=>$disp,'DATATU'=>$dataAtual]
           );

        }

        $time_end = microtime(true);
        $execution_time = ($time_end - $time_start)/60;

        echo '<b>Tempo total de execução:</b> '.$execution_time.' Mins';
        dd();
    }

    public function detalhe(Request $request){

        //echo "Dados Carregados";



        $analit = $request->analit;
        $mes = $request->mes;
        $ano = $request->ano;
        $unigasto = str_replace('x',',',$request->unigasto);
        $unidades = str_replace('x',',',$request->unidades);

        $sql = "SELECT RODCLI.RAZSOC AS RAZSOC, MES,ANO, NUMDOC,CODFIL,CODUNN,UNIDADE,CODCGA,GASTO,CODCUS,DESCRI,SINTET,SINTETICA,ANALIT,ANALITICA,ANO,MES,VLRINI,VLRREA,OBSERVACAO,COD_PACOTE,PACOTE FROM VW_ORCAMENTO_COMPETENCIA_V6 INNER JOIN RODCLI ON VW_ORCAMENTO_COMPETENCIA_V6.CODCLIFOR = RODCLI.CODCLIFOR WHERE ORIGEM <> 'OR' AND  CODCGA IN(".$unigasto.")  AND CODUNN IN(".$unidades.") AND ANO = ".$ano." AND ANALIT =".$analit." AND MES = ".$mes." AND CODCUS NOT IN(13,40,11)";
        $sql = "SELECT RODCLI.RAZSOC AS RAZSOC, MES,ANO,NUMDOC,CODUNN,UNIDADE,CODCGA,GASTO,ANO,MES,SUM(VLRREA) AS VLRREA,OBSERVACAO FROM VW_ORCAMENTO_COMPETENCIA_V6 INNER JOIN RODCLI ON VW_ORCAMENTO_COMPETENCIA_V6.CODCLIFOR = RODCLI.CODCLIFOR WHERE ORIGEM <> 'OR' AND  CODCGA IN(".$unigasto.")  AND CODUNN IN(".$unidades.") AND ANO = ".$ano." AND ANALIT =".$analit." AND MES = ".$mes." AND CODCUS NOT IN(13,40,11) GROUP BY RAZSOC, MES,ANO, NUMDOC,CODUNN,UNIDADE,CODCGA,GASTO,ANO,MES,OBSERVACAO";

       // SELECT  FROM VW_ORCAMENTO_COMPETENCIA_V6 WHERE  CODCGA IN(9)  AND CODUNN IN(6,7,15,16,17,18,19,20,21,22,23,24,25,26) AND ANO = 2020 AND ANALIT =155 AND MES = 2 AND CODCUS NOT IN(13,40,11) AND ORIGEM <> 'OR';
      // convert(varchar, DATATU, 105) as DATATU,
        //dd($sql);
        $con = DB::connection('sqlsrv')->select($sql);
        //$titulo = '';
        //if(count($con)>0){
        ///    $titulo = $con[0]->SINTET.'-'.$con[0]->SINTETICA.' / '.$con[0]->ANALIT.'-'.$con[0]->ANALITICA;
       // }
        $total = 0;
        $dados = '<table class="table">';
        $dados.= "<thead>";
        $dados.= '<tr><th>DOCUMENTO</th><th>VALOR</th><th style="width:90px">MES/ANO</th><th>FILIAL</th><th>UNIDADE</th><th>FORNECEDOR</th><th>OBS</th></tr></thead><tbody>';
        foreach($con as $c){
            $total = $total + $c->VLRREA;
            $dados.= '<tr><td>'.$c->NUMDOC.'</td><td><b>'.number_format($c->VLRREA, 2, ',', '.').'</b></td><td>'.$c->MES.'/'.$c->ANO.'</td><td>'.$c->CODCGA.'-'.$c->GASTO.'</td><td>'.$c->CODUNN.'-'.$c->UNIDADE.'</td><td>'.$c->RAZSOC.'</td><td>'.$c->OBSERVACAO.'</td></tr>';
        }
        $dados.= '<tr><td><b>TOTAL</b></td><td style="color:red" colspan="6"><b>'.number_format($total, 2, ',', '.')."</b></td></tr>";
        $dados.= "</tbody></tabel>";
        echo $dados;die;
    }

    public function menu(Request $request){

        View::share ( 'itemMenu', $this->itemMenu );
        return view('dre.menu');
    }

    public function pacote(Request $request){

        $codgru = $request->codgru;

        $sqlSintet = " SELECT P.CODCLAP,P.DESCRI FROM PAGCLA P WHERE P.CODCLAP IN(SELECT PC.PAICLAP FROM PAGCLA PC WHERE PC.CODGRU = ".$codgru." AND PC.SITUAC = 'A' GROUP BY PC.PAICLAP);";
        $sqlAnalit = "SELECT * FROM PAGCLA WHERE CODGRU = ".$codgru." AND SITUAC = 'A' ORDER BY PAICLAP ASC;";
        //$sqlAnalit = "SELECT PC.*,CUS.CODCUS,CUS.DESCRI AS CUSTO FROM PAGCLA PC INNER JOIN RODCUS CUS ON CUS.CODGRU = PC.CODGRU WHERE PC.CODGRU = ".$codgru." AND PC.SITUAC = 'A' AND CUS.SITUAC = 'A' ORDER BY PC.PAICLAP ASC";

        $pacoteAnaliticas = DB::select($sqlAnalit);
        $pacoteSinteticas = DB::select($sqlSintet);
        //$pacoteAnaliticas = DB::table('PAGCLA')->select('*')->where('CODGRU','=',$codgru)->where('SITUAC','=','A')->orderBy('PAICLAP','ASC')->get();
        //dd($pacoteSinteticas,$pacoteAnaliticas);

        View::share ( 'itemMenu', $this->itemMenu );
        return view('dre.pacote',['pacoteSinteticas'=>$pacoteSinteticas,'pacoteAnaliticas'=>$pacoteAnaliticas]);
    }

}
