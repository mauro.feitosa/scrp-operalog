<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Redirect,Response,DB,Config;
use Datatables;
use View;
use Carbon\Carbon;
use Session;

class CentrodegastounidadeController extends Controller
{
    public $itemMenu = 6;

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('ativo');
        View::share ( 'itemMenu', $this->itemMenu );
    }

    public function index()
    {
        $pageIcon = 'admin-people';
        $pageTitulo = 'Usuários';
        $itemMenu = 6;
        //View::share ( 'itemMenu', $itemMenu );

        return view('centrodegastounidade.index');
    }

    public function cdguList()
    {
        $sql = "SELECT GROUP_CONCAT(RODUNN.CODUNN ,' ', RODUNN.DESCRI SEPARATOR ',') AS UND ,MENUFILIAL.CODCGA,RODCGA.DESCRI AS CENTRODEGASTO FROM  MENUFILIAL
                INNER JOIN RODCGA ON MENUFILIAL.CODCGA = RODCGA.CODCGA
                INNER JOIN RODUNN ON MENUFILIAL.CODUNN = RODUNN.CODUNN GROUP BY MENUFILIAL.CODCGA,RODCGA.DESCRI";
        $menuFilial = DB::select($sql);


        return datatables()->of($menuFilial)
        ->addIndexColumn()
        ->addColumn('action', function($menuFilial){
              // $btn = '<form name="frmAdd" method="POST" action="'.route('centrodegastounidade.edit').'">';
              // $btn.=  '<input type="hidden" name="CODCGA" value="'.$menuFilial->CODCGA.'" />';
              // $btn.=  '<button type="submit" class="btn btn-primary save">Editar</button></form>';
               $btn =  '<a href="'.route('centrodegastounidade.edit',['CODCGA'=>$menuFilial->CODCGA]).'" class="btn btn-sm btn-info pull-left" style="margin-right: 3px;">Editar</a>';
//dd($btn);
                return $btn;
        })
        ->rawColumns(['action'])
            ->make(true);
    }

    public function create()
    {
        $listaRodcga = DB::table('RODCGA')->select('CODCGA','DESCRI AS CENTRODEGASTO')->where('SITUAC','=','A')->orderBy('CODCGA','ASC')->get();
        $listaRodunn = DB::table('RODUNN')->select('CODUNN','DESCRI AS UNIDADE')->where('SITUAC','=','A')->orderBy('CODUNN','ASC')->get();

       // dd($listaRodcga,$listaRodunn);

        return view('centrodegastounidade.create',['listaRodcga'=>$listaRodcga,'listaRodunn'=>$listaRodunn]);
    }

    public function store(Request $request)
    {
        //dd($request->CODUNN);
        if($request->CODUNN == null){
            $val[] = array('CODCGA'=>$request->CODCGA);
        }else{
            $val = array();
            foreach ($request->CODUNN as $v) {
                $val[] = array('CODCGA'=>$request->CODCGA,'CODUNN'=>$v);
            }

        }
        $insertVal = DB::table('MENUFILIAL')->insert($val);

        return redirect()->route('centrodegastounidade.index')
            ->with('flash_message',
             'Registro Adicionado.');
    }

    public function show($id)
    {

    }

    public function edit(Request $request,$id)
    {

        $listaRodcga = DB::table('RODCGA')->select('CODCGA','DESCRI AS CENTRODEGASTO')->where('SITUAC','=','A')->orderBy('CODCGA','ASC')->get();
        $listaRodunn = DB::table('RODUNN')->select('CODUNN','DESCRI AS UNIDADE')->where('SITUAC','=','A')->orderBy('CODUNN','ASC')->get();

        if($id){
            $codcga = $id;
            $unidades = DB::table('MENUFILIAL')->select('CODUNN')->where('CODCGA','=',$id)->pluck('CODUNN')->toArray();
        }

       //dd($unidades);

        return view('centrodegastounidade.edit',['listaRodcga'=>$listaRodcga,'listaRodunn'=>$listaRodunn,'codcga'=>$codcga,'unidades'=>$unidades]);
    }



    public function update(Request $request, $id)
    {

        $limpa = DB::table('MENUFILIAL')->where('CODCGA', '=', $request->CODCGA)->delete();

        if($request->CODUNN == null){
            $val[] = array('CODCGA'=>$request->CODCGA);
        }else{
            $val = array();
            foreach ($request->CODUNN as $v) {
                $val[] = array('CODCGA'=>$request->CODCGA,'CODUNN'=>$v);
            }
        }
        $insertVal = DB::table('MENUFILIAL')->insert($val);

        return redirect()->route('centrodegastounidade.index')
            ->with('flash_message',
             'Registro atualizado.');

    }

    public function destroy($id)
    {

    }

    public function pacote(Request $request){

        $created_at = Carbon::now()->format('Y-m-d H:i:s');
        $updated_at = Carbon::now()->format('Y-m-d H:i:s');
        $itensCodigo = array();

        if($request->codigo){

            DB::table('MENUPACOTE')->truncate();

            foreach($request->codigo as $cod){

                $itensCodigo[]  = ['CODIGO' => $cod,'created_at'=>$created_at,'updated_at'=>$updated_at];

            }

            $insertItemUSU_PACOTE     = DB::table('MENUPACOTE')->insert($itensCodigo);
        }

        View::share ( 'itemMenu', 10);

        $RODGRU = DB::table('RODGRU')->select('CODIGO','DESCRI')->orderBy('DESCRI','ASC')->get();
        $lista_pacote_menu = DB::table('MENUPACOTE')->select('CODIGO')->pluck('CODIGO')->toArray();

        return view('centrodegastounidade.pacote',['RODGRU'=>$RODGRU,'lista_pacote_menu'=>$lista_pacote_menu]);
    }


}
