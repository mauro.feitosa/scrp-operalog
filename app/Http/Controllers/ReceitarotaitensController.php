<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Redirect,Response,DB,Config;
use Datatables;
use View;
use Session;

class ReceitarotaitensController extends Controller
{
    public $itemMenu = 7;
    public $arrayTipo = array('FUNÇÃO'=>'F','NUMERO' => 'N','MOEDA' => 'M','PERCENTUAL' => 'P');

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('ativo');
        View::share ( 'itemMenu', $this->itemMenu );
    }

    public function index()
    {
        //$pageIcon = 'admin-list';
        //$pageTitulo = 'Receita Rota Itens';
        //$itemMenu = 6;
        //View::share ( 'itemMenu', $itemMenu );

        return view('receitarotaitens.index');
    }
    public function receitarotaitensList()
    {
        $sql = "SELECT RECEITA_ROTA_ITENS.ID,RECEITA_ROTA_ITENS.IDPAI,RECEITA_ROTA_ITENS.TIPO,RECEITA_ROTA_ITENS.OBJTIPO,RECEITA_ROTA_ITENS.ORDEM,RECEITA_ROTA_ITENS.ORDEMFUNC,concat(RODUNN.CODUNN, '-' ,RODUNN.DESCRI) AS UNIDADE, RECEITA_ROTA_ITENS.DESCRI FROM RECEITA_ROTA_ITENS INNER JOIN RODUNN ON RECEITA_ROTA_ITENS.CODUNN = RODUNN.CODUNN ORDER BY RECEITA_ROTA_ITENS.ID,RECEITA_ROTA_ITENS.IDPAI";
        $lista = DB::select($sql);

       // dd($lista);
        return datatables()->of($lista)
        ->addIndexColumn()
        ->addColumn('action', function($lista){

               $btn =  '<a href="'.route('receitarotaitens.edit',['ID'=>$lista->ID]).'" class="btn btn-sm btn-info pull-left" style="margin-right: 3px;">Editar</a>';

                return $btn;
        })
        ->rawColumns(['action'])
            ->make(true);
    }

    public function create()
    {
        $listaRodunn = DB::table('RODUNN')->select('CODUNN','DESCRI AS UNIDADE')->where('SITUAC','=','A')->orderBy('CODUNN','ASC')->get();
        $listaPai = DB::table('RECEITA_ROTA_ITENS')->select('ID','DESCRI')->where('IDPAI','=',0)->get();//->toArray();

        $v = (object) ['ID'=>0,'DESCRI'=>'Sem item pai'];
        $listaPai->push($v);
        return view('receitarotaitens.create',['listaRodunn'=>$listaRodunn,'listaPai'=>$listaPai,'arrayTipo' =>$this->arrayTipo]);
    }

    public function store(Request $request)
    {

       // $val[]= array('CODUNN'=>$request->CODUNN,'DESCRI'=>$request->DESCRI,'IDPAI'=>$request->IDPAI);

        $val[]= array('CODUNN'=>$request->CODUNN,'DESCRI'=>$request->DESCRI,'IDPAI'=>$request->IDPAI,'ORDEM' => $request->ORDEM,'TIPO'=>$request->TIPO,'OBJTIPO'=>$request->OBJTIPO,'ORDEMFUNC'=>$request->ORDEMFUNC);

        $insertVal = DB::table('RECEITA_ROTA_ITENS')->insert($val);

        return redirect()->route('receitarotaitens.index')
            ->with('flash_message',
             'Registro Adicionado.');
    }

    public function show($id)
    {

    }

    public function edit(Request $request,$id)
    {
        //dd($request,$id);

        //$arrayTipo = array('FUNÇÃO'=>'F','NUMERO' => 'N','MOEDA' => 'M','PERCENTUAL' => 'P');
        $listaPai = DB::table('RECEITA_ROTA_ITENS')->select('ID','DESCRI')->where('IDPAI','=',0)->orderBy('ID','ASC')->get();//->toArray();

        $listaRodunn = DB::table('RODUNN')->select('CODUNN','DESCRI AS UNIDADE')->where('SITUAC','=','A')->orderBy('CODUNN','ASC')->get();

        if($id){
            $receitaitens = DB::table('RECEITA_ROTA_ITENS')->select('*')->where('ID','=',$id)->first();
        }else{
            return redirect()->route('receitarotaitens.index')
            ->with('flash_message',
             'Registro não encotrado.');
        }

       // dd($receitaitens,$listaRodunn);

        return view('receitarotaitens.edit',['listaRodunn'=>$listaRodunn,'id'=>$id,'receitaitens'=>$receitaitens,'arrayTipo' =>$this->arrayTipo,'listaPai'=>$listaPai]);
    }

    public function update(Request $request, $id)
    {

        $val= array('CODUNN'=>$request->CODUNN,'DESCRI'=>$request->DESCRI,'IDPAI'=>$request->IDPAI,'ORDEM' => $request->ORDEM,'TIPO'=>$request->TIPO,'OBJTIPO'=>$request->OBJTIPO,'ORDEMFUNC'=>$request->ORDEMFUNC);

        $updateVal  = DB::table('RECEITA_ROTA_ITENS')
            ->where('ID','=', $id)
            ->update($val);

        return redirect()->route('receitarotaitens.index')
            ->with('flash_message',
             'Registro atualizado.');

    }

    public function destroy($id)
    {

    }


}
