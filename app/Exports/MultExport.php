<?php

namespace App\Exports;
use App\Exports\UsersExport;
use App\Exports\UsersExport2;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;
use Maatwebsite\Excel\Concerns\WithTitle;
use DB;

class MultExport implements WithMultipleSheets
{
    use Exportable;

    protected $year;

    public function __construct(int $ano, int $codunn, int $codcga)
    {
        $this->ano = $ano;
        $this->codunn = $codunn;
        $this->codcga = $codcga;

    }

    /**
     * @return array
     */
    public function sheets(): array
    {
        $sheets = [];

            $grupo = DB::table('RODGRU_LIST')
            ->select('RODGRU_LIST.CODIGO','RODGRU_LIST.DESCRI')
            ->where('RODGRU_LIST.LISTAR', '=','s')
            ->get();
            //dd($grupo);
            $sheets[] = new VariaveisExport($this->ano,$this->codunn,$this->codcga);
            $sheets[] = new ReceitaExport($this->ano,$this->codunn,$this->codcga);

            foreach($grupo as $gr)
            {
               // dd($gr->CODIGO);

                $sheets[] = new UsersExportPacote($this->ano,$gr->CODIGO,$this->codunn,$this->codcga);
            }
        //for ($month = 1; $month <= 12; $month++) {
           // $sheets[0] = new UsersExportPacote($ano,$codgru,15,$codcga);
         //   $sheets[1] = new UsersExportPacote($ano,$codgru,18,$codcga);
       // }

     //  return [
     //   'usuario_1' => new UsersExport(),
     //   'usuario_2' => new UsersExport2(),
      // ];

       return $sheets;
    }
}
