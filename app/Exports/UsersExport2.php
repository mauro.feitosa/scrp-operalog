<?php

namespace App\Exports;

use App\User;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use DB;

class UsersExport2 implements FromCollection , WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        // return User::all();
        return DB::table('users')->select('name','email')->where('setor_id','=',2)->get();
    }

    public function headings(): array
    {
        return [
            'nome', 'email'
        ];
    }

}
