<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithTitle;
use DB;
use PhpOffice\PhpSpreadsheet\Cell\Cell;
use Maatwebsite\Excel\Concerns\ToModel;
use PhpOffice\PhpSpreadsheet\Cell\DataType;
use Maatwebsite\Excel\Concerns\WithCustomValueBinder;
use PhpOffice\PhpSpreadsheet\Cell\DefaultValueBinder;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
////use Maatwebsite\Excel\Concerns\WithEvents;
///use Maatwebsite\Excel\Events\AfterSheet;


class VariaveisExport extends DefaultValueBinder implements FromView, WithTitle,ShouldAutoSize
{

    public function __construct(int $ano,int $codunn,int $codcga)
    {
        $this->ano = $ano;
        $this->codunn = $codunn;
        $this->codcga = $codcga;

    }

    public function bindValue(Cell $cell, $value)
    {
        if (is_numeric($value)) {
            $cell->setValueExplicit($value, DataType::TYPE_NUMERIC);

            return true;
        }

        // else return default behavior
        return parent::bindValue($cell, $value);
    }

    public function view(): View
    {

        $CODCGA = $this->codcga;
        $CODUNN = $this->codunn;
        $ano = $this->ano;

        ///$pacote = DB::table('RODGRU')->select('DESCRI')->where('CODIGO','=',$codgru)->first();
        $sqlAnalit = "SELECT I.* ,
        IFNULL((SELECT C.VALOR FROM VARIAVEIS_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '01' and I.ID = C.ITEN),'0.00') AS VAL1,
        IFNULL((SELECT C.VALOR FROM VARIAVEIS_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '02' and I.ID = C.ITEN),'0.00') AS VAL2,
        IFNULL((SELECT C.VALOR FROM VARIAVEIS_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '03' and I.ID = C.ITEN),'0.00') AS VAL3,
        IFNULL((SELECT C.VALOR FROM VARIAVEIS_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '04' and I.ID = C.ITEN),'0.00') AS VAL4,
        IFNULL((SELECT C.VALOR FROM VARIAVEIS_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '05' and I.ID = C.ITEN),'0.00') AS VAL5,
        IFNULL((SELECT C.VALOR FROM VARIAVEIS_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '06' and I.ID = C.ITEN),'0.00') AS VAL6,
        IFNULL((SELECT C.VALOR FROM VARIAVEIS_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '07' and I.ID = C.ITEN),'0.00') AS VAL7,
        IFNULL((SELECT C.VALOR FROM VARIAVEIS_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '08' and I.ID = C.ITEN),'0.00') AS VAL8,
        IFNULL((SELECT C.VALOR FROM VARIAVEIS_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '09' and I.ID = C.ITEN),'0.00') AS VAL9,
        IFNULL((SELECT C.VALOR FROM VARIAVEIS_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '10' and I.ID = C.ITEN),'0.00') AS VAL10,
        IFNULL((SELECT C.VALOR FROM VARIAVEIS_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '11' and I.ID = C.ITEN),'0.00') AS VAL11,
        IFNULL((SELECT C.VALOR FROM VARIAVEIS_CAD C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.MES = '12' and I.ID = C.ITEN),'0.00') AS VAL12
         FROM VARIAVEIS_ITENS I ORDER BY I.ORDEM ASC";

        $sqlv = "SELECT * FROM UND_VARIAVEIS WHERE SITUAC = 'A' ORDER BY ID ASC";

        $pacoteAnaliticas = DB::select($sqlAnalit);
        $pacoteSinteticas = DB::select($sqlv);

        return view('exports.variaveis', ['pacoteSinteticas'=>$pacoteSinteticas,'pacoteAnaliticas'=>$pacoteAnaliticas,'pacote'=>'RESUMO']);
    }

    public function title(): string
    {
        return 'RESUMO';
    }

}
