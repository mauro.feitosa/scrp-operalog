<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithTitle;
use DB;
use PhpOffice\PhpSpreadsheet\Cell\Cell;
use Maatwebsite\Excel\Concerns\ToModel;
use PhpOffice\PhpSpreadsheet\Cell\DataType;
use Maatwebsite\Excel\Concerns\WithCustomValueBinder;
use PhpOffice\PhpSpreadsheet\Cell\DefaultValueBinder;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
////use Maatwebsite\Excel\Concerns\WithEvents;
///use Maatwebsite\Excel\Events\AfterSheet;


class UsersExportPacote extends DefaultValueBinder implements FromView, WithTitle,ShouldAutoSize
{

    public function __construct(int $ano,int $codgru,int $codunn,int $codcga)
    {
        $this->ano = $ano;
        $this->codgru = $codgru;
        $this->codunn = $codunn;
        $this->codcga = $codcga;

    }

    public function bindValue(Cell $cell, $value)
    {
        if (is_numeric($value)) {
            $cell->setValueExplicit($value, DataType::TYPE_NUMERIC);

            return true;
        }

        // else return default behavior
        return parent::bindValue($cell, $value);
    }

    public function view(): View
    {
        $codgru = 5;
        $CODCGA = 9;
        $CODUNN = 15;
        $ano = 2019;

        $codgru = $this->codgru;
        $CODCGA = $this->codcga;
        $CODUNN = $this->codunn;
        $ano = $this->ano;

        $pacote = DB::table('RODGRU')->select('DESCRI')->where('CODIGO','=',$codgru)->first();
        $sqlSintet = "SELECT P.CODCLAP,P.DESCRI,
		IFNULL((SELECT SUM(C.VALORFOR) FROM ORCLAN_FOR C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.CODGRU = ".$codgru." AND C.MES = '01' AND P.CODCLAP = C.SINTET),0) AS VAL1,
        IFNULL((SELECT SUM(C.VALORFOR) FROM ORCLAN_FOR C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.CODGRU = ".$codgru." AND C.MES = '02' AND P.CODCLAP = C.SINTET),0) AS VAL2,
        IFNULL((SELECT SUM(C.VALORFOR) FROM ORCLAN_FOR C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.CODGRU = ".$codgru." AND C.MES = '03' AND P.CODCLAP = C.SINTET),0) AS VAL3,
        IFNULL((SELECT SUM(C.VALORFOR) FROM ORCLAN_FOR C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.CODGRU = ".$codgru." AND C.MES = '04' AND P.CODCLAP = C.SINTET),0) AS VAL4,
        IFNULL((SELECT SUM(C.VALORFOR) FROM ORCLAN_FOR C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.CODGRU = ".$codgru." AND C.MES = '05' AND P.CODCLAP = C.SINTET),0) AS VAL5,
        IFNULL((SELECT SUM(C.VALORFOR) FROM ORCLAN_FOR C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.CODGRU = ".$codgru." AND C.MES = '06' AND P.CODCLAP = C.SINTET),0) AS VAL6,
        IFNULL((SELECT SUM(C.VALORFOR) FROM ORCLAN_FOR C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.CODGRU = ".$codgru." AND C.MES = '07' AND P.CODCLAP = C.SINTET),0) AS VAL7,
        IFNULL((SELECT SUM(C.VALORFOR) FROM ORCLAN_FOR C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.CODGRU = ".$codgru." AND C.MES = '08' AND P.CODCLAP = C.SINTET),0) AS VAL8,
        IFNULL((SELECT SUM(C.VALORFOR) FROM ORCLAN_FOR C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.CODGRU = ".$codgru." AND C.MES = '09' AND P.CODCLAP = C.SINTET),0) AS VAL9,
        IFNULL((SELECT SUM(C.VALORFOR) FROM ORCLAN_FOR C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.CODGRU = ".$codgru." AND C.MES = '10' AND P.CODCLAP = C.SINTET),0) AS VAL10,
        IFNULL((SELECT SUM(C.VALORFOR) FROM ORCLAN_FOR C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.CODGRU = ".$codgru." AND C.MES = '11' AND P.CODCLAP = C.SINTET),0) AS VAL11,
        IFNULL((SELECT SUM(C.VALORFOR) FROM ORCLAN_FOR C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.CODGRU = ".$codgru." AND C.MES = '12' AND P.CODCLAP = C.SINTET),0) AS VAL12
        FROM PAGCLA P WHERE P.CODCLAP IN(SELECT PC.PAICLAP FROM PAGCLA PC WHERE PC.CODGRU = ".$codgru." AND PC.SITUAC = 'A' GROUP BY PC.PAICLAP)";

         $sqlAnalit = "SELECT P.*,J.TEXTO,
         IFNULL((SELECT C.VALORFOR FROM ORCLAN_FOR C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.CODGRU = ".$codgru." AND C.MES = '01' AND P.CODCLAP = C.ANALIT),'0.00') AS VAL1,
         IFNULL((SELECT C.VALORFOR FROM ORCLAN_FOR C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.CODGRU = ".$codgru." AND C.MES = '02' AND P.CODCLAP = C.ANALIT),'0.00') AS VAL2,
         IFNULL((SELECT C.VALORFOR FROM ORCLAN_FOR C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.CODGRU = ".$codgru." AND C.MES = '03' AND P.CODCLAP = C.ANALIT),'0.00') AS VAL3,
         IFNULL((SELECT C.VALORFOR FROM ORCLAN_FOR C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.CODGRU = ".$codgru." AND C.MES = '04' AND P.CODCLAP = C.ANALIT),'0.00') AS VAL4,
         IFNULL((SELECT C.VALORFOR FROM ORCLAN_FOR C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.CODGRU = ".$codgru." AND C.MES = '05' AND P.CODCLAP = C.ANALIT),'0.00') AS VAL5,
         IFNULL((SELECT C.VALORFOR FROM ORCLAN_FOR C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.CODGRU = ".$codgru." AND C.MES = '06' AND P.CODCLAP = C.ANALIT),'0.00') AS VAL6,
         IFNULL((SELECT C.VALORFOR FROM ORCLAN_FOR C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.CODGRU = ".$codgru." AND C.MES = '07' AND P.CODCLAP = C.ANALIT),'0.00') AS VAL7,
         IFNULL((SELECT C.VALORFOR FROM ORCLAN_FOR C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.CODGRU = ".$codgru." AND C.MES = '08' AND P.CODCLAP = C.ANALIT),'0.00') AS VAL8,
         IFNULL((SELECT C.VALORFOR FROM ORCLAN_FOR C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.CODGRU = ".$codgru." AND C.MES = '09' AND P.CODCLAP = C.ANALIT),'0.00') AS VAL9,
         IFNULL((SELECT C.VALORFOR FROM ORCLAN_FOR C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.CODGRU = ".$codgru." AND C.MES = '10' AND P.CODCLAP = C.ANALIT),'0.00') AS VAL10,
         IFNULL((SELECT C.VALORFOR FROM ORCLAN_FOR C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.CODGRU = ".$codgru." AND C.MES = '11' AND P.CODCLAP = C.ANALIT),'0.00') AS VAL11,
         IFNULL((SELECT C.VALORFOR FROM ORCLAN_FOR C WHERE C.CODCGA = ".$CODCGA." AND C.CODUNN = ".$CODUNN." AND  C.ANO =  ".$ano." AND C.CODGRU = ".$codgru." AND C.MES = '12' AND P.CODCLAP = C.ANALIT),'0.00') AS VAL12
         FROM PAGCLA P
         LEFT JOIN JUSTIFICATIVA J ON P.CODCLAP = J.ANALIT AND J.CODCGA = ".$CODCGA." AND J.CODUNN = ".$CODUNN." AND  J.ANO =  ".$ano."
         WHERE P.CODGRU = ".$codgru." AND P.SITUAC = 'A'  ORDER BY P.PAICLAP ASC";

        $pacoteAnaliticas = DB::select($sqlAnalit);
        $pacoteSinteticas = DB::select($sqlSintet);

        return view('exports.pacote', ['pacoteSinteticas'=>$pacoteSinteticas,'pacoteAnaliticas'=>$pacoteAnaliticas,'pacote'=>$pacote->DESCRI]);
    }

    public function title(): string
    {
        $codgru = $this->codgru;
        $pacote = DB::table('RODGRU')->select('DESCRI')->where('CODIGO','=',$codgru)->first();
        return $pacote->DESCRI;
    }

}
