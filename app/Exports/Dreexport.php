<?php

namespace App\Exports;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\BeforeExport;
use Maatwebsite\Excel\Events\BeforeWriting;
use Maatwebsite\Excel\Events\BeforeSheet;
use Maatwebsite\Excel\Events\BAfterSheet;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Auth;
use DB;


class Dreexport implements WithEvents,FromView,ShouldAutoSize
{

    public function view(): View
    {

      $tb = DB::table('dreexport')->select('dados')->where(['user_id'=>Auth::user()->id])->first();
      //$sessaoTabela = '';
      //dd($tb->dados);

        return view('dre.table',['tabela' => $tb->dados]);
    }

    public function registerEvents(): array
    {
        return [
            // Handle by a closure.
            BeforeExport::class => function(BeforeExport $event) {
                $event->writer->getProperties()->setCreator('SCRP');

            },
            AfterSheet::class => function(AfterSheet $event) {
                $event->sheet->freezeFirstColumn();

            }

            // Array callable, refering to a static method.
            //BeforeWriting::class => [self::class, 'beforeWriting'],

            //Using a class with an __invoke method.
            // BeforeSheet::class => function (BeforeSheet $event){
            //    $event->sheet->freezePane('A1');
             //}
        ];
    }

}
