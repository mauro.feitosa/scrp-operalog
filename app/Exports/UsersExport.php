<?php

namespace App\Exports;

use App\User;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithTitle;
use DB;

class UsersExport implements FromCollection , WithHeadings, WithTitle , FromView
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        // return User::all();
        return DB::table('users')->select('name','email')->where('setor_id','=',1)->get();
    }

    public function view()
    {
        return view('user');

    }

    public function headings(): array
    {
        return [
            'nome', 'email'
        ];
    }

    public function title(): string
    {
        return 'Usuarios';
    }

}
