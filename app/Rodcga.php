<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rodcga extends Model
{
	protected $table = 'RODCGA';
	protected $fillable = ['CODCGA', 'CODPAD', 'CODFIL', 'DESCRI', 'CODCON', 'CODGER', 'SITUAC'];

}