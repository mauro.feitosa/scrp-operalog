<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pagcla extends Model
{
	protected $table = 'PAGCLA';
	protected $fillable = ['CODCLAP', 'CODFIL', 'CODCON', 'DESCRI', 'CODGER', 'CODGRU', 'TIPCLA', 'CODPAD', 'PAICLAP', 'SITUAC'];

}