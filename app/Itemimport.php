<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Itemimport extends Model
{
    protected $table = 'itemimport';

    protected $fillable = ['codigo','valor','descri'];

}
