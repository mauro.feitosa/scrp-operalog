<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rodcus extends Model
{
	protected $table = 'RODCUS';
	protected $fillable = ['CODCUS', 'CODFIL', 'CODPAD', 'CODGRU', 'DESCRI', 'SITUAC', 'TIPCUS', 'ACELAN'];

}