<?php

namespace App\Imports;

use App\Itemimport;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;
use Maatwebsite\Excel\Concerns\SkipsUnknownSheets;

class ItemImporta implements ToModel //,WithMultipleSheets, SkipsUnknownSheets
{

    public function model(array $row)
    {
        return new Itemimport([
           'codigo'     => $row[0],
           'valor'    => $row[1],
           'descri' => $row[2],
        ]);
    }
    /*public function sheets(): array
    {
        return [
            'Worksheet1' => new FirstSheetImport(),
            'Worksheet2' => new SecondSheetImport(),
        ];
    }

    public function onUnknownSheet($sheetName)
    {
        // E.g. you can log that a sheet was not found.
        info("Sheet {$sheetName} was skipped");
    }*/
}
