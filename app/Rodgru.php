<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rodgru extends Model
{
	protected $table = 'RODGRU';
	protected $fillable = ['CODIGO', 'CODPAD', 'CODFIL', 'DESCRI'];

}