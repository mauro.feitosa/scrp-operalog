<!DOCTYPE html>
<html lang="pt_BR">
<head>
	<title>login SCRP</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->

	<link href="{{ asset('css/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
	<link href="{{ asset('fonts/css/all.css') }}" rel="stylesheet" type="text/css">

<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{ asset('css/util.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/main.css') }}">
<!--===============================================================================================-->
</head>
@php
$keyUser = 'us_'.date('mdYHis');
$keyPass = 'pa_'.date('mdYHis');
@endphp
<body style="background-color: #666666;">

	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100">
				<form method="POST" action="{{ route('login') }}" class="login100-form validate-form">
                {!! csrf_field() !!}
                <input type="hidden" name="auxusername" value="{{ $keyUser }}">
                <input type="hidden" name="auxpassword" value="{{ $keyPass }}">
					<span class="login100-form-title">
						<img class="img-fluid" src="{{ asset('images/logo.png') }}" />
					</span>


					<div class="wrap-input100">
						<input class="input100" type="username" name="username" autocomplete="username" placeholder="email" required>
						<!--span class="label-input100">Email</span--->
					</div>


					<div class="wrap-input100">
						<input class="input100" type="password" name="password" autocomplete="current-password" placeholder="senha" required>
						<!--span class="label-input100">Password</span-->
					</div>


					<div class="container-login100-form-btn">
						<button class="login100-form-btn">
							ENTRAR
						</button>
					</div>

					<div class="text-center p-t-46 p-b-20">
						<span class="txt2">

						</span>
					</div>

				</form>

				<div class="login100-more" style="background-image: url({{ asset('images/backgrounds/2.jpg') }});">
				</div>
			</div>
		</div>
	</div>
<!--===============================================================================================-->
	<script src="{{ asset('js/jquery/jquery-3.4.1.min.js') }}"></script>
	<script src="{{ asset('js/bootstrap/js/bootstrap.min.js') }}"></script>
<!--===============================================================================================-->
</body>
</html>
