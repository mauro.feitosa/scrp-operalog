@extends('master',['itemMenu'=>$itemMenu])

@section('page_header')

@php
$menuTitulo = Config::get('menu.menuTitulo');
$menuIcon = Config::get('menu.menuIcon');
$menuUrl = Config::get('menu.menuUrl');
@endphp
    <h1 class="page-title">
    <i class="{{ $menuIcon[$itemMenu] }}"></i> {{ $menuTitulo[$itemMenu] }}
    <a href="{{route('deducoes.create')}}" class="btn btn-success btn-add-new">
        <i class="admin-plus"></i> <span>Adicionar</span>
    </a>
    </h1>
@stop

@section('content')
<style>
.row>[class*=col-] {
    margin-bottom: 5px;
}
.table {
        font-size:10px !important;
    }
.table>thead>tr>th {
    font-weight: bolder;
    color: #526069;
}
.btn {
    padding: 4px 6px;
    font-size: 11px;
    margin-top: 0px;
    margin-bottom: 0px;
}
</style>
<div class="page-content container-fluid">

        <div class="alerts"></div>
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-bordered">
                        <div class="panel-body">
                        <table class="table table-bordered" id="laravel_datatable">
                                <thead>
                                   <tr>
                                      <th>ID</th>
                                      <th>FILIAL</th>
                                      <th>UNIDADE</th>
                                      <th>ANO</th>
                                      <th>MES</th>
                                      <th>VALOR</th>
                                      <th width="100px">AÇÃO</th>
                                   </tr>
                                </thead>
                        </table>
                        </div>
                    </div>
                </div>
            </div>
</div>
@endsection
@section('javascript')
<script>
        $(document).ready( function () {
         $('#laravel_datatable').DataTable({
                processing: true,
                serverSide: true,
                order: [[ 0, "desc" ]],
                ajax: "{{ url('deducoes-list') }}",
                columns: [
                         { data: 'ID', name: 'ID'},
                         { data: 'FILIAL', name: 'FILIAL' },
                         { data: 'UNIDADE', name: 'UNIDADE' },
                         { data: 'ANO', name: 'ANO' },
                         { data: 'MES', name: 'MES' },
                         { data: 'VALOR', name: 'VALOR' },
                         { data: 'action', name: 'action', orderable: false, searchable: false}
                      ],
                      language:{
                        "sEmptyTable": "Nenhum registro encontrado",
                        "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
                        "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
                        "sInfoFiltered": "(Filtrados de _MAX_ registros)",
                        "sInfoPostFix": "",
                        "sInfoThousands": ".",
                        "sLengthMenu": "_MENU_ resultados por página",
                        "sLoadingRecords": "Carregando...",
                        "sProcessing": "Processando...",
                        "sZeroRecords": "Nenhum registro encontrado",
                        "sSearch": "Pesquisar",
                        "oPaginate": {
                            "sNext": "Próximo",
                            "sPrevious": "Anterior",
                            "sFirst": "Primeiro",
                            "sLast": "Último"
                        },
                        "oAria": {
                            "sSortAscending": ": Ordenar colunas de forma ascendente",
                            "sSortDescending": ": Ordenar colunas de forma descendente"
                        }
                    }

             });
          });
</script>
@endsection
