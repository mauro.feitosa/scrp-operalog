@php
$menuTitulo = Config::get('menu.menuTitulo');
$menuIcon = Config::get('menu.menuIcon');
$menuUrl = Config::get('menu.menuUrl');

@endphp
@extends('master',['itemMenu'=>$itemMenu])

@section('page_header')
    <h1 class="page-title">
            <i class="{{ $menuIcon[$itemMenu] }}"></i> {{ $menuTitulo[$itemMenu] }} \ Pacote
    </h1>
@stop

@section('content')
<style>
.row>[class*=col-] {
    margin-bottom: 5px;
}
.topoMenu {
    background-color: #337ab7!important;
    color: #fffff!important;
    border-color: #337ab7!important;
}
.topoMenuTitulo {
    background-color: #337ab7!important;
    color: #fffff!important;
    font-size:12px;
    font-weight:bolder;
}
</style>
<div class="page-content container-fluid">
    <!--div class="clearfix container-fluid row"-->
        <div class="alerts"></div>
            <div class="row">
                    <div class="col-md-4">
                            <div class="panel panel-bordered">
                                    <div class="panel-body">
                        <div class="list-group-item topoMenu">
                               <h4 class="topoMenuTitulo" style="color:#ffffff!important">{{$unidadeFilial['CODCGA']}} - {{$unidadeFilial['CENTRODEGASTO']}}</h4>
                               <h4 class="topoMenuTitulo" style="color:#ffffff!important"><span class="badge badge-warning">{{$unidadeFilial['CODUNN']}} - {{$unidadeFilial['UNIDADE']}}</span></h4>
                        </div>
                            @php
                                $x = 0;
                            @endphp
                            @foreach ($listaPacote as $l)
                                @if($x==0)
                                   @php $x = 1; @endphp
                                    <form name="frmPacote5" action="{{route('forecast/variaveis')}}" method="POST">
                                        {!! csrf_field() !!}
                                    <input type="hidden" name="codgru" value="{{$l->CODIGO}}">
                                    <input type="hidden" name="CODUNN" value="{{$CODUNN}}">
                                    <input type="hidden" name="CODCGA" value="{{$CODCGA}}">
                                    <button type="submit" class="list-group-item" style="font-size:12px!important">
                                    VARIÁVEIS
                                    </button>
                                    </form>

                                    <form name="frmPacote5" action="{{route('forecast/receitarota')}}" method="POST">
                                        {!! csrf_field() !!}
                                    <input type="hidden" name="codgru" value="{{$l->CODIGO}}">
                                    <input type="hidden" name="CODUNN" value="{{$CODUNN}}">
                                    <input type="hidden" name="CODCGA" value="{{$CODCGA}}">
                                    <button type="submit" class="list-group-item" style="font-size:12px!important">
                                    RECEITA
                                    </button>
                                    </form>
                                @endif
                                @if(in_array($l->CODIGO,$usuario_pacote))
                                    <form name="frmPacote{{$l->CODIGO}}" action="{{route('forecast/pacote')}}" method="POST">
                                        {!! csrf_field() !!}
                                        <input type="hidden" name="codgru" value="{{$l->CODIGO}}">
                                        <input type="hidden" name="CODUNN" value="{{$CODUNN}}">
                                        <input type="hidden" name="CODCGA" value="{{$CODCGA}}">
                                        <button type="submit" class="list-group-item" style="font-size:12px!important">
                                        {{$l->CODIGO}} - PACOTE	{{$l->DESCRI}}
                                    </button>
                                    </form>
                                @endif
                            @endforeach
                            <!--form name="frmPacote5" action="{{route('forecast/pacote')}}" method="POST">
                                {!! csrf_field() !!}
                            <input type="hidden" name="codgru" value="5">
                            <input type="hidden" name="CODUNN" value="{{$CODUNN}}">
                            <input type="hidden" name="CODCGA" value="{{$CODCGA}}">
                            <button type="submit" class="list-group-item" style="font-size:12px!important">
                            05 - PACOTE	ADMINISTRATIVO
                            </button>
                            </form>

                            <form name="frmPacote2" action="{{route('forecast/pacote')}}" method="POST">
                                {!! csrf_field() !!}
                                <input type="hidden" name="codgru" value="2">
                                <input type="hidden" name="CODUNN" value="{{$CODUNN}}">
                                <input type="hidden" name="CODCGA" value="{{$CODCGA}}">
                            <button type="submit" class="list-group-item" style="font-size:12px!important">
                            02 - PACOTE	FROTA
                            </button>
                            </form>

                            <form name="frmPacote4" action="{{route('forecast/pacote')}}" method="POST">
                                {!! csrf_field() !!}
                                <input type="hidden" name="codgru" value="4">
                                <input type="hidden" name="CODUNN" value="{{$CODUNN}}">
                                <input type="hidden" name="CODCGA" value="{{$CODCGA}}">
                            <button type="submit" class="list-group-item" style="font-size:12px!important">
                            04 - PACOTE	GENTE
                            </button>
                            </form>

                            <form name="frmPacote11" action="{{route('forecast/pacote')}}" method="POST">
                                {!! csrf_field() !!}
                                <input type="hidden" name="codgru" value="11">
                                <input type="hidden" name="CODUNN" value="{{$CODUNN}}">
                                <input type="hidden" name="CODCGA" value="{{$CODCGA}}">
                            <button type="submit" class="list-group-item" style="font-size:12px!important">
                            11 - PACOTE	INVESTIMENTOS
                            </button>
                            </form>

                            <form name="frmPacote8" action="{{route('forecast/pacote')}}" method="POST">
                                {!! csrf_field() !!}
                                <input type="hidden" name="codgru" value="8">
                                <input type="hidden" name="CODUNN" value="{{$CODUNN}}">
                                <input type="hidden" name="CODCGA" value="{{$CODCGA}}">
                            <button type="submit" class="list-group-item" style="font-size:12px!important">
                            08 - PACOTE	JURÍDICO
                            </button>
                            </form>

                            <form name="frmPacote12" action="{{route('forecast/pacote')}}" method="POST">
                                {!! csrf_field() !!}
                                <input type="hidden" name="codgru" value="12">
                                <input type="hidden" name="CODUNN" value="{{$CODUNN}}">
                                <input type="hidden" name="CODCGA" value="{{$CODCGA}}">
                            <button type="submit" class="list-group-item" style="font-size:12px!important">
                            12 - PACOTE	MANUTENÇÃO PREDIAL
                            </button>
                            </form>

                            <form name="frmPacote3" action="{{route('forecast/operacional')}}" method="POST">
                                {!! csrf_field() !!}
                                <input type="hidden" name="codgru" value="3">
                                <input type="hidden" name="CODUNN" value="{{$CODUNN}}">
                                <input type="hidden" name="CODCGA" value="{{$CODCGA}}">
                            <button type="submit" class="list-group-item" style="font-size:12px!important">
                            03 - PACOTE	OPERACIONAL
                            </button>
                            </form>

                            <form name="frmPacote14" action="{{route('forecast/pacote')}}" method="POST">
                                {!! csrf_field() !!}
                                <input type="hidden" name="codgru" value="14">
                                <input type="hidden" name="CODUNN" value="{{$CODUNN}}">
                                <input type="hidden" name="CODCGA" value="{{$CODCGA}}">
                            <button type="submit" class="list-group-item" style="font-size:12px!important">
                            14 - PACOTE	REMUNERAÇÃO DE PESSOAL
                            </button>
                            </form>

                            <form name="frmPacote15" action="{{route('forecast/pacote')}}" method="POST">
                                {!! csrf_field() !!}
                                <input type="hidden" name="codgru" value="15">
                                <input type="hidden" name="CODUNN" value="{{$CODUNN}}">
                                <input type="hidden" name="CODCGA" value="{{$CODCGA}}">
                            <button type="submit" class="list-group-item" style="font-size:12px!important">
                            15 - PACOTE	TECNOLOGIA DA INFORMAÇÃO
                            </button>
                            </form>

                            <form name="frmPacote7" action="{{route('forecast/pacote')}}" method="POST">
                                {!! csrf_field() !!}
                                <input type="hidden" name="codgru" value="7">
                                <input type="hidden" name="CODUNN" value="{{$CODUNN}}">
                                <input type="hidden" name="CODCGA" value="{{$CODCGA}}">
                            <button type="submit" class="list-group-item" style="font-size:12px!important">
                            07 - PACOTE	TRIBUTOS
                            </button>
                            </form>

                            <form name="frmPacote17" action="{{route('forecast/pacote')}}" method="POST">
                                {!! csrf_field() !!}
                                <input type="hidden" name="codgru" value="17">
                                <input type="hidden" name="CODUNN" value="{{$CODUNN}}">
                                <input type="hidden" name="CODCGA" value="{{$CODCGA}}">
                            <button type="submit" class="list-group-item" style="font-size:12px!important">
                            17 - PACOTE	VIAGENS
                            </button>
                            </form-->
                        </div>
                </div>
            </div>
    <!--/div-->
</div>
@endsection
