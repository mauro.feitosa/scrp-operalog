@php
$menuTitulo = Config::get('menu.menuTitulo');
$menuIcon = Config::get('menu.menuIcon');
$menuUrl = Config::get('menu.menuUrl');
@endphp
@extends('master',['itemMenu'=>$itemMenu])

@section('page_header')
    <h1 class="page-title">
            <i class="{{ $menuIcon[$itemMenu] }}"></i> {{ $menuTitulo[$itemMenu] }}
    </h1>
@stop

@section('content')
<style>
.row>[class*=col-] {
    margin-bottom: 5px;
}
</style>
<div class="page-content container-fluid">
    <!--div class="clearfix container-fluid row"-->
        <div class="alerts"></div>
            <div class="row">
                <div class="col-md-12">
                
                    <div class="col-xs-12 col-sm-6 col-md-4">
                            <form name="frmPacote5" action="{{route('dre/pacote')}}" method="POST">
                                {!! csrf_field() !!}
                            <input type="hidden" name="codgru" value="5">    
                            <button type="submit" class="btn btn-primary btn-block">
                            05 - PACOTE	ADMINISTRATIVO
                            </button>
                            </form>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-4">
                            <form name="frmPacote2" action="{{route('dre/pacote')}}" method="POST">
                                {!! csrf_field() !!}
                                <input type="hidden" name="codgru" value="2">  
                            <button type="submit" class="btn btn-primary btn-block">
                            02 - PACOTE	FROTA
                            </button>            
                            </form>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-4">
                            <form name="frmPacote4" action="{{route('dre/pacote')}}" method="POST">
                                {!! csrf_field() !!}
                                <input type="hidden" name="codgru" value="4">  
                            <button type="submit" class="btn btn-primary btn-block">
                            04 - PACOTE	GENTE
                            </button>   
                            </form>
                    </div>     
                    <div class="col-xs-12 col-sm-6 col-md-4">
                            <form name="frmPacote11" action="{{route('dre/pacote')}}" method="POST">
                                {!! csrf_field() !!}
                                <input type="hidden" name="codgru" value="11">  
                            <button type="submit" class="btn btn-primary btn-block">
                            11 - PACOTE	INVESTIMENTOS
                            </button>             
                            </form>
                    </div>        
                    <div class="col-xs-12 col-sm-6 col-md-4">
                            <form name="frmPacote8" action="{{route('dre/pacote')}}" method="POST">
                                {!! csrf_field() !!}
                                <input type="hidden" name="codgru" value="8">  
                            <button type="submit" class="btn btn-primary btn-block">
                            08 - PACOTE	JURÍDICO
                            </button>             
                            </form>
                    </div>  
                    <div class="col-xs-12 col-sm-6 col-md-4">
                            <form name="frmPacote12" action="{{route('dre/pacote')}}" method="POST">
                                {!! csrf_field() !!}
                                <input type="hidden" name="codgru" value="12">  
                            <button type="submit" class="btn btn-primary btn-block">
                            12 - PACOTE	MANUTENÇÃO PREDIAL
                            </button>            
                            </form>
                    </div>        
                    <div class="col-xs-12 col-sm-6 col-md-4">
                            <form name="frmPacote3" action="{{route('dre/pacote')}}" method="POST">
                                {!! csrf_field() !!}
                                <input type="hidden" name="codgru" value="3">  
                            <button type="submit" class="btn btn-primary btn-block">
                            03 - PACOTE	OPERACIONAL
                            </button>              
                            </form>
                    </div>   
                    <div class="col-xs-12 col-sm-6 col-md-4">
                            <form name="frmPacote14" action="{{route('dre/pacote')}}" method="POST">
                                {!! csrf_field() !!}
                                <input type="hidden" name="codgru" value="14">  
                            <button type="submit" class="btn btn-primary btn-block">
                            14 - PACOTE	REMUNERAÇÃO DE PESSOAL
                            </button>              
                            </form>
                    </div>       
                    <div class="col-xs-12 col-sm-6 col-md-4">
                            <form name="frmPacote15" action="{{route('dre/pacote')}}" method="POST">
                                {!! csrf_field() !!}
                                <input type="hidden" name="codgru" value="15">  
                            <button type="submit" class="btn btn-primary btn-block">
                            15 - PACOTE	TECNOLOGIA DA INFORMAÇÃO
                            </button>              
                            </form>
                    </div>    
                    <div class="col-xs-12 col-sm-6 col-md-4">
                            <form name="frmPacote7" action="{{route('dre/pacote')}}" method="POST">
                                {!! csrf_field() !!}
                                <input type="hidden" name="codgru" value="7">  
                            <button type="submit" class="btn btn-primary btn-block">
                            07 - PACOTE	TRIBUTOS
                            </button>              
                            </form>
                    </div>      
                    <div class="col-xs-12 col-sm-6 col-md-4">
                            <form name="frmPacote17" action="{{route('dre/pacote')}}" method="POST">
                                {!! csrf_field() !!}
                                <input type="hidden" name="codgru" value="17">  
                            <button type="submit" class="btn btn-primary btn-block">
                            17 - PACOTE	VIAGENS
                            </button>              
                            </form>
                    </div>
                </div>
            </div>                      
    <!--/div-->
</div>
@endsection
