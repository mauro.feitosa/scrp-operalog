@php
$menuTitulo = Config::get('menu.menuTitulo');
$menuIcon = Config::get('menu.menuIcon');
$menuUrl = Config::get('menu.menuUrl');
@endphp

@extends('master',['itemMenu'=>$itemMenu])

@section('page_header')

@stop

@section('content')
<style>
.row>[class*=col-] {
    margin-bottom: 5px;
}

.nav-tabs>li.active>a, .nav-tabs>li.active>a:focus, .nav-tabs>li.active>a:hover {
    color: #fff;
    background:  #FFBF00!important;
    font-weight: bold;
}
@media (max-width: 400px) {

}
.table {
        font-size:10px !important;
    }
#parent {
    min-height: height: calc(100vh - 130px);
    height: calc(100vh - 130px);
			}

#fixTable {
    width: 2000px !important;
}
.cabMes {
    width:100px;text-transform: uppercase;
    border-color:#fff;
    color:#fff!important;
    background-color:#000080!important;
    text-align: center;
    height:46px!important;
    font-weight: bold!important;
    vertical-align: middle!important;
}
.cabClass {
    width:400px!important;
    text-transform: uppercase;
    border-color:#fff;
    color:#fff!important;
    background-color:#000080!important;
    text-align: left;
    height:46px!important;
    font-weight: bold!important;
    vertical-align: middle!important;
}
.cabNum {
    width:50px;text-transform: uppercase;
    border-color:#fff;
    color:#fff!important;
    background-color:#000080!important;
    text-align: left;
    height:46px!important;
    font-weight: bold!important;
    vertical-align: middle!important;
}
.cabAltura{
    height: 310px!important);
}
.cabValor {
    height:41px!important;
    vertical-align: middle!important;
}
.cabSintetica{background-color:#B8CCE4;font-weight: bold!important}
.currency{width:100px;}


#parent::-webkit-scrollbar-track
{
    -webkit-box-shadow: inset 0 0 8px rgba(0,0,0,0.3);
    border-radius: 10px;
    background-color: #F1F1F1;
}

#parent::-webkit-scrollbar
{
    width: 8px;
    height: 8px;
    background-color: #F1F1F1;
}

#parent::-webkit-scrollbar-thumb
{
    border-radius: 10px;
    -webkit-box-shadow: inset 0 0 8px rgba(0,0,0,.3);
    background-color: #C1C1C1;
}

.panel-bordered>.panel-body {
    padding: 10px 10px 10px;
    overflow: hidden;
}
body {
    overflow: hidden;
}
.badge:hover {
  color: #ffffff;
  text-decoration: none;
  cursor: pointer;
}
.badge-error {
  background-color: #b94a48;
}
.badge-error:hover {
  background-color: #953b39;
}
.badge-warning {
  background-color: #f89406;
}
.badge-warning:hover {
  background-color: #c67605;
}
.badge-success {
  background-color: #468847;
}
.badge-success:hover {
  background-color: #356635;
}
.badge-info {
  background-color: #3a87ad;
}
.badge-info:hover {
  background-color: #2d6987;
}
.badge-inverse {
  background-color: #333333;
}
.badge-inverse:hover {
  background-color: #1a1a1a;
}
.btn {
    padding: 4px 6px;
    font-size: 12px;
    margin-top: 0px;
    margin-bottom: 0px;
    font-weight: bolder;
}
.formZero{
    margin:0px;
    padding:0px;
}

input {font-weight:bold;}

input[type="text"]:read-only:not([read-only="false"]) { color: blue; background-color: #eee; border-width: 1px!important;border-style: solid!important; border-color: #A9A9A9!important;}
/*body::-webkit-scrollbar-track
{
    -webkit-box-shadow: inset 0 0 8px rgba(0,0,0,0.3);
    border-radius: 10px;
    background-color: #F5F5F5;
}
body::-webkit-scrollbar
{
    width: 8px;
    height: 8px;
    background-color: #F5F5F5;
}

body::-webkit-scrollbar-thumb
{
    border-radius: 10px;
    -webkit-box-shadow: inset 0 0 8px rgba(0,0,0,.3);
    background-color: #555;
}*/
</style>
<div id="#admin" class="page-content container-fluid">
        <div class="alerts"></div>
            <div class="row">
                <div class="col-md-12">
                <form name="frm" method="POST" action="{{route('forecast/pacote')}}" class="formZero">
                    <input type="hidden" value="ok" name="salvar" />
                    {{ csrf_field() }}
                    <div  class="panel panel-bordered">
                    <div style="margin-left:14px;margin-top:8px;font-size:14px;display:table;width:100%">
                        <span class="badge badge-error">{{$CODCGA}}</span>
                        <span type="submit" class="badge badge-info">{{$centrodegasto}}</span> /
                        <span class="badge badge-error">{{$CODUNN}}</span><span class="badge badge-info">{{$unidade}}</span> /
                        <span class="badge badge-warning">{{$pacote}}</span>
                        @if(isset($subitem))
                        / <span class="badge badge-warning">{{$subitem}}</span>
                        @endif
                        <div class="pull-right" style="margin-right:30px;width:300px;">

                            <input type="hidden" name="CODCGA" value="{{$CODCGA}}"/>
                            <input type="hidden" name="CODUNN" value="{{$CODUNN}}"/>
                            <input type="hidden" name="codgru" value="{{$codgru}}"/>

                                <button type="submit" class="btn btn-success pull-right" > <i class="admin-check-circle"></i>  SALVAR DADOS </button>

                        </div>
                    </div>
                        <div class="panel-body">
                            <div id="parent" >
                                <table id="fixTable" class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th class="cabNum">#</th>
                                            <th class="cabClass">Classificação Sintetica / Analitica</th>
                                            <th class="cabMes">Meta<br>Janeiro</th>
                                            <th class="cabMes">Meta<br>Fevereiro</th>
                                            <th class="cabMes">Meta<br>Março</th>
                                            <th class="cabMes">Meta<br>Abril</th>
                                            <th class="cabMes">Meta<br>Maio</th>
                                            <th class="cabMes">Meta<br>Junho</th>
                                            <th class="cabMes">Meta<br>Junho</th>
                                            <th class="cabMes">Meta<br>Agosto</th>
                                            <th class="cabMes">Meta<br>Setembro</th>
                                            <th class="cabMes">Meta<br>Outubro</th>
                                            <th class="cabMes">Meta<br>Novembro</th>
                                            <th class="cabMes">Meta<br>Dezembro</th>
                                            <th class="cabClass">Justificativa</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($pacoteSinteticas as $s)
                                        <tr class="cabSintetica">
                                            <td scope="row"></td>
                                            <td>{{$s->DESCRI}}</td>
                                            <td>R$ <span class="total_jan{{$s->CODCLAP}}">{{str_replace('.',',',$s->VAL1)}}</span></td>
                                            <td>R$ <span class="total_fev{{$s->CODCLAP}}">{{str_replace('.',',',$s->VAL2)}}</span></td>
                                            <td>R$ <span class="total_mar{{$s->CODCLAP}}">{{str_replace('.',',',$s->VAL3)}}</span></td>
                                            <td>R$ <span class="total_abr{{$s->CODCLAP}}">{{str_replace('.',',',$s->VAL4)}}</span></td>
                                            <td>R$ <span class="total_mai{{$s->CODCLAP}}">{{str_replace('.',',',$s->VAL5)}}</span></td>
                                            <td>R$ <span class="total_jun{{$s->CODCLAP}}">{{str_replace('.',',',$s->VAL6)}}</span></td>
                                            <td>R$ <span class="total_jul{{$s->CODCLAP}}">{{str_replace('.',',',$s->VAL7)}}</span></td>
                                            <td>R$ <span class="total_ago{{$s->CODCLAP}}">{{str_replace('.',',',$s->VAL8)}}</span></td>
                                            <td>R$ <span class="total_set{{$s->CODCLAP}}">{{str_replace('.',',',$s->VAL9)}}</span></td>
                                            <td>R$ <span class="total_out{{$s->CODCLAP}}">{{str_replace('.',',',$s->VAL10)}}</span></td>
                                            <td>R$ <span class="total_nov{{$s->CODCLAP}}">{{str_replace('.',',',$s->VAL11)}}</span></td>
                                            <td>R$ <span class="total_dez{{$s->CODCLAP}}">{{str_replace('.',',',$s->VAL12)}}</span></td>
                                            <td></td>
                                        </tr>
                                            @foreach ($pacoteAnaliticas as $a)
                                                @if($s->CODCLAP == $a->PAICLAP)
                                                <tr>
                                                <td class="cabValor" scope="row">{{$a->CODCLAP}}</td>
                                                <td class="cabValor">{{$a->DESCRI}}</td>
                                                <td class="cabValor">
                                                    <input type="text" readonly value="{{str_replace('.',',',$a->VAL1)}}" class=" sum_jan{{$a->PAICLAP}}" name="janfor_{{$a->CODCLAP}}_{{$a->PAICLAP}}">
                                                </td>
                                                <td class="cabValor">
                                                    <input type="text" readonly value="{{str_replace('.',',',$a->VAL2)}}" class=" sum_fev{{$a->PAICLAP}}" name="fevfor_{{$a->CODCLAP}}_{{$a->PAICLAP}}">
                                                </td>
                                                <td class="cabValor">
                                                    <input type="text" readonly value="{{str_replace('.',',',$a->VAL3)}}" class=" sum_mar{{$a->PAICLAP}}" name="marfor_{{$a->CODCLAP}}_{{$a->PAICLAP}}">
                                                </td>
                                                <td class="cabValor">
                                                    <input type="text" readonly value="{{str_replace('.',',',$a->VAL4)}}" class=" sum_abr{{$a->PAICLAP}}" name="abrfor_{{$a->CODCLAP}}_{{$a->PAICLAP}}">
                                                </td>
                                                <td class="cabValor">
                                                    <input type="text" readonly value="{{str_replace('.',',',$a->VAL5)}}" class=" sum_mai{{$a->PAICLAP}}" name="maifor_{{$a->CODCLAP}}_{{$a->PAICLAP}}">
                                                </td>
                                                <td>
                                                    <input type="text" readonly value="{{str_replace('.',',',$a->VAL6)}}" class=" sum_jun{{$a->PAICLAP}}" name="junfor_{{$a->CODCLAP}}_{{$a->PAICLAP}}">
                                                </td>
                                                <td class="cabValor">
                                                    @if(in_array($a->CODCLAP,$analitBoqueia))
                                                    <input type="text" readonly value="{{str_replace('.',',',$a->VAL7)}}" class="sum_jul{{$a->PAICLAP}}" name="jul_{{$a->CODCLAP}}_{{$a->PAICLAP}}">
                                                    @else
                                                    <input type="text" value="{{str_replace('.',',',$a->VAL7)}}" class="currency sum_jul{{$a->PAICLAP}}" name="jul_{{$a->CODCLAP}}_{{$a->PAICLAP}}">
                                                    @endif
                                                </td>
                                                <td class="cabValor">
                                                    @if(in_array($a->CODCLAP,$analitBoqueia))
                                                    <input type="text" readonly  value="{{str_replace('.',',',$a->VAL8)}}" class="sum_ago{{$a->PAICLAP}}" name="ago_{{$a->CODCLAP}}_{{$a->PAICLAP}}">
                                                    @else
                                                    <input type="text" value="{{str_replace('.',',',$a->VAL8)}}" class="currency sum_ago{{$a->PAICLAP}}" name="ago_{{$a->CODCLAP}}_{{$a->PAICLAP}}">
                                                    @endif
                                                </td>
                                                <td>
                                                    @if(in_array($a->CODCLAP,$analitBoqueia))
                                                    <input type="text" readonly value="{{str_replace('.',',',$a->VAL9)}}" class="sum_set{{$a->PAICLAP}}" name="set_{{$a->CODCLAP}}_{{$a->PAICLAP}}">
                                                    @else
                                                    <input type="text" value="{{str_replace('.',',',$a->VAL9)}}" class="currency sum_set{{$a->PAICLAP}}" name="set_{{$a->CODCLAP}}_{{$a->PAICLAP}}">
                                                    @endif
                                                </td>
                                                <td class="cabValor">
                                                    @if(in_array($a->CODCLAP,$analitBoqueia))
                                                    <input type="text" readonly value="{{str_replace('.',',',$a->VAL10)}}" class="sum_out{{$a->PAICLAP}}" name="out_{{$a->CODCLAP}}_{{$a->PAICLAP}}">
                                                    @else
                                                    <input type="text" value="{{str_replace('.',',',$a->VAL10)}}" class="currency sum_out{{$a->PAICLAP}}" name="out_{{$a->CODCLAP}}_{{$a->PAICLAP}}">
                                                    @endif
                                                </td>
                                                <td class="cabValor">
                                                    @if(in_array($a->CODCLAP,$analitBoqueia))
                                                    <input type="text" readonly value="{{str_replace('.',',',$a->VAL11)}}" class="sum_nov{{$a->PAICLAP}}" name="novr_{{$a->CODCLAP}}_{{$a->PAICLAP}}">
                                                    @else
                                                    <input type="text" value="{{str_replace('.',',',$a->VAL11)}}" class="currency sum_nov{{$a->PAICLAP}}" name="nov_{{$a->CODCLAP}}_{{$a->PAICLAP}}">
                                                    @endif
                                                </td>
                                                <td class="cabValor">
                                                    @if(in_array($a->CODCLAP,$analitBoqueia))
                                                    <input type="text" readonly value="{{str_replace('.',',',$a->VAL12)}}" class="sum_dez{{$a->PAICLAP}}" name="dez_{{$a->CODCLAP}}_{{$a->PAICLAP}}">
                                                    @else
                                                    <input type="text" value="{{str_replace('.',',',$a->VAL12)}}" class="currency sum_dez{{$a->PAICLAP}}" name="dez_{{$a->CODCLAP}}_{{$a->PAICLAP}}">
                                                    @endif
                                                </td>
                                                <td class="cabValor">
                                                <input type="text" name="just_{{$a->CODCLAP}}_{{$a->PAICLAP}}" value="{{$a->TEXTO}}">
                                                </td>
                                                </tr>
                                                @endif
                                            @endforeach
                                        @endforeach
                                    </tbody>
                                </table>
                                </div>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
</div>
@endsection
@section('javascript')
<script>

			$(document).ready(function() {
				$("#fixTable").tableHeadFixer({"left" : 2});

                $(window).keydown(function(event){
                    if(event.keyCode == 13) {
                    event.preventDefault();
                    return false;
                    }
                });

                //var Scrollbar = window.Scrollbar;

//Scrollbar.init(document.querySelector('#parent'));
			});


        $(function() {
          $(".currency").maskMoney({prefix:'R$ ', allowZero: true, allowNegative: true, thousands:'.', decimal:',', affixesStay: false});

        });

        Number.prototype.format = function(n, x, s, c) {
            var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\D' : '$') + ')',
                num = this.toFixed(Math.max(0, ~~n));

            return (c ? num.replace('.', c) : num).replace(new RegExp(re, 'g'), '$&' + (s || ','));
        };
        @php
           $mesJs = ["jan","fev","mar","abr","mai","jun","jul","ago","set","out","nov","dez"];
        @endphp


        @foreach ($mesJs as $m)

        @foreach ($pacoteSinteticas as $s)
        $(document).on("change", ".sum_{{$m}}{{$s->CODCLAP}}", function() {
        //    console.log('oi');
        var sum = 0;
        var tmp = 0.00;
            $(".sum_{{$m}}{{$s->CODCLAP}}").each(function(){
                //console.log($(this).val());
        var str = $(this).val();
        sum += parseFloat(str.replace('.','').replace(',','.').replace(' ',''));
        tmp = sum;
        console.log(sum);
        //tmp = ss.toString();
        console.log(tmp.format(2, 3, '.', ','));
        ///sum += +$(this).val();
            //$(".total_{{$s->CODCLAP}}").maskMoney('mask',tmp);
            $(".total_{{$m}}{{$s->CODCLAP}}").html(tmp.format(2, 3, '.', ','));
        });
        //$(".total_{{$s->CODCLAP}}").maskMoney('mask', sum);
        //$(".total_{{$s->CODCLAP}}").maskMoney('mask',tmp.replace(',','.').replace(' ',''));
        //$(".total_txt{{$s->CODCLAP}}").text($(".total_{{$s->CODCLAP}}").val());
        //$(".total_{{$s->CODCLAP}}").html(sum);
        //console.log(sum);
        });
        @endforeach

        @endforeach

</script>
@endsection
