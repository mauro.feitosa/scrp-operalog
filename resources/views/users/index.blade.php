@extends('master',['itemMenu'=>$itemMenu])

@section('page_header')
    <h1 class="page-title">
    <i class="{{ $pageIcon }}"></i> {{ $pageTitulo }}
    </h1>
@stop

@section('content')
<style>
.row>[class*=col-] {
    margin-bottom: 5px;
}
</style>
<div class="page-content container-fluid">

        <div class="alerts"></div>
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-bordered">
                        <div class="panel-body">
                        <table class="table table-bordered" id="laravel_datatable">
                                <thead>
                                   <tr>
                                      <th>Id</th>
                                      <th>Nome</th>
                                      <th>Email</th>
                                      <th>Criado em</th>
                                      <th width="100px">Ação</th>
                                   </tr>
                                </thead>
                        </table>
                        </div>
                    </div>        
                </div>
            </div>                      
</div>
@endsection
@section('javascript')
<script>
        $(document).ready( function () {
         $('#laravel_datatable').DataTable({
                processing: true,
                serverSide: true,
                ajax: "{{ url('users-list') }}",
                columns: [
                         { data: 'id', name: 'id' },
                         { data: 'name', name: 'name' },
                         { data: 'email', name: 'email' },
                         { data: 'created_at', name: 'created_at' },
                         { data: 'action', name: 'action', orderable: false, searchable: false}
                      ],
                      language:{
                        "sEmptyTable": "Nenhum registro encontrado",
                        "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
                        "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
                        "sInfoFiltered": "(Filtrados de _MAX_ registros)",
                        "sInfoPostFix": "",
                        "sInfoThousands": ".",
                        "sLengthMenu": "_MENU_ resultados por página",
                        "sLoadingRecords": "Carregando...",
                        "sProcessing": "Processando...",
                        "sZeroRecords": "Nenhum registro encontrado",
                        "sSearch": "Pesquisar",
                        "oPaginate": {
                            "sNext": "Próximo",
                            "sPrevious": "Anterior",
                            "sFirst": "Primeiro",
                            "sLast": "Último"
                        },
                        "oAria": {
                            "sSortAscending": ": Ordenar colunas de forma ascendente",
                            "sSortDescending": ": Ordenar colunas de forma descendente"
                        }
                    }
                      
             });
          });
</script>
@endsection