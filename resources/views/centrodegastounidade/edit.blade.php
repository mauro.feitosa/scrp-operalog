@extends('master',['itemMenu'=>$itemMenu])

@section('page_header')

@php
$menuTitulo = Config::get('menu.menuTitulo');
$menuIcon = Config::get('menu.menuIcon');
$menuUrl = Config::get('menu.menuUrl');
@endphp
    <h1 class="page-title">
    <i class="{{ $menuIcon[$itemMenu] }}"></i> {{ $menuTitulo[$itemMenu] }}
    </h1>
@stop

@section('content')
<style>
.row>[class*=col-] {
    margin-bottom: 5px;
}
</style>
<div class="page-content container-fluid">

        <div class="alerts"></div>
            <div class="row">
                <div class="col-md-6">
                    <div class="panel panel-bordered">
                        <form name="frmAdd" method="POST" action="{{route('centrodegastounidade.update',$codcga)}}">
                        {{ csrf_field() }}
                        <input type="hidden" name="_method" value="PUT">
                        <div class="panel-body">
                                    <div class="form-group  ">
                                            <label for="name">Centro de Gasto</label>
                                            <select class="form-control select2" name="CODCGA">
                                                @foreach ($listaRodcga as $lr)
                                                    @if($lr->CODCGA == $codcga)
                                                        <option selected="selected" value="{{$lr->CODCGA}}">{{$lr->CODCGA}} - {{$lr->CENTRODEGASTO}}</option>
                                                    @else
                                                        <option value="{{$lr->CODCGA}}">{{$lr->CODCGA}} - {{$lr->CENTRODEGASTO}}</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                    </div>
                                    <label for="name">Unidade</label>
                                    @foreach ($listaRodunn as $un)
                                    
                                    <div class="form-group" >
                                            <label style="font-weight:normal!important;"><input type="checkbox"  @if(in_array($un->CODUNN,$unidades)) checked="checked" @endif name="CODUNN[]" value="{{$un->CODUNN}}">{{$un->CODUNN}} - {{$un->UNIDADE}}</label>
                                    </div>    
                                    @endforeach
                        </div>
                        <div class="panel-footer">
                                <button type="submit" class="btn btn-primary save">Salvar</button>
                        </div>
                       </form> 
                    </div>        
                </div>
            </div>                      
</div>
@endsection
@section('javascript')
<script>
        $(document).ready( function () {
          });
</script>
@endsection