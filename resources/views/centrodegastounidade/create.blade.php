@extends('master',['itemMenu'=>$itemMenu])

@section('page_header')

@php
$menuTitulo = Config::get('menu.menuTitulo');
$menuIcon = Config::get('menu.menuIcon');
$menuUrl = Config::get('menu.menuUrl');
@endphp
    <h1 class="page-title">
    <i class="{{ $menuIcon[$itemMenu] }}"></i> {{ $menuTitulo[$itemMenu] }}
    </h1>
@stop

@section('content')
<style>
.row>[class*=col-] {
    margin-bottom: 5px;
}
</style>
<div class="page-content container-center">

        <div class="alerts"></div>
            <div class="row">
                <div class="col-md-6">
                    <div class="panel panel-bordered">
                        <form name="frmAdd" method="POST" action="{{route('centrodegastounidade.store')}}">
                        {{ csrf_field() }}
                        <div class="panel-body">
                                    <div class="form-group  ">
                                            <label for="name">Centro de Gasto</label>
                                            <select class="form-control select2" name="CODCGA">
                                                @foreach ($listaRodcga as $lr)
                                                <option value="{{$lr->CODCGA}}">{{$lr->CODCGA}} - {{$lr->CENTRODEGASTO}}</option>
                                                @endforeach
                                            </select>
                                    </div>
                                    <label for="name">Unidade</label>
                                    @foreach ($listaRodunn as $un)
                                    
                                    <div class="form-group" >
                                            <label style="font-weight:normal!important;"><input type="checkbox" name="CODUNN[]" value="{{$un->CODUNN}}">{{$un->CODUNN}} - {{$un->UNIDADE}}</label>
                                    </div>    
                                    @endforeach
                               
                          
                        </div>
                        <div class="panel-footer">
                                <button type="submit" class="btn btn-primary save">Salvar</button>
                        </div>
                       </form> 
                    </div>        
                </div>
            </div>                      
</div>
@endsection
@section('javascript')
<script>
        $(document).ready( function () {
          });
</script>
@endsection