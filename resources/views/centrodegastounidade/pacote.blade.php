@extends('master',['itemMenu'=>$itemMenu])

@section('page_header')

@php
$menuTitulo = Config::get('menu.menuTitulo');
$menuIcon = Config::get('menu.menuIcon');
$menuUrl = Config::get('menu.menuUrl');
@endphp
    <h1 class="page-title">
    <i class="{{ $menuIcon[$itemMenu] }}"></i> {{ $menuTitulo[$itemMenu] }}
    </h1>
@stop

@section('content')
<style>
.row>[class*=col-] {
    margin-bottom: 5px;
}
</style>
<div class="page-content container-fluid">

        <div class="alerts"></div>
            <div class="row">
                <div class="col-md-8">
                    <div class="panel panel-bordered">
                    <form name="frmPacote" method="POST" enctype="multipart/form-data" action="{{route('centrodegastounidade/pacote')}}" >
                        {{ csrf_field() }}
                        <div class="form-group">
                                <table>
                                    <tr>
                                        <td valign="top">
                                            <ul class="list-group" style="font-size:10px!important;padding-left:10px;">
                                            <li class="list-group-item active">PACOTES</li>
                                            <li class="list-group-item" style="background-color:lightgrey"><label><input id="checkAll3" type="checkbox"> MARCAR/DESMARCAR TODOS </label></li>
                                                @foreach ($RODGRU as $pac)

                                                    @if(in_array($pac->CODIGO,$lista_pacote_menu))
                                                        <li class="list-group-item"><label><input checked="checked" class="item3" name="codigo[]" type="checkbox" value="{{$pac->CODIGO}}"> {{$pac->CODIGO}} - {{$pac->DESCRI}}</label></li>
                                                    @else
                                                        <li class="list-group-item"><label><input class="item3" name="codigo[]" type="checkbox" value="{{$pac->CODIGO}}"> {{$pac->CODIGO}} - {{$pac->DESCRI}}</label></li>
                                                    @endif

                                                @endforeach
                                        </ul>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        <div class="panel-footer">
                                {{ Form::submit('Atualizar', array('class' => 'btn btn-primary')) }}
                        </div>
                       </form>
                    </div>
                </div>
            </div>
</div>
@endsection
@section('javascript')
<script>
    $('#checkAll1').click(function () {
    $(':checkbox.item1').prop('checked', this.checked);
    });

    $('#checkAll2').click(function () {
    $(':checkbox.item2').prop('checked', this.checked);
    });

    $('#checkAll3').click(function () {
    $(':checkbox.item3').prop('checked', this.checked);
    });

        $(document).ready( function () {



          });
</script>
@endsection
