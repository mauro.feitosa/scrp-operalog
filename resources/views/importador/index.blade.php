@php
$menuTitulo = Config::get('menu.menuTitulo');
$menuIcon = Config::get('menu.menuIcon');
$menuUrl = Config::get('menu.menuUrl');
@endphp
@extends('master',['itemMenu'=>$itemMenu])

@section('page_header')
    <h1 class="page-title">
    <i class="{{ $menuIcon[$itemMenu] }}"></i> {{ $menuTitulo[$itemMenu] }}
    </h1>
@stop

@section('content')
<style>
.row>[class*=col-] {
    margin-bottom: 5px;
}
</style>
<div class="page-content container-fluid">
    <!--div class="clearfix container-fluid row"-->
        <div class="alerts"></div>
            <div class="row">
                <div class="col-md-12">
                    <div class="col-xs-12 col-sm-6 col-md-4">
                        <form action="{{ route('importador/rodcus')}}" method="POST" name="rodcus" id="rodcus">
                                <input class="btn  btn-success" id="startProgressRodcus" type="submit" name="startProgress" value="Carregar RODCUS"/> <span id="respostaRodcus"></span>
                            </form>

                            <div class="progress" style="margin-top:10px">
                                <div class="progress-bar active" id="progressbarRodcus" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 0%; background-color: #399;">
                                </div>
                                <div id="erro"></div>
                            </div>
                    </div>

                    <div class="col-xs-12 col-sm-6 col-md-4">
                        <form action="{{ route('importador/rodgru')}}" method="POST" name="rodgru" id="rodgru">
                                <input class="btn  btn-success" id="startProgressRodgru" type="submit" name="startProgress" value="Carregar RODGRU"/> <span id="respostaRodgru"></span>
                            </form>

                            <div class="progress" style="margin-top:10px">
                                <div class="progress-bar active" id="progressbarRodgru" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 0%; background-color: #399;">
                                </div>
                                <div id="erro"></div>
                            </div>
                    </div>

                    <div class="col-xs-12 col-sm-6 col-md-4">
                        <form action="{{ route('importador/rodunn')}}" method="POST" name="rodunn" id="rodunn">
                                <input class="btn  btn-success" id="startProgressRodunn" type="submit" name="startProgress" value="Carregar RODUNN"/> <span id="respostaRodunn"></span>
                            </form>

                            <div class="progress" style="margin-top:10px">
                                <div class="progress-bar active" id="progressbarRodunn" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 0%; background-color: #399;">
                                </div>
                                <div id="erro"></div>
                            </div>
                    </div>

                    <div class="col-xs-12 col-sm-6 col-md-4">
                        <form action="{{ route('importador/rodcga')}}" method="POST" name="rodcga" id="rodcga">
                                <input class="btn  btn-success" id="startProgressRodcga" type="submit" name="startProgress" value="Carregar RODCGA"/> <span id="respostaRodcga"></span>
                            </form>

                            <div class="progress" style="margin-top:10px">
                                <div class="progress-bar active" id="progressbarRodcga" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 0%; background-color: #399;">
                                </div>
                                <div id="erro"></div>
                            </div>
                    </div>

                    <div class="col-xs-12 col-sm-6 col-md-4">
                        <form action="{{ route('importador/pagcla')}}" method="POST" name="pagcla" id="pagcla">
                                <input class="btn  btn-success" id="startProgressPagcla" type="submit" name="startProgress" value="Carregar PAGCLA"/> <span id="respostaPagcla"></span>
                            </form>

                            <div class="progress" style="margin-top:10px">
                                <div class="progress-bar active" id="progressbarPagcla" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 0%; background-color: #399;">
                                </div>
                                <div id="erro"></div>
                            </div>
                    </div>

                    <div class="col-xs-12 col-sm-6 col-md-4">
                        <form action="{{ route('importador/cargo')}}" method="POST" name="cargo" id="cargo">
                                <input class="btn  btn-success" id="startProgressCargo" type="submit" name="startProgress" value="Carregar CARGOS"/> <span id="respostaCargo"></span>
                            </form>

                            <div class="progress" style="margin-top:10px">
                                <div class="progress-bar active" id="progressbarCargo" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 0%; background-color: #399;">
                                </div>
                                <div id="erro"></div>
                            </div>
                    </div>


                    <div class="col-xs-12 col-sm-6 col-md-4">
                                <input onclick="publica()" class="btn  btn-success" id="startprogressbarDre" type="submit" name="startprogressbarDre" value="Carregar DRE"/> <span id="respostaDre"></span>
                            <div class="progress" style="margin-top:10px">
                                <div class="progress-bar active" id="progressbarDre" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 0%; background-color: #399;">
                                </div>
                                <div id="erro"></div>
                            </div>
                    </div>

                </div>
            </div>


    <!--/div-->
</div>
@endsection

@section('javascript')
<script>

$('document').ready(function () {
        $('#startProgressRodcus').click(function () {
            $('#respostaRodcus').html('');
            $('#progressbarRodcus').css('width', '0% ; background-color: #399');
            getProgressRodcus();

            $.ajax({
                url: "importador/rodcus",
                type: "GET",
                async: true, //IMPORTANT!
                contentType: false,
                processData: false,
                success: function(data){
                    if(data!==''){
                        //alert(data);
                        $('#respostaRodcus').html(data);
                    }
                    return false;
                }
            });
            return false;
        });

        $('#startProgressRodgru').click(function () {
            $('#respostaRodgru').html('');
            $('#progressbarRodgru').css('width', '0% ; background-color: #399');
            getProgressRodgru();

            $.ajax({
                url: "importador/rodgru",
                type: "GET",
                async: true, //IMPORTANT!
                contentType: false,
                processData: false,
                success: function(data){
                    if(data!==''){
                        //alert(data);
                        $('#respostaRodgru').html(data);
                    }
                    return false;
                }
            });
            return false;
        });

        $('#startProgressRodunn').click(function () {
            $('#respostaRodunn').html('');
            $('#progressbarRodunn').css('width', '0% ; background-color: #399');
            getProgressRodunn();

            $.ajax({
                url: "importador/rodunn",
                type: "GET",
                async: true, //IMPORTANT!
                contentType: false,
                processData: false,
                success: function(data){
                    if(data!==''){
                        //alert(data);
                        $('#respostaRodunn').html(data);
                    }
                    return false;
                }
            });
            return false;
        });

        $('#startProgressRodcga').click(function () {
            $('#respostaRodcga').html('');
            $('#progressbarRodcga').css('width', '0% ; background-color: #399');
            getProgressRodcga();

            $.ajax({
                url: "importador/rodcga",
                type: "GET",
                async: true, //IMPORTANT!
                contentType: false,
                processData: false,
                success: function(data){
                    if(data!==''){
                        //alert(data);
                        $('#respostaRodcga').html(data);
                    }
                    return false;
                }
            });
            return false;
        });

        $('#startProgressPagcla').click(function () {
            $('#respostaPagcla').html('');
            $('#progressbarPagcla').css('width', '0% ; background-color: #399');
            getProgressPagcla();

            $.ajax({
                url: "importador/pagcla",
                type: "GET",
                async: true, //IMPORTANT!
                contentType: false,
                processData: false,
                success: function(data){
                    if(data!==''){
                        //alert(data);
                        $('#respostaPagcla').html(data);
                    }
                    return false;
                }
            });
            return false;
        });

        $('#startProgressCargo').click(function () {
            $('#respostaCargo').html('');
            $('#progressbarCargo').css('width', '0% ; background-color: #399');
            getProgressCargo();

            $.ajax({
                url: "importador/cargo",
                type: "GET",
                async: true, //IMPORTANT!
                contentType: false,
                processData: false,
                success: function(data){
                    if(data!==''){
                        //alert(data);
                        $('#respostaCargo').html(data);
                    }
                    return false;
                }
            });
            return false;
        });

    });

    function getProgressRodcus() {
        $.ajax({
            url: "importador/getprogressrodcus",
            type: "GET",
            contentType: false,
            processData: false,
            async: false,
            success: function (data) {
                console.log(data);
                $('#progressbarRodcus').css('width', data+'%');
                if(data!=='100'){
                    setTimeout("getProgressRodcus()", 5000);
                }
            },
            error: function (request, status, error) {
                alert(request.responseText);
                return false;
            }
        });
    }

    function getProgressRodgru() {
        $.ajax({
            url: "importador/getprogressrodgru",
            type: "GET",
            contentType: false,
            processData: false,
            async: false,
            success: function (data) {
                console.log(data);
                $('#progressbarRodgru').css('width', data+'%');
                if(data!=='100'){
                    setTimeout("getProgressRodgru()", 5000);
                }
            },
            error: function (request, status, error) {
                alert(request.responseText);
                return false;
            }
        });
    }

    function getProgressRodunn() {
        $.ajax({
            url: "importador/getprogressrodunn",
            type: "GET",
            contentType: false,
            processData: false,
            async: false,
            success: function (data) {
                console.log(data);
                $('#progressbarRodunn').css('width', data+'%');
                if(data!=='100'){
                    setTimeout("getProgressRodunn()", 5000);
                }
            },
            error: function (request, status, error) {
                alert(request.responseText);
                return false;
            }
        });
    }

    function getProgressRodcga() {
        $.ajax({
            url: "importador/getprogressrodcga",
            type: "GET",
            contentType: false,
            processData: false,
            async: false,
            success: function (data) {
                console.log(data);
                $('#progressbarRodcga').css('width', data+'%');
                if(data!=='100'){
                    setTimeout("getProgressRodcga()", 5000);
                }
            },
            error: function (request, status, error) {
                alert(request.responseText);
                return false;
            }
        });
    }

    function getProgressPagcla() {
        $.ajax({
            url: "importador/getprogresspagcla",
            type: "GET",
            contentType: false,
            processData: false,
            async: false,
            success: function (data) {
                console.log(data);
                $('#progressbarPagcla').css('width', data+'%');
                if(data!=='100'){
                    setTimeout("getProgressPagcla()", 5000);
                }
            },
            error: function (request, status, error) {
                alert(request.responseText);
                return false;
            }
        });
    }

    function getProgressCargo() {
        $.ajax({
            url: "importador/getprogresscargo",
            type: "GET",
            contentType: false,
            processData: false,
            async: false,
            success: function (data) {
                console.log(data);
                $('#progressbarCargo').css('width', data+'%');
                if(data!=='100'){
                    setTimeout("getProgressCargo()", 5000);
                }
            },
            error: function (request, status, error) {
                alert(request.responseText);
                return false;
            }
        });
    }

    publica = function() {

    //console.log();

    getProgress();

    $.ajax({
        type:"post",
        url:"importador/importar",
        //data:"codcga="+codcga,
        success:function(data){
            $('#progressbarDre').css("width", data + '%');
        }
    });
    //  $('#progress'+codcga).css("width", codcga + '%');
    }

    getProgress = function() {
        $.ajax({
            url: "importador/getprogress",
            type: "post",
            //data:"codcga="+codcga,
            success: function (data) {
                console.log(data);
                $('#progressbarDre').css('width', data+'%');
                if(data!=='100'){
                    setTimeout(getprogress(), 4000);
                }
            },
            error: function (request, status, error) {
                alert(request.responseText);
                return false;
            }
        });
    }

</script>
@endsection
