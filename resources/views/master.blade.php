<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <title>@yield('page_title')</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}"/>

    <!-- Favicon -->
    <link rel="shortcut icon" href="{{ asset('assets/images/logo-icon.png') }}" type="image/x-icon">

    <!-- App CSS -->
    <link rel="stylesheet" href="{{ asset('assets/css/app.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/bootstrap-table-expandable.css') }}">
    @yield('css')

    <!-- Few Dynamic Styles -->
    <style type="text/css">
        .admin .side-menu .navbar-header {
            background:#22A7F0;
            border-color:#22A7F0;
        }
        .widget .btn-primary{
            border-color:#22A7F0;
        }
        .widget .btn-primary:focus, .widget .btn-primary:hover, .widget .btn-primary:active, .widget .btn-primary.active, .widget .btn-primary:active:focus{
            background:#22A7F0;
        }
        .admin .breadcrumb a{
            color:#22A7F0;
        }
    </style>

    @yield('head')
</head>

<body class="admin">

@yield('popup')

<div id="admin-loader">
        <img src="{{ asset('assets/images/admin_loader.gif') }}" alt="Admin Loader">
</div>

<?php

$user_avatar = Auth::user()->avatar;
if ((substr(Auth::user()->avatar, 0, 7) == 'http://') || (substr(Auth::user()->avatar, 0, 8) == 'https://')) {
    $user_avatar = Auth::user()->avatar;
}

?>

<div class="app-container">
    <div class="fadetoblack visible-xs"></div>
    <div class="row content-container">
        @include('dashboard.navbar')
        @include('dashboard.sidebar')
        <script>
            (function(){
                    window.localStorage['admin.stickySidebar'] = 'false';
                    var appContainer = document.querySelector('.app-container'),
                        sidebar = appContainer.querySelector('.side-menu'),
                        navbar = appContainer.querySelector('nav.navbar.navbar-top'),
                        loader = document.getElementById('admin-loader'),
                        hamburgerMenu = document.querySelector('.hamburger'),
                        sidebarTransition = sidebar.style.transition,
                        navbarTransition = navbar.style.transition,
                        containerTransition = appContainer.style.transition;

                    sidebar.style.WebkitTransition = sidebar.style.MozTransition = sidebar.style.transition =
                    appContainer.style.WebkitTransition = appContainer.style.MozTransition = appContainer.style.transition =
                    navbar.style.WebkitTransition = navbar.style.MozTransition = navbar.style.transition = 'none';

                    if (window.localStorage && window.localStorage['admin.stickySidebar'] == 'true') {
                        appContainer.className += ' expanded no-animation';
                        loader.style.left = (sidebar.clientWidth/2)+'px';
                        hamburgerMenu.className += ' is-active no-animation';
                    }

                   navbar.style.WebkitTransition = navbar.style.MozTransition = navbar.style.transition = navbarTransition;
                   sidebar.style.WebkitTransition = sidebar.style.MozTransition = sidebar.style.transition = sidebarTransition;
                   appContainer.style.WebkitTransition = appContainer.style.MozTransition = appContainer.style.transition = containerTransition;
            })();
        </script>
        <!-- Main Content -->
        <div class="container-fluid">
            <div class="side-body padding-top">
                @yield('page_header')
                <div id="admin-notifications"></div>
                @yield('content')
            </div>
        </div>
    </div>
</div>
@include('partials.app-footer')

<!-- Javascript Libs -->

<script type="text/javascript" src="{{ asset('assets/js/app.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/jquery.maskMoney.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/tableHeadFixer.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/scrollbar/smooth-scrollbar.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/bootstrap-table-expandable.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/loadingoverlay.min.js') }}"></script>
<script>

</script>
@yield('javascript')
</body>
</html>
