@extends('master',['itemMenu'=>$itemMenu])

@section('page_header')
    <h1 class="page-title">
    <i class="{{ $pageIcon }}"></i> {{ $pageTitulo }}
    </h1>
@stop

@section('content')
<style>
.row>[class*=col-] {
    margin-bottom: 5px;
}
</style>
<div class="page-content container-fluid">
    <!--div class="clearfix container-fluid row"-->
        <div class="alerts"></div>
            <div class="row">
                <div class="col-md-12">
                    <div class="col-xs-12 col-sm-6 col-md-4">
                            <button type="submit" class="btn btn-primary btn-block">
                            05	ADMINISTRATIVO
                            </button>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-4">
                            <button type="submit" class="btn btn-primary btn-block">
                            02	FROTA
                            </button>            
                    
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-4">
                            <button type="submit" class="btn btn-primary btn-block">
                            04	GENTE
                            </button>   
                    
                    </div>     
                    <div class="col-xs-12 col-sm-6 col-md-4">
                            <button type="submit" class="btn btn-primary btn-block">
                            11	INVESTIMENTOS
                            </button>             
                    
                    </div>        
                    <div class="col-xs-12 col-sm-6 col-md-4">
                            <button type="submit" class="btn btn-primary btn-block">
                            08	JURÍDICO
                            </button>             
                    
                    </div>  
                    <div class="col-xs-12 col-sm-6 col-md-4">
                            <button type="submit" class="btn btn-primary btn-block">
                            12	MANUTENÇÃO PREDIAL
                            </button>            
                    
                    </div>        
                    <div class="col-xs-12 col-sm-6 col-md-4">
                            <button type="submit" class="btn btn-primary btn-block">
                            03	OPERACIONAL
                            </button>              
                    
                    </div>   
                    <div class="col-xs-12 col-sm-6 col-md-4">
                            <button type="submit" class="btn btn-primary btn-block">
                            14	REMUNERAÇÃO DE PESSOAL
                            </button>              
                    
                    </div>       
                    <div class="col-xs-12 col-sm-6 col-md-4">
                            <button type="submit" class="btn btn-primary btn-block">
                            15	TECNOLOGIA DA INFORMAÇÃO
                            </button>              
                    
                    </div>    
                    <div class="col-xs-12 col-sm-6 col-md-4">
                            <button type="submit" class="btn btn-primary btn-block">
                            07	TRIBUTOS
                            </button>              
                
                    </div>      
                    <div class="col-xs-12 col-sm-6 col-md-4">
                            <button type="submit" class="btn btn-primary btn-block">
                            17	VIAGENS
                            </button>              
                    
                    </div>
                </div>
            </div>                      
    <!--/div-->
</div>
@endsection
