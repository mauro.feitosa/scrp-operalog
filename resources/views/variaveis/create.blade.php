@extends('master',['itemMenu'=>$itemMenu])

@section('page_header')

@php
$menuTitulo = Config::get('menu.menuTitulo');
$menuIcon = Config::get('menu.menuIcon');
$menuUrl = Config::get('menu.menuUrl');
@endphp
    <h1 class="page-title">
    <i class="{{ $menuIcon[$itemMenu] }}"></i> {{ $menuTitulo[$itemMenu] }}
    </h1>
@stop

@section('content')
<style>
.row>[class*=col-] {
    margin-bottom: 5px;
}
</style>
<div class="page-content container-center">

        <div class="alerts"></div>
            <div class="row">
                <div class="col-md-6">
                    <div class="panel panel-bordered">
                        <form name="frmAdd" method="POST" action="{{route('variaveis.store')}}">
                        {{ csrf_field() }}
                        <div class="panel-body">
                                    <div class="form-group  ">
                                      <label for="name">Descrição do item</label>
                                      <input type="text" name="DESCRI" class="form-control" />
                                    </div>
                                    <div class="form-group  ">
                                            <label for="name">Grupo Variaveis</label>
                                            <select class="form-control select2" name="CODUNN">
                                                @foreach ($listaUND_VARIAVEIS as $lr)
                                                <option value="{{$lr->CODUNN}}">{{$lr->CODUNN}} - {{$lr->UNIDADE}}</option>
                                                @endforeach
                                            </select>
                                    </div>
                                    <div class="form-group  ">
                                        <label for="name">Item Pai</label>
                                        <select class="form-control select2" name="IDPAI">
                                            @foreach ($listaPai as $lp)
                                            @if($lp->ID == 0)
                                            <option selected="selected" value="{{$lp->ID}}">{{$lp->ID}} - {{$lp->DESCRI}}</option>
                                            @else
                                            <option value="{{$lp->ID}}">{{$lp->ID}} - {{$lp->DESCRI}}</option>
                                            @endif
                                            @endforeach
                                        </select>
                                   </div>

                                   <div class="form-group" >
                                        <label for="name">Tipo</label>
                                        <select class="form-control select2" name="TIPO">
                                            @foreach ($arrayTipo as $chave => $valor )
                                                <option value="{{$valor}}">{{$valor}} - {{$chave}}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="form-group" >
                                        <label for="name">Função</label>
                                        <input class="form-control" type="text" name="OBJTIPO" />
                                    </div>

                                    <div class="form-group" >
                                        <label for="name">Ordem Função</label>
                                        <input class="form-control" type="text" name="ORDEMFUNC" />
                                    </div>

                                    <div class="form-group" >
                                        <label for="name">Ordem</label>
                                    <input class="form-control" type="text" value="{{$ordem}}" name="ORDEM" />
                                    </div>


                        </div>
                        <div class="panel-footer">
                                <button type="submit" class="btn btn-primary save">Salvar</button>
                        </div>
                       </form>
                    </div>
                </div>
            </div>
</div>
@endsection
@section('javascript')
<script>
        $(document).ready( function () {
          });
</script>
@endsection
