<table>
    <thead>
        <tr>
            <th>#</th>
            <th>Classificação Sintetica / Analitica</th>
            <th >Janeiro</th>
            <th >Fevereiro</th>
            <th >Março</th>
            <th >Abril</th>
            <th >Maio</th>
            <th >Junho</th>
            <th >Junho</th>
            <th >Agosto</th>
            <th >Setembro</th>
            <th >Outubro</th>
            <th >Novembro</th>
            <th >Dezembro</th>
            <th >Justificativa</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($pacoteSinteticas as $s)
        <tr>
            <td></td>
            <td>{{$s->DESCRI}}</td>
            <td>{{$s->VAL1}}</td>
            <td>{{$s->VAL2}}</td>
            <td>{{$s->VAL3}}</td>
            <td>{{$s->VAL4}}</td>
            <td>{{$s->VAL5}}</td>
            <td>{{$s->VAL6}}</td>
            <td>{{$s->VAL7}}</td>
            <td>{{$s->VAL8}}</td>
            <td>{{$s->VAL9}}</td>
            <td>{{$s->VAL10}}</td>
            <td>{{$s->VAL11}}</td>
            <td>{{$s->VAL12}}</td>
            <td></td>
        </tr>
            @foreach ($pacoteAnaliticas as $a)
                @if($s->CODCLAP == $a->PAICLAP)
                <tr>
                <td>{{$a->CODCLAP}}</td>
                <td >{{$a->DESCRI}}</td>
                <td >
                    {{$a->VAL1}}
                </td>
                <td >
                    {{$a->VAL2}}
                </td>
                <td >
                    {{$a->VAL3}}
                </td>
                <td >
                    {{$a->VAL4}}
                </td>
                <td >
                    {{$a->VAL5}}
                </td>
                <td>
                    {{$a->VAL6}}
                </td>
                <td >
                    {{$a->VAL7}}
                </td>
                <td >
                    {{$a->VAL8}}
                </td>
                <td>
                    {{$a->VAL9}}
                </td>
                <td >
                    {{$a->VAL10}}
                </td>
                <td >
                    {{$a->VAL11}}
                </td>
                <td >
                    {{$a->VAL12}}
                </td>
                <td >
                {{$a->TEXTO}}
                </td>
                </tr>
                @endif
            @endforeach
        @endforeach
    </tbody>
</table>
