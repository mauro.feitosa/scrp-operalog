<table>
    <thead>
        <tr>
            <th class="cabNum">#</th>
            <th class="cabClass">Itens</th>
            <th class="cabMes">Meta<br>Janeiro</th>
            <th class="cabMes">Meta<br>Fevereiro</th>
            <th class="cabMes">Meta<br>Março</th>
            <th class="cabMes">Meta<br>Abril</th>
            <th class="cabMes">Meta<br>Maio</th>
            <th class="cabMes">Meta<br>Junho</th>
            <th class="cabMes">Meta<br>Junho</th>
            <th class="cabMes">Meta<br>Agosto</th>
            <th class="cabMes">Meta<br>Setembro</th>
            <th class="cabMes">Meta<br>Outubro</th>
            <th class="cabMes">Meta<br>Novembro</th>
            <th class="cabMes">Meta<br>Dezembro</th>
        </tr>
    </thead>
    <tbody>
            @foreach ($pacoteAnaliticas as $a)
            <tr>
                <td>{{$a->ID}}</td>
                <td >{{$a->DESCRI}}</td>
                <td >
                    {{$a->VAL1}}
                </td>
                <td >
                    {{$a->VAL2}}
                </td>
                <td >
                    {{$a->VAL3}}
                </td>
                <td >
                    {{$a->VAL4}}
                </td>
                <td >
                    {{$a->VAL5}}
                </td>
                <td>
                    {{$a->VAL6}}
                </td>
                <td >
                    {{$a->VAL7}}
                </td>
                <td >
                    {{$a->VAL8}}
                </td>
                <td>
                    {{$a->VAL9}}
                </td>
                <td >
                    {{$a->VAL10}}
                </td>
                <td >
                    {{$a->VAL11}}
                </td>
                <td >
                    {{$a->VAL12}}
                </td>
            </tr>
            @endforeach
    </tbody>
</table>
