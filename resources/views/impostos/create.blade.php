@extends('master',['itemMenu'=>$itemMenu])

@section('page_header')

@php
$menuTitulo = Config::get('menu.menuTitulo');
$menuIcon = Config::get('menu.menuIcon');
$menuUrl = Config::get('menu.menuUrl');
@endphp
    <h1 class="page-title">
    <i class="{{ $menuIcon[$itemMenu] }}"></i> {{ $menuTitulo[$itemMenu] }}
    </h1>
@stop

@section('content')
<style>
.row>[class*=col-] {
    margin-bottom: 5px;
}
</style>
<div class="page-content container-center">

        <div class="alerts"></div>
            <div class="row">
                <div class="col-md-6">
                    <div class="panel panel-bordered">
                        <form name="frmAdd" method="POST" action="{{route('impostos.store')}}">
                        {{ csrf_field() }}
                        <input id="ano" type="hidden" name="ano" value="{{$ano}}">
                        <div class="panel-body">
                                    <div class="form-group  ">
                                            <label for="name">FILIAL</label>
                                            <select class="form-control select2" name="filial">
                                                @foreach ($listaRodcga as $lr)
                                                <option value="{{$lr->CODCGA}}">{{$lr->CODCGA}} - {{$lr->NOMEFILIAL}}</option>
                                                @endforeach
                                            </select>
                                    </div>

                                    <div class="form-group" >
                                        <label for="name">ICMS</label>
                                        <input class="form-control" type="text" name="icms" />
                                    </div>

                                    <div class="form-group" >
                                        <label for="name">CONFIS</label>
                                        <input class="form-control" type="text" name="confis" />
                                    </div>

                                    <div class="form-group" >
                                        <label for="name">INDICEISS</label>
                                        <input class="form-control" type="text" name="indiceiss" />
                                    </div>

                                    <div class="form-group" >
                                        <label for="name">PIS</label>
                                        <input class="form-control" type="text" name="pis" />
                                    </div>

                                    <div class="form-group" >
                                        <label for="name">CONTRIBUICAO_SERV</label>
                                        <input class="form-control" type="text" name="contribuicao_serv" />
                                    </div>

                                    <div class="form-group" >
                                        <label for="name">CONTRIBUICAO_TRANS</label>
                                        <input class="form-control" type="text" name="contribuicao_trans" />
                                    </div>

                                    <div class="form-group" >
                                        <label for="name">IRPJ_SERV</label>
                                        <input class="form-control" type="text" name="irpj_serv" />
                                    </div>

                                    <div class="form-group" >
                                        <label for="name">IRPJ_TRANS</label>
                                        <input class="form-control" type="text" name="irpj_trans" />
                                    </div>

                                    <div class="form-group" >
                                        <label for="name">CPRB</label>
                                        <input class="form-control" type="text" name="cprb" />
                                    </div>

                                    <div class="form-group" >
                                        <label for="name">DESPCORP</label>
                                        <input class="form-control" type="text" name="despcorp" />
                                    </div>
                        </div>
                        <div class="panel-footer">
                                <button type="submit" class="btn btn-primary save">Salvar</button>
                        </div>
                       </form>
                    </div>
                </div>
            </div>
</div>
@endsection
@section('javascript')
<script>
        $(document).ready( function () {
          });
</script>
@endsection
