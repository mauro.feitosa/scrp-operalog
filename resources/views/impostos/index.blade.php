@extends('master',['itemMenu'=>$itemMenu])

@section('page_header')

@php
$menuTitulo = Config::get('menu.menuTitulo');
$menuIcon = Config::get('menu.menuIcon');
$menuUrl = Config::get('menu.menuUrl');
@endphp
    <h1 class="page-title">
    <i class="{{ $menuIcon[$itemMenu] }}"></i> {{ $menuTitulo[$itemMenu] }}
    <a href="{{route('impostos.create')}}" class="btn btn-success btn-add-new">
        <i class="admin-plus"></i> <span>Adicionar</span>
    </a>
    </h1>
@stop

@section('content')
<style>
.row>[class*=col-] {
    margin-bottom: 5px;
}
.table {
        font-size:10px !important;
    }
.table>thead>tr>th {
    font-weight: bolder;
    color: #526069;
}
.btn {
    padding: 4px 6px;
    font-size: 11px;
    margin-top: 0px;
    margin-bottom: 0px;
}
</style>
<div class="page-content container-fluid">

        <div class="alerts"></div>
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-bordered">
                        <div class="panel-body">
                        <table class="table table-bordered" id="laravel_datatable">
                                <thead>
                                   <tr>
                                      <th>ID</th>
                                      <th>NOMEFILIAL</th>
                                      <th>ANO</th>
                                      <th>ICMS</th>
                                      <th>CONFIS</th>
                                      <th>INDICEISS</th>
                                      <th>PIS</th>
                                      <th>CONTRIBUICAO_SERV</th>
                                      <th>CONTRIBUICAO_TRANS</th>
                                      <th>IRPJ_SERV</th>
                                      <th>IRPJ_TRANS</th>
                                      <th>CPRB</th>
                                      <th>DESPCORP</th>
                                      <th width="100px">AÇÃO</th>
                                   </tr>
                                </thead>
                        </table>
                        </div>
                    </div>
                </div>
            </div>
</div>
@endsection
@section('javascript')
<script>
        $(document).ready( function () {
         $('#laravel_datatable').DataTable({
                processing: true,
                serverSide: true,
                order: [[ 1, "asc" ]],
                ajax: "{{ url('impostos-list') }}",
                columns: [
                         { data: 'ID', name: 'ID'},
                         { data: 'NOMEFILIAL', name: 'NOMEFILIAL' },
                         { data: 'ANO', name: 'ANO' },
                         { data: 'ICMS', name: 'ICMS' },
                         { data: 'CONFIS', name: 'CONFIS' },
                         { data: 'INDICEISS', name: 'INDICEISS' },
                         { data: 'PIS', name: 'PIS' },
                         { data: 'CONTRIBUICAO_SERV', name: 'CONTRIBUICAO_SERV' },
                         { data: 'CONTRIBUICAO_TRANS', name: 'CONTRIBUICAO_TRANS' },
                         { data: 'IRPJ_SERV', name: 'IRPJ_SERV' },
                         { data: 'IRPJ_TRANS', name: 'IRPJ_TRANS' },
                         { data: 'CPRB', name: 'CPRB' },
                         { data: 'DESPCORP', name: 'DESPCORP' },
                         { data: 'action', name: 'action', orderable: false, searchable: false}
                      ],
                      language:{
                        "sEmptyTable": "Nenhum registro encontrado",
                        "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
                        "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
                        "sInfoFiltered": "(Filtrados de _MAX_ registros)",
                        "sInfoPostFix": "",
                        "sInfoThousands": ".",
                        "sLengthMenu": "_MENU_ resultados por página",
                        "sLoadingRecords": "Carregando...",
                        "sProcessing": "Processando...",
                        "sZeroRecords": "Nenhum registro encontrado",
                        "sSearch": "Pesquisar",
                        "oPaginate": {
                            "sNext": "Próximo",
                            "sPrevious": "Anterior",
                            "sFirst": "Primeiro",
                            "sLast": "Último"
                        },
                        "oAria": {
                            "sSortAscending": ": Ordenar colunas de forma ascendente",
                            "sSortDescending": ": Ordenar colunas de forma descendente"
                        }
                    }

             });
          });
</script>
@endsection
