@extends('master',['itemMenu'=>$itemMenu])
@php
$menuTitulo = Config::get('menu.menuTitulo');
$menuIcon = Config::get('menu.menuIcon');
$menuUrl = Config::get('menu.menuUrl');
@endphp
@section('page_header')
    <h1 class="page-title">
            <i class="admin-lock"></i> ACESSO NEGADO
    </h1>
@stop

@section('content')
<style>
.row>[class*=col-] {
    margin-bottom: 5px;
}
</style>
<div class="page-content container-fluid">

        <div class="alerts"></div>
            <div class="row">
                <div class="col-md-12">
                    ACESSO NEGADO : USUÁRIO NÃO AUTORIZADO!
                </div>
            </div>
</div>
@endsection
