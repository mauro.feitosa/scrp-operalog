@extends('master',['itemMenu'=>$itemMenu])

@section('page_header')
    <!--h1 class="page-title">
    <i class="{{ $pageIcon }}"></i> {{ $pageTitulo }}
    </h1-->
@stop

@section('content')
<style>
.row>[class*=col-] {
    margin-bottom: 5px;
}

.nav-tabs>li.active>a, .nav-tabs>li.active>a:focus, .nav-tabs>li.active>a:hover {
    color: #fff;
    background:  #FFBF00!important;
    font-weight: bold;
}
@media (max-width: 400px) {  

}
.table {
        font-size:10px !important;
    }
#parent {
    min-height: height: calc(100vh - 114px);
    height: calc(100vh - 114px);
			}
			
#fixTable {
    width: 2000px !important;
}
.cabMes { 
    width:120px!important;
    text-transform: uppercase;
    border-color:#fff;
    color:#fff!important;
    background-color:#000080!important;
    text-align: center;
    height:46px!important;
    font-weight: bold!important;
    vertical-align: middle!important;
} 
.cabClass { 
    width:260px!important;
    text-transform: uppercase;
    border-color:#fff;
    color:#fff!important;
    background-color:#000080!important;
    text-align: left;
    height:46px!important;
    font-weight: bold!important;
    vertical-align: middle!important;
} 
.cabNum { 
    width:40px!important;
    text-transform: uppercase;
    border-color:#fff;
    color:#fff!important;
    background-color:#000080!important;
    text-align: left;
    height:46px!important;
    font-weight: bold!important;
    vertical-align: middle!important;
} 
.cabAltura{
    height: 310px!important);
}
.cabValor {
    height:41px!important; 
    vertical-align: middle!important;
}
.cabSintetica{background-color:#B8CCE4;font-weight: bold!important}
.currency{width:100px;}


#parent::-webkit-scrollbar-track
{
    -webkit-box-shadow: inset 0 0 8px rgba(0,0,0,0.3);
    border-radius: 10px;
    background-color: #F1F1F1;
}

#parent::-webkit-scrollbar
{
    width: 8px;
    height: 8px;
    background-color: #F1F1F1;
}

#parent::-webkit-scrollbar-thumb
{
    border-radius: 10px;
    -webkit-box-shadow: inset 0 0 8px rgba(0,0,0,.3);
    background-color: #C1C1C1;
}

.panel-bordered>.panel-body {
    padding: 10px 10px 10px;
    overflow: hidden;
}
body {
    overflow: hidden;
}
/*body::-webkit-scrollbar-track
{
    -webkit-box-shadow: inset 0 0 8px rgba(0,0,0,0.3);
    border-radius: 10px;
    background-color: #F5F5F5;
}
body::-webkit-scrollbar
{
    width: 8px;
    height: 8px;
    background-color: #F5F5F5;
}

body::-webkit-scrollbar-thumb
{
    border-radius: 10px;
    -webkit-box-shadow: inset 0 0 8px rgba(0,0,0,.3);
    background-color: #555;
}*/
</style>
<div id="#admin" class="page-content container-fluid">
        <div class="alerts"></div>
            <div class="row">
                <div class="col-md-12">
                    <div  class="panel panel-bordered">

                        <div class="panel-body">
                            <div id="parent" >
                                <table id="fixTable" class="table table-bordered">
                                    <thead> 
                                        <tr> 
                                            <th class="cabNum">#</th> 
                                            <th class="cabClass">Classificação Sintetica / Analitica</th> 
                                            <th class="cabMes">Meta<br>Janeiro</th> 
                                            <th class="cabMes">Meta<br>Fevereiro</th>
                                            <th class="cabMes">Meta<br>Março</th>
                                            <th class="cabMes">Meta<br>Abril</th>
                                            <th class="cabMes">Meta<br>Maio</th>
                                            <th class="cabMes">Meta<br>Junho</th>
                                            <th class="cabMes">Meta<br>Junho</th>
                                            <th class="cabMes">Meta<br>Agosto</th>
                                            <th class="cabMes">Meta<br>Setembro</th>
                                            <th class="cabMes">Meta<br>Outubro</th>
                                            <th class="cabMes">Meta<br>Novembro</th>
                                            <th class="cabMes">Meta<br>Dezembro</th>
                                            <th class="">Justificativa</th>
                                        </tr> 
                                    </thead> 
                                    <tbody>
                                        @foreach ($pacoteSinteticas as $s)
                                        <tr class="cabSintetica">
                                            <td scope="row"></td> 
                                            <td>{{$s->DESCRI}}</td>
                                            <td>R$ <span class="total_jan{{$s->CODCLAP}}">0,00</span></td>
                                            <td>R$ <span class="total_fev{{$s->CODCLAP}}">0,00</span></td>
                                            <td>R$ <span class="total_mar{{$s->CODCLAP}}">0,00</span></td>
                                            <td>R$ <span class="total_abr{{$s->CODCLAP}}">0,00</span></td>
                                            <td>R$ <span class="total_mai{{$s->CODCLAP}}">0,00</span></td>
                                            <td>R$ <span class="total_jun{{$s->CODCLAP}}">0,00</span></td>       
                                            <td>R$ <span class="total_jul{{$s->CODCLAP}}">0,00</span></td> 
                                            <td>R$ <span class="total_ago{{$s->CODCLAP}}">0,00</span></td> 
                                            <td>R$ <span class="total_set{{$s->CODCLAP}}">0,00</span></td> 
                                            <td>R$ <span class="total_out{{$s->CODCLAP}}">0,00</span></td> 
                                            <td>R$ <span class="total_nov{{$s->CODCLAP}}">0,00</span></td> 
                                            <td>R$ <span class="total_dez{{$s->CODCLAP}}">0,00</span></td> 
                                            <td></td>                                      
                                        </tr>
                                            @foreach ($pacoteAnaliticas as $a)
                                                @if($s->CODCLAP == $a->PAICLAP)
                                                <tr> 
                                                <td class="cabValor" scope="row">{{$a->CODCLAP}}</td> 
                                                <td class="cabValor">{{$a->DESCRI}}</td>                                                  
                                                <td class="cabValor">
                                                    <input type="text" value="0,00" class="currency sum_{{$a->PAICLAP}}" name="jan_{{$a->CODCLAP}}">
                                                </td> 
                                                <td class="cabValor">
                                                    <input type="text" value="0,00" class="currency sum_{{$a->PAICLAP}}" name="fev_{{$a->CODCLAP}}">
                                                </td>
                                                <td class="cabValor">
                                                    <input type="text" value="0,00" class="currency sum_{{$a->PAICLAP}}" name="mar_{{$a->CODCLAP}}">
                                                </td>
                                                <td class="cabValor">
                                                    <input type="text" value="0,00" class="currency sum_{{$a->PAICLAP}}" name="abr_{{$a->CODCLAP}}">
                                                </td>
                                                <td class="cabValor">
                                                    <input type="text" value="0,00" class="currency sum_{{$a->PAICLAP}}" name="mai_{{$a->CODCLAP}}">
                                                </td>
                                                <td>
                                                    <input type="text" value="0,00" class="currency sum_{{$a->PAICLAP}}" name="jun_{{$a->CODCLAP}}">              
                                                </td>       
                                                <td class="cabValor">
                                                    <input type="text" value="0,00" class="currency sum_{{$a->PAICLAP}}" name="jul_{{$a->CODCLAP}}">
                                                </td> 
                                                <td class="cabValor">
                                                    <input type="text" value="0,00" class="currency sum_{{$a->PAICLAP}}" name="ago_{{$a->CODCLAP}}">
                                                </td> 
                                                <td>
                                                    <input type="text" value="0,00" class="currency sum_{{$a->PAICLAP}}" name="set_{{$a->CODCLAP}}">
                                                </td> 
                                                <td class="cabValor">
                                                    <input type="text" value="0,00" class="currency sum_{{$a->PAICLAP}}" name="out_{{$a->CODCLAP}}">
                                                </td> 
                                                <td class="cabValor">
                                                    <input type="text" value="0,00" class="currency sum_{{$a->PAICLAP}}" name="nov_{{$a->CODCLAP}}">
                                                </td> 
                                                <td class="cabValor">
                                                    <input type="text" value="0,00" class="currency sum_{{$a->PAICLAP}}" name="dez_{{$a->CODCLAP}}">
                                                </td> 
                                                <td class="cabValor">
                                                    <input type="text" name="just_{{$a->CODCLAP}}">
                                                </td>
                                                </tr>
                                                @endif                                             
                                            @endforeach
                                        @endforeach 
                                    </tbody> 
                                </table>
                                </div>                                  
                        </div>    
                    </div>    
                </div>
            </div>                      
</div>
@endsection
@section('javascript')
<script>

			$(document).ready(function() {
				$("#fixTable").tableHeadFixer({"left" : 2}); 
                //var Scrollbar = window.Scrollbar;

//Scrollbar.init(document.querySelector('#parent'));
			});


        $(function() {
          $(".currency").maskMoney({prefix:'R$ ', allowZero: true, allowNegative: true, thousands:'.', decimal:',', affixesStay: false});

        });

        Number.prototype.format = function(n, x, s, c) {
            var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\D' : '$') + ')',
                num = this.toFixed(Math.max(0, ~~n));

            return (c ? num.replace('.', c) : num).replace(new RegExp(re, 'g'), '$&' + (s || ','));
        };

        @foreach ($pacoteSinteticas as $s)
        $(document).on("change", ".sum_{{$s->CODCLAP}}", function() {
        //    console.log('oi');
        var sum = 0;
        var tmp = 0.00;
            $(".sum_{{$s->CODCLAP}}").each(function(){
                //console.log($(this).val());
        var str = $(this).val(); 
        sum += parseFloat(str.replace('.','').replace(',','.').replace(' ',''));
        tmp = sum;
        console.log(sum);
        //tmp = ss.toString();   
        console.log(tmp.format(2, 3, '.', ','));
        ///sum += +$(this).val();
            //$(".total_{{$s->CODCLAP}}").maskMoney('mask',tmp);
            $(".total_jan{{$s->CODCLAP}}").html(tmp.format(2, 3, '.', ','));
        });
        //$(".total_{{$s->CODCLAP}}").maskMoney('mask', sum);
        //$(".total_{{$s->CODCLAP}}").maskMoney('mask',tmp.replace(',','.').replace(' ',''));
        //$(".total_txt{{$s->CODCLAP}}").text($(".total_{{$s->CODCLAP}}").val());
        //$(".total_{{$s->CODCLAP}}").html(sum);
        //console.log(sum);
        });
        @endforeach

</script>  
@endsection
