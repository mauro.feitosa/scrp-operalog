@php
$menuTitulo = Config::get('menu.menuTitulo');
$menuIcon = Config::get('menu.menuIcon');
$menuUrl = Config::get('menu.menuUrl');

//dd($menuTitulo,$menuIcon,$menuUrl);
@endphp
@extends('master',['itemMenu'=>$itemMenu])

@section('page_header')

@stop

@section('content')
<style>
.row>[class*=col-] {
    margin-bottom: 5px;
}

.nav-tabs>li.active>a, .nav-tabs>li.active>a:focus, .nav-tabs>li.active>a:hover {
    color: #fff;
    background:  #FFBF00!important;
    font-weight: bold;
}
@media (max-width: 400px) {

}
.table {
        font-size:10px !important;
    }
#parent {
    min-height: height: calc(100vh - 140px);
    height: calc(100vh - 140px);
			}

#fixTable {
    width: 1800px !important;
}
.cabMes {
    width:100px;text-transform: uppercase;
    border-color:#fff;
    color:#fff!important;
    background-color:#000080!important;
    text-align: center;
    height:46px!important;
    font-weight: bold!important;
    vertical-align: middle!important;
}
.cabClass {
    width:400px!important;
    text-transform: uppercase;
    border-color:#fff;
    color:#fff!important;
    background-color:#000080!important;
    text-align: left;
    height:46px!important;
    font-weight: bold!important;
    vertical-align: middle!important;
}
.cabNum {
    width:50px;text-transform: uppercase;
    border-color:#fff;
    color:#fff!important;
    background-color:#000080!important;
    text-align: left;
    height:46px!important;
    font-weight: bold!important;
    vertical-align: middle!important;
}
.cabAltura{
    height: 310px!important);
}
.cabValor {
    height:41px!important;
    vertical-align: middle!important;
}
.cabValorTit {
    height:41px!important;
    vertical-align: middle!important;
    font-weight: bold!important;
    text-transform: uppercase!important;
}
.cabSintetica{background-color:#B8CCE4;font-weight: bold!important}
.tituloTrecho{background-color:#F5F5F5!important;font-weight: bold!important}
.currency{width:100px;}
.currencyVal{width:100px;}

#parent::-webkit-scrollbar-track
{
    -webkit-box-shadow: inset 0 0 8px rgba(0,0,0,0.3);
    border-radius: 10px;
    background-color: #F1F1F1;
}

#parent::-webkit-scrollbar
{
    width: 8px;
    height: 8px;
    background-color: #F1F1F1;
}

#parent::-webkit-scrollbar-thumb
{
    border-radius: 10px;
    -webkit-box-shadow: inset 0 0 8px rgba(0,0,0,.3);
    background-color: #C1C1C1;
}

.panel-bordered>.panel-body {
    padding: 10px 10px 10px;
    overflow: hidden;
}
body {
    overflow: hidden;
}
.badge:hover {
  color: #ffffff;
  text-decoration: none;
  cursor: pointer;
}
.badge-error {
  background-color: #b94a48;
}
.badge-error:hover {
  background-color: #953b39;
}
.badge-warning {
  background-color: #f89406;
}
.badge-warning:hover {
  background-color: #c67605;
}
.badge-success {
  background-color: #468847;
}
.badge-success:hover {
  background-color: #356635;
}
.badge-info {
  background-color: #3a87ad;
}
.badge-info:hover {
  background-color: #2d6987;
}
.badge-inverse {
  background-color: #333333;
}
.badge-inverse:hover {
  background-color: #1a1a1a;
}
.btn {
    padding: 4px 6px;
    font-size: 12px;
    margin-top: 0px;
    margin-bottom: 0px;
    font-weight: bolder;
}
.formZero{
    margin:0px;
    padding:0px;
}
.select2-selection__rendered {
  font-family: Arial, Helvetica, sans-serif;
  font-size: 10px!important;
  font-weight: bold;
}
.select2-results__options{
        font-size:10px!important;
        font-weight: bold;
 }

input {font-weight:bold;}

input[type="text"]:read-only:not([read-only="false"]) { color: blue; background-color: #eee; border-width: 1px!important;border-style: solid!important; border-color: #A9A9A9!important;}

</style>
<div id="#admin"class="page-content container-fluid">
        <div class="alerts"></div>
            <div class="row">
                <div class="col-md-12">

                <form id="frmfechar" name="frmfechar" method="POST" action="{{route('previsaoorcamentaria/simuladorepi')}}"class="formZero">
                    {{ csrf_field() }}
                    <input type="hidden" name="CODCGA" value="{{$CODCGA}}"/>
                    <input type="hidden" name="CODUNN" value="{{$CODUNN}}"/>
                    <input type="hidden" name="codgru" value="{{$codgru}}"/>
                    <input type="hidden" name="mesepi" value="{{$mesepi}}"/>
                </form>

                    <div  class="panel panel-bordered">
                    <div style="margin-left:14px;margin-top:8px;font-size:14px;display:table;width:100%">
                        <span class="badge badge-error">{{$CODCGA}}</span>
                        <span type="submit"class="badge badge-info">{{$centrodegasto}}</span> /
                        <span class="badge badge-error">{{$CODUNN}}</span><span class="badge badge-info">{{$unidade}}</span> /
                        <span class="badge badge-warning">SIMULADOR EPI</span>
                    </div>

                        <div class="panel-body">
                            <div id="parent" >
                                    <div class="alerts"></div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="panel panel-bordered">
                                            <form name="frmSalvar" method="POST" action="{{route('previsaoorcamentaria/simuladorepi')}}"class="formZero">
                                                <input type="hidden" value="ok" name="salvar" />
                                                {{ csrf_field() }}
                                                <input type="hidden" name="CODCGA" value="{{$CODCGA}}"/>
                                                <input type="hidden" name="CODUNN" value="{{$CODUNN}}"/>
                                                <input type="hidden" name="codgru" value="{{$codgru}}"/>
                                                <input type="hidden" name="mesepi" value="{{$mesepi}}"/>
                                                <div class="panel-body">
                                                    <div class="form-group  ">
                                                        <label for="name">Classificação</label>
                                                        <select id="ANALIT" class="form-control select2" name="ANALIT">
                                                            <option value="" disabled  selected>SELECIONE UMA CLASSIFICAÇÃO...</option>
                                                            @foreach ($listaAnalit as $la)
                                                            <option value="{{$la->ANALIT}}">{{str_pad($la->ANALIT , 3 , '0' , STR_PAD_LEFT)}} - {{$la->ANALITICA}}</option>;
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    <div class="form-group  ">
                                                        <label for="name">Produto</label>
                                                        <select id="EPI_SMLD_PROD_ID" class="form-control select2" name="EPI_SMLD_PROD_ID">
                                                        </select>
                                                    </div>
                                                    <div class="form-group  ">
                                                        <label for="name">Cargo</label>
                                                        <select id="EPI_SMLD_CARGO_ID" class="form-control select2" name="EPI_SMLD_CARGO_ID">
                                                        </select>
                                                    </div>
                                                    <div class="form-group  ">
                                                        <label for="name">Centro de Custo</label>
                                                        <select id="CODCUS" class="form-control select2" name="CODCUS">
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="panel-footer">
                                                        <button id="btnsalvar" type="submit"  class="btn btn-primary save"> SALVAR </button>
                                                        <button style="margin-left:10px;" onclick="cancelarAdd()" type="button" id="fecharRateio"class="btn btn-danger" > CANCELAR </button>
                                                </div>
                                               </form>
                                            </div>
                                        </div>
                                    </div>
                            </div>
                    </div>
                    </form>
                </div>

                <!--form name="addNovoTrecho">
                        <button type="submit"class="btn btn-warning" > <i class="admin-check-circle"></i> - REMOVER TRECHO </button>
                </form-->
            </div>
</div>
@endsection
@section('javascript')
<script>

$(document).ready(function() {
                $("#fixTable").tableHeadFixer({"left" : 2});
                $('#btnsalvar').prop('disabled', true);
                $('#EPI_SMLD_PROD_ID').prop('disabled', true);
                $('#EPI_SMLD_CARGO_ID').prop('disabled', true);
                $('#CODCUS').prop('disabled', true);
     //           $("#ANALIT").select2({
     // placeholder: 'Selecione a Classificação'
   // });
                ///$("#selMes").select2({ width: 'resolve' });

                $(window).keydown(function(event){
                    if(event.keyCode == 13) {
                    event.preventDefault();
                    return false;
                    }
                });

            });


    $("#ANALIT").on("change", function () {
        $('#btnsalvar').prop('disabled', false);
        $('#EPI_SMLD_PROD_ID').prop('disabled', false);
        $('#EPI_SMLD_CARGO_ID').prop('disabled', false);
        $('#CODCUS').prop('disabled', false);
        var x = $("#ANALIT").val();
        console.log(x);
      $("#EPI_SMLD_PROD_ID option[value]").remove();
      $("#EPI_SMLD_CARGO_ID option[value]").remove();
      $("#CODCUS option[value]").remove();

      $.ajax({
              type: 'POST',
              url: "{{route('previsaoorcamentaria/simuladorepi')}}",
              data: {'id':x,'ajax':'p1'},
              dataType: "json",
              success: function(data) {
                  console.log(data);

                //$('body').append(data);
                //alert(data);
                $("#EPI_SMLD_PROD_ID").select2({
                data: data
                });
              }
        });

        $.ajax({
              type: 'POST',
              url: "{{route('previsaoorcamentaria/simuladorepi')}}",
              data: {'id':x,'ajax':'p2'},
              dataType: "json",
              success: function(data) {
                  console.log(data);

                //$('body').append(data);
                //alert(data);
                $("#EPI_SMLD_CARGO_ID").select2({
                data: data
                });
              }
        });

        $.ajax({
              type: 'POST',
              url: "{{route('previsaoorcamentaria/simuladorepi')}}",
              data: {'id':x,'ajax':'p3'},
              dataType: "json",
              success: function(data) {
                  console.log(data);

                //$('body').append(data);
                //alert(data);
                $("#CODCUS").select2({
                data: data
                });
              }
        });

      //$("#second").append(rangeOptions).val("").trigger("change");
    });


    cancelarAdd = function(){
            $('form#frmfechar').submit();
          }

        $(function() {
          $(".currency").maskMoney({prefix:'', allowZero: true, allowNegative: true, thousands:'.', decimal:',', affixesStay: false});
        });

        Number.prototype.format = function(n, x, s, c) {
            var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\D' : '$') + ')',
                num = this.toFixed(Math.max(0, ~~n));

            return (c ? num.replace('.', c) : num).replace(new RegExp(re, 'g'), '$&' + (s || ','));
        };
</script>
@endsection
