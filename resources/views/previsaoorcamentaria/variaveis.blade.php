@php
$menuTitulo = Config::get('menu.menuTitulo');
$menuIcon = Config::get('menu.menuIcon');
$menuUrl = Config::get('menu.menuUrl');
@endphp

@extends('master',['itemMenu'=>$itemMenu])

@section('page_header')

@stop

@section('content')
<style>
.row>[class*=col-] {
    margin-bottom: 5px;
}

.nav-tabs>li.active>a, .nav-tabs>li.active>a:focus, .nav-tabs>li.active>a:hover {
    color: #fff;
    background:  #FFBF00!important;
    font-weight: bold;
}
@media (max-width: 400px) {

}
.table {
        font-size:10px !important;
    }
#parent {
    min-height: height: calc(100vh - 130px);
    height: calc(100vh - 130px);
			}

#fixTable {
    width: 1800px !important;
}
.cabMes {
    width:100px;text-transform: uppercase;
    border-color:#fff;
    color:#fff!important;
    background-color:#000080!important;
    text-align: center;
    height:46px!important;
    font-weight: bold!important;
    vertical-align: middle!important;
}
.cabClass {
    width:400px!important;
    text-transform: uppercase;
    border-color:#fff;
    color:#fff!important;
    background-color:#000080!important;
    text-align: left;
    height:46px!important;
    font-weight: bold!important;
    vertical-align: middle!important;
}
.cabNum {
    width:50px;text-transform: uppercase;
    border-color:#fff;
    color:#fff!important;
    background-color:#000080!important;
    text-align: left;
    height:46px!important;
    font-weight: bold!important;
    vertical-align: middle!important;
}
.cabAltura{
    height: 310px!important);
}
.cabValor {
    height:41px!important;
    vertical-align: middle!important;
}
.cabValorTit {
    height:41px!important;
    vertical-align: middle!important;
    font-weight: bold!important;
    text-transform: uppercase!important;
}
.cabSintetica{background-color:#B8CCE4;font-weight: bold!important}
.currency{width:100px;}
.currencyVal{width:100px;}

#parent::-webkit-scrollbar-track
{
    -webkit-box-shadow: inset 0 0 8px rgba(0,0,0,0.3);
    border-radius: 10px;
    background-color: #F1F1F1;
}

#parent::-webkit-scrollbar
{
    width: 8px;
    height: 8px;
    background-color: #F1F1F1;
}

#parent::-webkit-scrollbar-thumb
{
    border-radius: 10px;
    -webkit-box-shadow: inset 0 0 8px rgba(0,0,0,.3);
    background-color: #C1C1C1;
}

.panel-bordered>.panel-body {
    padding: 10px 10px 10px;
    overflow: hidden;
}
body {
    overflow: hidden;
}
.badge:hover {
  color: #ffffff;
  text-decoration: none;
  cursor: pointer;
}
.badge-error {
  background-color: #b94a48;
}
.badge-error:hover {
  background-color: #953b39;
}
.badge-warning {
  background-color: #f89406;
}
.badge-warning:hover {
  background-color: #c67605;
}
.badge-success {
  background-color: #468847;
}
.badge-success:hover {
  background-color: #356635;
}
.badge-info {
  background-color: #3a87ad;
}
.badge-info:hover {
  background-color: #2d6987;
}
.badge-inverse {
  background-color: #333333;
}
.badge-inverse:hover {
  background-color: #1a1a1a;
}
.btn {
    padding: 4px 6px;
    font-size: 12px;
    margin-top: 0px;
    margin-bottom: 0px;
    font-weight: bolder;
}
.formZero{
    margin:0px;
    padding:0px;
}
input {font-weight:bold;}

input[type="text"]:read-only:not([read-only="false"]) { color: blue; background-color: #eee; border-width: 1px!important;border-style: solid!important; border-color: #A9A9A9!important;}
/*body::-webkit-scrollbar-track
{
    -webkit-box-shadow: inset 0 0 8px rgba(0,0,0,0.3);
    border-radius: 10px;
    background-color: #F5F5F5;
}
body::-webkit-scrollbar
{
    width: 8px;
    height: 8px;
    background-color: #F5F5F5;
}

body::-webkit-scrollbar-thumb
{
    border-radius: 10px;
    -webkit-box-shadow: inset 0 0 8px rgba(0,0,0,.3);
    background-color: #555;
}*/
</style>
<div id="#admin" class="page-content container-fluid">
        <div class="alerts"></div>
            <div class="row">
                <div class="col-md-12">
                <form name="frm" method="POST" action="{{route('previsaoorcamentaria/variaveis')}}" class="formZero">
                    <input type="hidden" value="ok" name="salvar" />
                    {{ csrf_field() }}
                    <div  class="panel panel-bordered">
                    <div style="margin-left:14px;margin-top:8px;font-size:14px;display:table;width:100%">
                        <span class="badge badge-error">{{$CODCGA}}</span>
                        <span type="submit" class="badge badge-info">{{$centrodegasto}}</span> /
                        <span class="badge badge-error">{{$CODUNN}}</span><span class="badge badge-info">{{$unidade}}</span> /
                        <span class="badge badge-warning">{{$pacote}}</span> /
                        <span class="badge badge-warning">VARIAVEIS</span>
                        @if(isset($subitem))
                        / <span class="badge badge-warning">{{$subitem}}</span>
                        @endif
                        <div class="pull-right" style="margin-right:30px;width:300px;">

                            <input type="hidden" name="CODCGA" value="{{$CODCGA}}"/>
                            <input type="hidden" name="CODUNN" value="{{$CODUNN}}"/>
                            <input type="hidden" name="codgru" value="{{$codgru}}"/>

                                <button type="submit" class="btn btn-success pull-right" > <i class="admin-check-circle"></i>  SALVAR DADOS </button>

                        </div>
                    </div>
                        <div class="panel-body">
                            <div id="parent" >
                                <table id="fixTable" class="table table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th class="cabNum">#</th>
                                            <th class="cabClass">Itens</th>
                                            <th class="cabNum"></th>
                                            <th class="cabMes">Meta<br>Janeiro</th>
                                            <th class="cabMes">Meta<br>Fevereiro</th>
                                            <th class="cabMes">Meta<br>Março</th>
                                            <th class="cabMes">Meta<br>Abril</th>
                                            <th class="cabMes">Meta<br>Maio</th>
                                            <th class="cabMes">Meta<br>Junho</th>
                                            <th class="cabMes">Meta<br>Julho</th>
                                            <th class="cabMes">Meta<br>Agosto</th>
                                            <th class="cabMes">Meta<br>Setembro</th>
                                            <th class="cabMes">Meta<br>Outubro</th>
                                            <th class="cabMes">Meta<br>Novembro</th>
                                            <th class="cabMes">Meta<br>Dezembro</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($pacoteSinteticas as $s)
                                        <tr class="cabSintetica">
                                            <td scope="row"></td>
                                            <td>{{$s->DESCRI}}</td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                            @foreach ($pacoteAnaliticas as $a)
                                                @if($s->CODUNN == $a->CODUNN)
                                                <tr>
                                                <td class="cabValor" scope="row">{{$a->ID}}</td>
                                                <td class="cabValor">{{$a->DESCRI}}</td>
                                                <td class="cabValor">
                                                    <a onclick="replicar('_{{$a->ID}}_{{$a->CODUNN}}','B')" href="#">REPLICAR<i class="admin-double-right"></i></a>
                                                </td>
                                                <td class="cabValor">
                                                    @if($a->TIPO !== 'B')
                                                    <input type="text" value="{{str_replace('.',',',$a->VAL1)}}" class="currency sum_jan{{$a->ID}}" name="jan_{{$a->ID}}_{{$a->CODUNN}}">
                                                    @else
                                                    <select name="jan_{{$a->ID}}_{{$a->CODUNN}}">
                                                        <option value="1" @if($a->VAL1 == 1) selected="selected"  @endif>SIM</option>
                                                        <option value="2" @if($a->VAL1 == 2) selected="selected"  @endif>NÃO</option>
                                                    </select>
                                                    @endif
                                                </td>
                                                <td class="cabValor">
                                                    @if($a->TIPO !== 'B')
                                                    <input type="text" value="{{str_replace('.',',',$a->VAL2)}}" class="currency sum_fev{{$a->ID}}" name="fev_{{$a->ID}}_{{$a->CODUNN}}">
                                                    @else
                                                    <select name="jan_{{$a->ID}}_{{$a->CODUNN}}">
                                                        <option value="1" @if($a->VAL2 == 1) selected="selected"  @endif>SIM</option>
                                                        <option value="2" @if($a->VAL2 == 2) selected="selected"  @endif>NÃO</option>
                                                    </select>
                                                    @endif
                                                </td>
                                                <td class="cabValor">
                                                    @if($a->TIPO !== 'B')
                                                    <input type="text" value="{{str_replace('.',',',$a->VAL3)}}" class="currency sum_mar{{$a->ID}}" name="mar_{{$a->ID}}_{{$a->CODUNN}}">
                                                    @else
                                                    <select name="jan_{{$a->ID}}_{{$a->CODUNN}}">
                                                        <option value="1" @if($a->VAL3 == 1) selected="selected"  @endif>SIM</option>
                                                        <option value="2" @if($a->VAL3 == 2) selected="selected"  @endif>NÃO</option>
                                                    </select>
                                                    @endif
                                                </td>
                                                <td class="cabValor">
                                                    @if($a->TIPO !== 'B')
                                                    <input type="text" value="{{str_replace('.',',',$a->VAL4)}}" class="currency sum_abr{{$a->ID}}" name="abr_{{$a->ID}}_{{$a->CODUNN}}">
                                                    @else
                                                    <select name="jan_{{$a->ID}}_{{$a->CODUNN}}">
                                                        <option value="1" @if($a->VAL4 == 1) selected="selected"  @endif>SIM</option>
                                                        <option value="2" @if($a->VAL4 == 2) selected="selected"  @endif>NÃO</option>
                                                    </select>
                                                    @endif
                                                </td>
                                                <td class="cabValor">
                                                    @if($a->TIPO !== 'B')
                                                    <input type="text" value="{{str_replace('.',',',$a->VAL5)}}" class="currency sum_mai{{$a->ID}}" name="mai_{{$a->ID}}_{{$a->CODUNN}}">
                                                    @else
                                                    <select name="jan_{{$a->ID}}_{{$a->CODUNN}}">
                                                        <option value="1" @if($a->VAL5 == 1) selected="selected"  @endif>SIM</option>
                                                        <option value="2" @if($a->VAL5 == 2) selected="selected"  @endif>NÃO</option>
                                                    </select>
                                                    @endif
                                                </td>
                                                <td>
                                                    @if($a->TIPO !== 'B')
                                                    <input type="text" value="{{str_replace('.',',',$a->VAL6)}}" class="currency sum_jun{{$a->ID}}" name="jun_{{$a->ID}}_{{$a->CODUNN}}">
                                                    @else
                                                    <select name="jan_{{$a->ID}}_{{$a->CODUNN}}">
                                                        <option value="1" @if($a->VAL6 == 1) selected="selected"  @endif>SIM</option>
                                                        <option value="2" @if($a->VAL6 == 2) selected="selected"  @endif>NÃO</option>
                                                    </select>
                                                    @endif
                                                </td>
                                                <td class="cabValor">
                                                    @if($a->TIPO !== 'B')
                                                    <input type="text" value="{{str_replace('.',',',$a->VAL7)}}" class="currency sum_jul{{$a->ID}}" name="jul_{{$a->ID}}_{{$a->CODUNN}}">
                                                    @else
                                                    <select name="jan_{{$a->ID}}_{{$a->CODUNN}}">
                                                        <option value="1" @if($a->VAL7 == 1) selected="selected"  @endif>SIM</option>
                                                        <option value="2" @if($a->VAL7 == 2) selected="selected"  @endif>NÃO</option>
                                                    </select>
                                                    @endif
                                                </td>
                                                <td class="cabValor">
                                                    @if($a->TIPO !== 'B')
                                                    <input type="text" value="{{str_replace('.',',',$a->VAL8)}}" class="currency sum_ago{{$a->ID}}" name="ago_{{$a->ID}}_{{$a->CODUNN}}">
                                                    @else
                                                    <select name="jan_{{$a->ID}}_{{$a->CODUNN}}">
                                                        <option value="1" @if($a->VAL8 == 1) selected="selected"  @endif>SIM</option>
                                                        <option value="2" @if($a->VAL8 == 2) selected="selected"  @endif>NÃO</option>
                                                    </select>
                                                    @endif
                                                </td>
                                                <td>
                                                    @if($a->TIPO !== 'B')
                                                    <input type="text" value="{{str_replace('.',',',$a->VAL9)}}" class="currency sum_set{{$a->ID}}" name="set_{{$a->ID}}_{{$a->CODUNN}}">
                                                    @else
                                                    <select name="jan_{{$a->ID}}_{{$a->CODUNN}}">
                                                        <option value="1" @if($a->VAL9 == 1) selected="selected"  @endif>SIM</option>
                                                        <option value="2" @if($a->VAL9 == 2) selected="selected"  @endif>NÃO</option>
                                                    </select>
                                                    @endif
                                                </td>
                                                <td class="cabValor">
                                                    @if($a->TIPO !== 'B')
                                                    <input type="text" value="{{str_replace('.',',',$a->VAL10)}}" class="currency sum_out{{$a->ID}}" name="out_{{$a->ID}}_{{$a->CODUNN}}">
                                                    @else
                                                    <select name="jan_{{$a->ID}}_{{$a->CODUNN}}">
                                                        <option value="1" @if($a->VAL10 == 1) selected="selected"  @endif>SIM</option>
                                                        <option value="2" @if($a->VAL10 == 2) selected="selected"  @endif>NÃO</option>
                                                    </select>
                                                    @endif
                                                </td>
                                                <td class="cabValor">
                                                    @if($a->TIPO !== 'B')
                                                    <input type="text" value="{{str_replace('.',',',$a->VAL11)}}" class="currency sum_nov{{$a->ID}}" name="nov_{{$a->ID}}_{{$a->CODUNN}}">
                                                    @else
                                                    <select name="jan_{{$a->ID}}_{{$a->CODUNN}}">
                                                        <option value="1" @if($a->VAL11 == 1) selected="selected"  @endif>SIM</option>
                                                        <option value="2" @if($a->VAL11 == 2) selected="selected"  @endif>NÃO</option>
                                                    </select>
                                                    @endif
                                                </td>
                                                <td class="cabValor">
                                                    @if($a->TIPO !== 'B')
                                                    <input type="text" value="{{str_replace('.',',',$a->VAL12)}}" class="currency sum_dez{{$a->ID}}" name="dez_{{$a->ID}}_{{$a->CODUNN}}">
                                                    @else
                                                    <select name="jan_{{$a->ID}}_{{$a->CODUNN}}">
                                                        <option value="1" @if($a->VAL12 == 1) selected="selected"  @endif>SIM</option>
                                                        <option value="2" @if($a->VAL12 == 2) selected="selected"  @endif>NÃO</option>
                                                    </select>
                                                    @endif
                                                </td>
                                                </tr>
                                                @endif
                                            @endforeach
                                        @endforeach
                                    </tbody>
                                </table>
                                </div>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
</div>
@endsection
@section('javascript')
<script>

			$(document).ready(function() {
				$("#fixTable").tableHeadFixer({"left" : 2});
                //var Scrollbar = window.Scrollbar;

//Scrollbar.init(document.querySelector('#parent'));
			});

        replicar = function(item,tipo){

            valor = $( "input[name*='jan"+item+"']" ).val();

            $( "input[name*='"+item+"']" ).val(valor);
            //$( "input[name*='"+item+"']" ).val(valor);
            //$("#select_id").val("val2").change();

        }

        $(function() {
          $(".currency").maskMoney({prefix:'', allowZero: true, allowNegative: true, thousands:'.', decimal:',', affixesStay: false});
        });

        Number.prototype.format = function(n, x, s, c) {
            var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\D' : '$') + ')',
                num = this.toFixed(Math.max(0, ~~n));

            return (c ? num.replace('.', c) : num).replace(new RegExp(re, 'g'), '$&' + (s || ','));
        };


</script>
@endsection
