@php
$menuTitulo = Config::get('menu.menuTitulo');
$menuIcon = Config::get('menu.menuIcon');
$menuUrl = Config::get('menu.menuUrl');

//dd($menuTitulo,$menuIcon,$menuUrl);
@endphp
@extends('master',['itemMenu'=>$itemMenu])

@section('page_header')

@stop

@section('content')
<style>
.row>[class*=col-] {
    margin-bottom: 5px;
}

.nav-tabs>li.active>a, .nav-tabs>li.active>a:focus, .nav-tabs>li.active>a:hover {
    color: #fff;
    background:  #FFBF00!important;
    font-weight: bold;
}
@media (max-width: 400px) {

}
.table {
        font-size:10px !important;
    }
#parent {
    min-height: height: calc(100vh - 140px);
    height: calc(100vh - 140px);
			}

#fixTable {
    width: 1800px !important;
}
.cabMes {
    width:100px;text-transform: uppercase;
    border-color:#fff;
    color:#fff!important;
    background-color:#000080!important;
    text-align: center;
    height:46px!important;
    font-weight: bold!important;
    vertical-align: middle!important;
}
.cabClass {
    width:200px!important;
    text-transform: uppercase;
    border-color:#fff;
    color:#fff!important;
    background-color:#000080!important;
    text-align: left;
    height:46px!important;
    font-weight: bold!important;
    vertical-align: middle!important;
}
.cabNum {
    width:30px;text-transform: uppercase;
    border-color:#fff;
    color:#fff!important;
    background-color:#000080!important;
    text-align: left;
    height:46px!important;
    font-weight: bold!important;
    vertical-align: middle!important;
}
.cabAltura{
    height: 310px!important);
}
.cabValor {
    height:41px!important;
    vertical-align: middle!important;
}
.cabValorTit {
    height:41px!important;
    vertical-align: middle!important;
    font-weight: bold!important;
    text-transform: uppercase!important;
}
.cabSintetica{background-color:#B8CCE4;font-weight: bold!important}
.tituloTrecho{background-color:#F5F5F5!important;font-weight: bold!important}
.currency{width:100px;}
.currencyVal{width:100px;}

#parent::-webkit-scrollbar-track
{
    -webkit-box-shadow: inset 0 0 8px rgba(0,0,0,0.3);
    border-radius: 10px;
    background-color: #F1F1F1;
}

#parent::-webkit-scrollbar
{
    width: 8px;
    height: 8px;
    background-color: #F1F1F1;
}

#parent::-webkit-scrollbar-thumb
{
    border-radius: 10px;
    -webkit-box-shadow: inset 0 0 8px rgba(0,0,0,.3);
    background-color: #C1C1C1;
}

.panel-bordered>.panel-body {
    padding: 10px 10px 10px;
    overflow: hidden;
}
body {
    overflow: hidden;
}
.badge:hover {
  color: #ffffff;
  text-decoration: none;
  cursor: pointer;
}
.badge-error {
  background-color: #b94a48;
}
.badge-error:hover {
  background-color: #953b39;
}
.badge-warning {
  background-color: #f89406;
}
.badge-warning:hover {
  background-color: #c67605;
}
.badge-success {
  background-color: #468847;
}
.badge-success:hover {
  background-color: #356635;
}
.badge-info {
  background-color: #3a87ad;
}
.badge-info:hover {
  background-color: #2d6987;
}
.badge-inverse {
  background-color: #333333;
}
.badge-inverse:hover {
  background-color: #1a1a1a;
}
.btn {
    padding: 4px 6px;
    font-size: 12px;
    margin-top: 0px;
    margin-bottom: 0px;
    font-weight: bolder;
}
.formZero{
    margin:0px;
    padding:0px;
}
.select2-selection__rendered {
  font-family: Arial, Helvetica, sans-serif;
  font-size: 10px!important;
  font-weight: bold;
}
.select2-results__options{
        font-size:10px!important;
        font-weight: bold;
}
 .select2-container--default .select2-selection--single{
    background-color:lightgray;
}

input {font-weight:bold;}

input[type="text"]:read-only:not([read-only="false"]) { color: blue; background-color: #eee; border-width: 1px!important;border-style: solid!important; border-color: #A9A9A9!important;}

</style>
<div id="#admin"class="page-content container-fluid">
        <div class="alerts"></div>
            <div class="row">
                <div class="col-md-12">

                        <form id="frmTrocaMes" name="frmTrocaMes" method="POST" action="{{route('previsaoorcamentaria/simuladorepi')}}"class="formZero">
                            {{ csrf_field() }}
                            <input type="hidden" name="CODCGA" value="{{$CODCGA}}"/>
                            <input type="hidden" name="CODUNN" value="{{$CODUNN}}"/>
                            <input type="hidden" name="codgru"  value="{{$codgru}}"/>
                            <input type="hidden" name="mesepi" value="" />
                        </form>

                        <form id="frmAddepi" name="frmAddepi" method="POST" action="{{route('previsaoorcamentaria/simuladorepi')}}"class="formZero">
                            {{ csrf_field() }}
                            <input type="hidden" name="CODCGA" value="{{$CODCGA}}"/>
                            <input type="hidden" name="CODUNN" value="{{$CODUNN}}"/>
                            <input type="hidden" name="codgru"  value="{{$codgru}}"/>
                            <input type="hidden" name="addMes" value="" />
                            <input type="hidden" name="addepi" value="ok" id="addepi" />
                        </form>

                        <form id="frmRemover" name="frmRemover" method="POST" action="{{route('previsaoorcamentaria/simuladorepi')}}"class="formZero">
                            {{ csrf_field() }}
                            <input type="hidden" name="CODCGA" value="{{$CODCGA}}"/>
                            <input type="hidden" name="CODUNN" value="{{$CODUNN}}"/>
                            <input type="hidden" name="codgru" value="{{$codgru}}"/>
                            <input type="hidden" name="mesepiRemover" value="{{$mesepi}}" />
                            <input type="hidden" value="ok" name="removerEpi" />
                            <input id="remover_id" type="hidden" value="" name="remover_id" />
                        </form>

                <form name="frmSalvar" method="POST" action="{{route('previsaoorcamentaria/simuladorepi')}}"class="formZero">
                    <input type="hidden" value="ok" name="salvarEpi" />
                    {{ csrf_field() }}
                    <div  class="panel panel-bordered">
                    <div style="margin-left:14px;margin-top:8px;font-size:14px;display:table;width:100%">
                        <span class="badge badge-error">{{$CODCGA}}</span>
                        <span type="submit"class="badge badge-info">{{$centrodegasto}}</span> /
                        <span class="badge badge-error">{{$CODUNN}}</span><span class="badge badge-info">{{$unidade}}</span> /
                        <span class="badge badge-warning">SIMULADOR EPI</span>
                        &nbsp;&nbsp;
                        @if($permissaoeditar == 'S')
                        <select class="form-control select2" style="width:280px!important;font-size:9px!important;margin-right:10px;" id="selMes" name="selMes">
                                <option value="0">SELECIONE UM MÊS...</option>
                            @foreach ($arrayMesF as $key => $value)
                                @if( $key == $mesepi)
                                <option selected="selected" value="{{$key}}">{{$key}}-{{$value}}</option>
                                @else
                                <option value="{{$key}}">{{$key}}-{{$value}}</option>
                                @endif
                            @endforeach
                        </select>

                        <button style="margin-left:10px;" type="button" id="btnaddrateio"class="btn btn-primary" > + NOVO ITEM EPI </button> |
                        <button style="margin-left:10px;" type="submit"class="btn btn-success"> SALVAR </button>
                        @endif
                            <input type="hidden" name="CODCGA" value="{{$CODCGA}}"/>
                            <input type="hidden" name="CODUNN" value="{{$CODUNN}}"/>
                            <input type="hidden" name="codgru" value="{{$codgru}}"/>
                            <input type="hidden" name="mesepiLista" value="{{$mesepi}}"/>
                    </div>
                    @if(count($epi) > 0)
                        <div class="panel-body">
                            <div id="parent" >
                                <table id="fixTable" class="table table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th class="cabNum">#</th>
                                            <th class="cabClass">Classificação</th>
                                            <th class="cabMes">Produto</th>
                                            <th class="cabMes">Cargo</th>
                                            <th class="cabMes">Centro de Custo</th>
                                            <th class="cabMes">Total QLP</th>
                                            <th class="cabMes">Vlr Unt. (R$)</th>
                                            <th class="cabMes">Qntd Itens <br>por Colaborador</th>
                                            <th class="cabMes">Estoque Mínimo</th>
                                            <th class="cabMes">Valor total</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                            @foreach ($epi as $a)
                                                <input type="hidden" name="ID[]" value="{{$a->ID}}"/>
                                                <tr>
                                                <td class="cabValor" scope="row">
                                                <button type="button" onclick="remover('{{$a->ID}}')" id="removerRateio{{$a->ID}}"class="btn btn-danger pull-left" > <i class="admin-trash"></i></button>
                                                </td>
                                                <td class="cabValorTit">{{$a->ANALIT}}-{{$a->ANALITICA}}</td>
                                                <td class="cabValor">
                                                    {{$a->PRODUTO}}
                                                </td>
                                                <td class="cabValor">
                                                    {{$a->CARGO}}
                                                </td>
                                                <td class="cabValor">
                                                    {{$a->CUSTO}}
                                                </td>
                                                <td class="cabValor">
                                                    @if($a->TIPO == 'OP')
                                                    <input readonly type="text" value="{{str_replace('.',',',$a->TOTALQLP)}}"class="currency currencyVal" name="TOTALQLP[]">
                                                    @else
                                                    <input type="text" value="{{str_replace('.',',',$a->TOTALQLP)}}"class="currency currencyVal" name="TOTALQLP[]">
                                                    @endif
                                                </td>
                                                <td class="cabValor">
                                                    <input  type="text" value="{{str_replace('.',',',$a->VALORUNITARIO)}}"class="currency currencyVal" name="VALORUNITARIO[]">
                                                </td>
                                                <td class="cabValor">
                                                    <input  type="text" value="{{str_replace('.',',',$a->QTDITEM)}}"class="currency currencyVal" name="QTDITEM[]">
                                                </td>
                                                <td class="cabValor">
                                                    <input  type="text" value="{{str_replace('.',',',$a->ESTOQMIN)}}"class="currency currencyVal" name="ESTOQMIN[]">
                                                </td>
                                                <td class="cabValor">
                                                    <input readonly  type="text" value="{{str_replace('.',',',$a->VALORTOTAL)}}"class="currency currencyVal" name="VALORTOTAL[]">
                                                </td>
                                                </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                </div>
                        </div>
                        @endif
                    </div>
                    </form>
                </div>

                <!--form name="addNovoTrecho">
                        <button type="submit"class="btn btn-warning" > <i class="admin-check-circle"></i> - REMOVER TRECHO </button>
                </form-->
            </div>
</div>
@endsection
@section('javascript')
<script>

$(document).ready(function() {
                $("#fixTable").tableHeadFixer({"left" : 2});
                $("#selMes").select2({ width: 'resolve' });

                $(window).keydown(function(event){
                    if(event.keyCode == 13) {
                    event.preventDefault();
                    return false;
                    }
                });

            });

            $(document.body).on("change","#selMes",function(){
                //alert(this.value);
                selMes = this.value
                console.log(selMes );
                if(selMes !== '0'){
                 $( "input[name*='mesepi']" ).val(selMes);
                 $('form#frmTrocaMes').submit();
                }
            });

            $('#btnaddrateio').click( function() {
                console.log()
                selMes = $('#selMes').val();
                console.log(selMes );
                if(selMes !== '0'){
                 $( "input[name*='addMes']" ).val(selMes);
                 $('form#frmAddepi').submit();
                }
            });

    remover = function(id){
            console.log(id);
            $('#remover_id').val(id);
            $('form#frmRemover').submit();
    }

          fecharateio = function(){
            $('form#frmfecharRateio').submit();
          }

          replicar = function(item,tipo){

            valor = $( "input[name*='jan"+item+"']" ).val();
            $( "input[name*='fev"+item+"']" ).val(valor);
            $( "input[name*='mar"+item+"']" ).val(valor);
            $( "input[name*='abr"+item+"']" ).val(valor);
            $( "input[name*='mai"+item+"']" ).val(valor);
            $( "input[name*='jun"+item+"']" ).val(valor);
            $( "input[name*='jul"+item+"']" ).val(valor);
            $( "input[name*='ago"+item+"']" ).val(valor);
            $( "input[name*='set"+item+"']" ).val(valor);
            $( "input[name*='out"+item+"']" ).val(valor);
            $( "input[name*='nov"+item+"']" ).val(valor);
            $( "input[name*='dez"+item+"']" ).val(valor);
            }

        $(function() {
          $(".currency").maskMoney({prefix:'', allowZero: true, allowNegative: true, thousands:'.', decimal:',', affixesStay: false});
        });

        Number.prototype.format = function(n, x, s, c) {
            var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\D' : '$') + ')',
                num = this.toFixed(Math.max(0, ~~n));

            return (c ? num.replace('.', c) : num).replace(new RegExp(re, 'g'), '$&' + (s || ','));
        };
</script>
@endsection
