@extends('master',['itemMenu'=>$itemMenu])

@section('page_header')

@php
$menuTitulo = Config::get('menu.menuTitulo');
$menuIcon = Config::get('menu.menuIcon');
$menuUrl = Config::get('menu.menuUrl');
@endphp
    <h1 class="page-title">
    <i class="{{ $menuIcon[$itemMenu] }}"></i> {{ $menuTitulo[$itemMenu] }}
    </h1>
@stop

@section('content')
<style>
.row>[class*=col-] {
    margin-bottom: 5px;
}
</style>
<div class="page-content container-center">

        <div class="alerts"></div>
            <div class="row">
                <div class="col-md-6">
                    <div class="panel panel-bordered">
                        <form name="frmAdd" method="POST" action="{{route('custoclassificacao.store')}}">
                        {{ csrf_field() }}
                        <div class="panel-body">
                                    <div class="form-group  ">

                                    <label for="name">Classificação</label>
                                    <select class="form-control select2" name="CODCLAP">
                                        @foreach ($listaClassificacao as $lr)
                                        <option value="{{$lr->CODCLAP}}">{{$lr->CODCLAP}} - {{$lr->SINTETICA}} [{{$lr->ANALITICA}}] </option>
                                        @endforeach
                                    </select>
                                    </div>
                                    <div class="form-group  ">

                                        <label for="name">Centro de Custo</label>
                                        <select class="form-control select2" name="CODCUS">
                                            <option value="0">0 - RATEIO DE CUSTO</option>
                                            @foreach ($listaCustos as $lr)
                                            <option value="{{$lr->CODCUS}}">{{$lr->CODCUS}} - {{$lr->CUSTO}}</option>
                                            @endforeach
                                        </select>
                                   </div>
                                   <div class="form-group" >
                                        <label for="name">Rateio Obrigatorio?</label>
                                        <select class="form-control select2" name="OBRIGARATEIO">
                                                <option value="N">Não</option>
                                                <option value="S">Sim</option>
                                        </select>
                                    </div>
                        </div>
                        <div class="panel-footer">
                                <button type="submit" class="btn btn-primary save">Salvar</button>
                        </div>
                       </form>
                    </div>
                </div>
            </div>
</div>
@endsection
@section('javascript')
<script>
        $(document).ready( function () {
          });
</script>
@endsection
